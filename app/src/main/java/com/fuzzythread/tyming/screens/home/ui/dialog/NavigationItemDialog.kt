package com.fuzzythread.tyming.screens.home.ui.dialog

import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.databinding.DialogNavigationItemBinding
import com.fuzzythread.tyming.global.utils.other.EntityType
import com.fuzzythread.tyming.global.utils.ui.convertDpToPx

class NavigationItemDialog : DialogFragment() {

    private var callbackDismiss: (() -> Unit)? = null
    private var callbackEdit: (() -> Unit)? = null
    private var callbackDelete: (() -> Unit)? = null
    private var callbackAdd: (() -> Unit)? = null
    private var callbackExtra: (() -> Unit)? = null
    private var entityType: EntityType = EntityType.TASK
    private var flag: Boolean = false
    private var trackProgress: Boolean = false

    private var clickedAdd: Boolean = false

    fun setDismissCallBack(func: () -> Unit): NavigationItemDialog {
        callbackDismiss = func
        return this
    }

    fun setEditCallBack(func: () -> Unit): NavigationItemDialog {
        callbackEdit = func
        return this
    }

    fun setAddCallBack(func: () -> Unit): NavigationItemDialog {
        callbackAdd = func
        return this
    }

    fun setDeleteCallBack(func: () -> Unit): NavigationItemDialog {
        callbackDelete = func
        return this
    }

    fun setExtraCallBack(func: () -> Unit): NavigationItemDialog {
        callbackExtra = func
        return this
    }

    fun setDialogConfiguration(entityType: EntityType, flag: Boolean = false, trackProgress: Boolean = false): NavigationItemDialog {
        this.entityType = entityType
        this.flag = flag
        this.trackProgress = trackProgress
        return this
    }

    private fun removeDismissCallBacks() {
        callbackDismiss = null
        callbackEdit = null
        callbackDelete = null
        callbackAdd = null
        callbackExtra = null
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        val binding = DialogNavigationItemBinding.inflate(inflater, container, false)

        if (entityType == EntityType.TASK) {
            if (trackProgress) {
                binding.extraBtn.visibility = View.VISIBLE
            } else {
                binding.extraBtn.visibility = View.GONE
            }
            if (flag) {
                binding.extra.text = context?.getString(R.string.uncomplete)
            } else {
                binding.extra.text = context?.getString(R.string.complete)?.capitalize()
            }
            binding.addLabel.text = context?.getString(R.string.add_time_record)
        }

        if (entityType == EntityType.PROJECT || entityType == EntityType.AREA) {
            binding.task2.setImageResource(R.drawable.ic_archive)
            if (flag) {
                binding.extra.text = context?.getString(R.string.unarchive)
            } else {
                binding.extra.text = context?.getString(R.string.to_archive)
            }

            if (entityType == EntityType.AREA) {
                binding.addLabel.text = context?.getString(R.string.add_project)
            } else {
                binding.addLabel.text = context?.getString(R.string.add_task)
            }
        }

        if (entityType == EntityType.TIMERECORD) {
            binding.extraBtn.visibility = View.GONE
            binding.addBtn.visibility = View.GONE

            // remove margin:
            val layoutParams = binding.editBtn.layoutParams as ViewGroup.MarginLayoutParams
            layoutParams.setMargins(0, 0, 0, 0)
            binding.editBtn.layoutParams = layoutParams
        }

        binding.editLabel.text = context?.getString(R.string.edit)
        binding.deleteLabel.text = context?.getString(R.string.delete)

        binding.extraBtn.setOnClickListener {
            clickedAdd = true
            callbackExtra?.invoke()
            dialog?.dismiss()
        }

        binding.addBtn.setOnClickListener {
            clickedAdd = true
            callbackAdd?.invoke()
            dialog?.dismiss()
        }

        binding.editBtn.setOnClickListener {
            clickedAdd = true
            callbackEdit?.invoke()
            dialog?.dismiss()
        }

        binding.deleteBtn.setOnClickListener {
            clickedAdd = true
            callbackDelete?.invoke()
            dialog?.dismiss()
        }

        setDialogPosition()
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        val window = dialog?.window
        val params = window!!.attributes
        params.dimAmount = 0.6f
        window.attributes = params
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(width, height)
        window.setBackgroundDrawableResource(android.R.color.transparent)
        dialog?.setCanceledOnTouchOutside(true)
    }

    private fun setDialogPosition() {
        val window = dialog?.window
        window!!.setGravity(Gravity.CENTER_HORIZONTAL or Gravity.BOTTOM)
        val params = window.attributes
        params.y = convertDpToPx(this.requireContext(), 8)
        params.windowAnimations = R.style.DialogSlideBottom
        window.attributes = params
    }

    override fun onDismiss(dialog: DialogInterface) {
        if (!clickedAdd) {
            callbackDismiss?.invoke()
        }
        removeDismissCallBacks()
        super.onDismiss(dialog)
    }
}
