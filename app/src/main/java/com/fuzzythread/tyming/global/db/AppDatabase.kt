package com.fuzzythread.tyming.global.db

import android.content.Context
import com.fuzzythread.tyming.global.db.dao.AreaDao
import com.fuzzythread.tyming.global.db.dao.ProjectDao
import com.fuzzythread.tyming.global.db.dao.TaskDao
import com.fuzzythread.tyming.global.db.dao.TimeEntryDao
import com.fuzzythread.tyming.global.db.entities.MyObjectBox
import io.objectbox.BoxStore

class AppDatabase {

    private var boxStore: BoxStore

    constructor(context: Context){
        boxStore = MyObjectBox.builder().androidContext(context).build()
    }

    fun categoryDao(): AreaDao = AreaDao(store = boxStore)
    fun projectDao(): ProjectDao = ProjectDao(store = boxStore)
    fun taskDao(): TaskDao = TaskDao(store = boxStore)
    fun timeEntryDao(): TimeEntryDao = TimeEntryDao(store = boxStore)
}