package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog

import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import com.fuzzythread.tyming.R
import android.view.Gravity
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.fuzzythread.tyming.databinding.DialogParentBinding
import com.fuzzythread.tyming.global.di.ViewModelFactory
import com.fuzzythread.tyming.global.utils.other.EditingType
import com.fuzzythread.tyming.global.utils.other.ParentType
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.model.ParentDialogVM
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.adapter.ParentsAdapter
import dagger.android.support.DaggerAppCompatDialogFragment
import javax.inject.Inject

class ParentDialog : DaggerAppCompatDialogFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val parentDialogVM: ParentDialogVM by lazy {
        ViewModelProvider(this, viewModelFactory).get(ParentDialogVM::class.java)
    }

    private var callback: ((Long, ParentType) -> Unit)? = null
    private var parentId: Long = 0
    private var parentType: ParentType = ParentType.NONE
    private var editingType: EditingType = EditingType.TASK

    // UI:
    private var adapter: ParentsAdapter? = null
    private var mView: View? = null

    fun getInstance(parentId: Long, parentType: ParentType, editingType: EditingType): ParentDialog {
        val fragment = ParentDialog()
        fragment.arguments = Bundle().apply {
            putLong(PARENT_ID, parentId)
            putString(PARENT_TYPE, parentType.toString())
            putString(EDITING_TYPE, editingType.toString())
        }
        return fragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        // restore bundle:
        parentId = arguments?.getLong(PARENT_ID) ?: 0
        parentType = ParentType.valueOf(arguments?.getString(PARENT_TYPE) ?: "NONE")
        editingType = EditingType.valueOf(arguments?.getString(EDITING_TYPE) ?: "DAY")
        super.onCreate(savedInstanceState)
    }

    fun setChooseParentCallBack(func: ((Long, ParentType) -> Unit)) {
        callback = func
    }

    private fun removeCallBacks() {
        callback = null
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        val itemBinding = DialogParentBinding.inflate(inflater, container, false)
        itemBinding.viewmodel = parentDialogVM
        parentDialogVM.initialize(parentId, parentType)
        parentDialogVM.editingType = editingType
        adapter = ParentsAdapter(this.requireContext(), parentId, parentType)
        initializeListeners(itemBinding)
        setDialogPosition()
        return itemBinding.root
    }

    private fun initializeListeners(binding: DialogParentBinding) {

        binding.save.setOnClickListener {
            callback?.invoke(parentDialogVM.parentId, parentDialogVM.parentType)
            dialog?.dismiss()
        }

        binding.area.setOnClickListener {
            val id = if (parentDialogVM.parentType == ParentType.AREA) {
                parentDialogVM.parentId
            } else {
                0
            }
            adapter?.refresh(parentDialogVM.areasVMs, id, ParentType.AREA)
            parentDialogVM.editing = true
        }

        binding.project.setOnClickListener {
            val id = if (parentDialogVM.parentType == ParentType.PROJECT) {
                parentDialogVM.parentId
            } else {
                0
            }
            adapter?.refresh(parentDialogVM.projectsVMs, id, ParentType.PROJECT)
            parentDialogVM.editing = true
        }

        binding.noParent.setOnClickListener {
            parentDialogVM.initialize(0, ParentType.NONE)
        }

        // callback:
        val callbackItemSelected = { parentId: Long, parentType: ParentType ->
            this.parentId = parentId
            this.parentType = parentType
            parentDialogVM.initialize(parentId, parentType)
            parentDialogVM.editing = false
        }

        adapter?.setSelectedCallback(callbackItemSelected)
        binding.parentsRv.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        val window = dialog?.window
        val params = window!!.attributes
        params.dimAmount = 0.6f
        window.attributes = params
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(width, height)
        window.setBackgroundDrawableResource(android.R.color.transparent)
        dialog?.setCanceledOnTouchOutside(true)
    }

    private fun setDialogPosition() {
        val window = dialog?.window
        window!!.setGravity(Gravity.CENTER_HORIZONTAL or Gravity.CENTER_VERTICAL)
        val params = window.attributes
        params.windowAnimations = R.style.DialogSlideBottom
        window.attributes = params
    }

    override fun onDismiss(dialog: DialogInterface) {
        removeCallBacks()
        super.onDismiss(dialog)
    }

    companion object {
        const val PARENT_ID = "PARENT_ID"
        const val PARENT_TYPE = "PARENT_TYPE"
        const val EDITING_TYPE = "EDITING_TYPE"
    }
}
