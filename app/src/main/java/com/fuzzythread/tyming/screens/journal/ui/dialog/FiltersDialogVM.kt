package com.fuzzythread.tyming.screens.journal.ui.dialog

import androidx.databinding.Bindable
import com.fuzzythread.tyming.BR
import com.fuzzythread.tyming.global.ObservableViewModel
import com.fuzzythread.tyming.global.utils.other.DisplayCompletedTasks
import com.fuzzythread.tyming.global.utils.other.DisplayMode
import com.fuzzythread.tyming.global.utils.other.SortBy
import com.fuzzythread.tyming.global.utils.other.TimeFrame

class FiltersDialogVM : ObservableViewModel() {
    var timeFrame = TimeFrame.MONTH
        set(value) {
            field = value
            notifyPropertyChanged(BR.monthly)
            notifyPropertyChanged(BR.weekly)
        }

    var sortBy = SortBy.DESC
        set(value) {
            field = value
            notifyPropertyChanged(BR.desc)
            notifyPropertyChanged(BR.asc)
        }

    var displayMode = DisplayMode.CLUSTER
        set(value) {
            field = value
            notifyPropertyChanged(BR.cluster)
            notifyPropertyChanged(BR.startEnd)
        }

    var completedTasks = DisplayCompletedTasks.VISIBLE
        set(value) {
            field = value
            notifyPropertyChanged(BR.completedTasksVisible)
            notifyPropertyChanged(BR.completedTasksHidden)
        }

    @get:Bindable
    val monthly: Boolean get() = timeFrame == TimeFrame.MONTH

    @get:Bindable
    val weekly: Boolean get() = timeFrame == TimeFrame.WEEK

    @get:Bindable
    val desc: Boolean get() = sortBy == SortBy.DESC

    @get:Bindable
    val asc: Boolean get() = sortBy == SortBy.ASC

    @get:Bindable
    val custom: Boolean get() = sortBy == SortBy.CUSTOM

    @get:Bindable
    val cluster: Boolean get() = displayMode == DisplayMode.CLUSTER

    @get:Bindable
    val startEnd: Boolean get() = displayMode == DisplayMode.STARTEND

    @get:Bindable
    val completedTasksVisible: Boolean get() = completedTasks == DisplayCompletedTasks.VISIBLE

    @get:Bindable
    val completedTasksHidden: Boolean get() = completedTasks == DisplayCompletedTasks.HIDDEN
}
