package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model

import android.content.Context
import androidx.databinding.Bindable
import com.fuzzythread.tyming.BR
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.global.ObservableViewModel
import com.fuzzythread.tyming.global.db.entities.AreaEntity
import com.fuzzythread.tyming.global.exception.TitleNotSpecifiedException
import com.fuzzythread.tyming.global.ui.helpers.IAdapterComparable
import com.fuzzythread.tyming.global.utils.other.EntityType
import java.util.*

class AreaViewModel(val context: Context?) : ObservableViewModel(), IAdapterComparable<AreaViewModel> {

    constructor(areaEntity: AreaEntity, context: Context) : this(context) {
        id = areaEntity.id
        notes = areaEntity.notes
        title = areaEntity.title
        color = areaEntity.color
        createdDate = areaEntity.createdDate
        archivedDate = areaEntity.archivedDate
        collapsed = areaEntity.isCollapsed
        displayOrder = areaEntity.displayOrder
    }

    // region Database Model Attributes

    var id: Long = 0

    @get:Bindable
    var title: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.title)
        }

    @get:Bindable
    var notes: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.notes)
        }

    @get:Bindable
    var displayOrder: Long = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.displayOrder)
        }

    var createdDate: Date? = null
    var archivedDate: Date? = null

    val isArchived: Boolean
        get() {
            return archivedDate != null
        }

    @get:Bindable
    var color: String = "#1e88e5"
        set(value) {
            field = value
            notifyPropertyChanged(BR.color)
        }

    // endregion

    // region View States

    @get:Bindable
    var editing: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.editing)
        }

    @get:Bindable
    var completed: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.completed)
        }

    @get:Bindable
    var selected: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.selected)
        }

    @get:Bindable
    var collapsed: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.collapsed)
            notifyPropertyChanged(BR.projectCountFormatted)
        }
        get() {
            if (projectCount == 0) {
                return true
            }
            return field
        }

    var projectCount: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.projectCountFormatted)
        }

    @get:Bindable
    var projectCountFormatted: String = "0"
        get() {
            return if (projectCount == 0) {
                context?.getString(R.string.no_projects) ?: "No Projects"
            } else {
                if (collapsed) {
                    "$projectCount ${if (projectCount > 1) {
                        context?.getString(R.string._projects) ?:"Projects"
                    } else {
                        context?.getString(R.string.project) ?: "Project"
                    }}"
                } else {
                    ""
                }
            }
        }

    // endregion

    // region DiffUtil Functions

    override fun areItemsTheSame(itemToCompare: AreaViewModel): Boolean {
        return itemToCompare.id == id
    }

    override fun areContentsTheSame(itemToCompare: AreaViewModel): Boolean {
        return itemToCompare.title == title &&
                itemToCompare.notes == notes &&
                itemToCompare.createdDate == createdDate &&
                itemToCompare.color == color &&
                itemToCompare.collapsed == collapsed &&
                itemToCompare.displayOrder == displayOrder &&
                itemToCompare.projectCountFormatted == projectCountFormatted
    }

    fun validate() {
        validateAreaName()
    }

    private fun validateAreaName() {
        if (title.isEmpty() || title.isBlank()) {
            throw TitleNotSpecifiedException(EntityType.AREA.toString())
        }
    }

    // endregion
}
