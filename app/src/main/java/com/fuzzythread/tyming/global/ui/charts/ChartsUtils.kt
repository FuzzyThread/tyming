package com.fuzzythread.tyming.global.ui.charts

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.global.utils.other.TimeFrame
import com.fuzzythread.tyming.global.utils.time.formatTimeToString
import com.fuzzythread.tyming.global.utils.ui.convertDpToPx
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.*
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.formatter.DefaultValueFormatter
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import kotlin.math.abs

fun PieChart.disablePieChartDefaults() {
    with(this) {
        legend.isEnabled = false // Hide the legend
        isDrawHoleEnabled = true
        holeRadius = 86f
        setHoleColor(Color.TRANSPARENT)
        isRotationEnabled = false
        setDrawCenterText(true)
        description = Description().apply { text = "" }
    }
}

fun PieChart.addMarkerView(context: Context?) {
    with(this) {
        context?.also {
            val mv = CustomMarkerView(it, R.layout.marker_chart)
            mv.chartView = this
            setMarkerView(mv)
        }
    }
}

fun BarChart.initStyle(timeFrame: TimeFrame, daysAmount: Int) {
    with(this) {
        if (timeFrame == TimeFrame.WEEK) {
            setBottomAxisFontSize(textSize = 12f)
            setBarChartOffsets(top = 8, bottom = 32, left = -8, right = -6)
        } else {
            setBottomAxisFontSize(textSize = 12f)
            setBarChartOffsets(top = 8, bottom = 32, left = 8, right = 12)
        }
    }
}

fun BarChart.initBarChartDaysLabels(isCurrentWeek: Boolean, daysNames: List<String>, range: IntRange, currentDayId: Int = 0) {
    val colors = (range).map { i ->
        if (i == currentDayId && isCurrentWeek) {
            Color.YELLOW
        } else {
            Color.WHITE
        }
    }
    with(this) {
        // days
        xAxis.valueFormatter = object : DefaultValueFormatter(7), IAxisValueFormatter {
            override fun getFormattedValue(value: Float, axis: AxisBase?): String {
                val id = (value).toInt()
                return if (id < daysNames.size) {
                    daysNames[id]
                } else {
                    ""
                }
            }
        }
        xAxis.labelCount = daysNames.size
        // highlight current day
        setXAxisRenderer(ColoredLabelXAxisRenderer(viewPortHandler, xAxis, getTransformer(YAxis.AxisDependency.LEFT), colors))
    }
}

fun BarChart.drawBarChartLimitLines(barData: BarData, averageHours: Float, overallWeekTime: Float, typeface: Typeface, context: Context?) {
    with(this) {
        if (barData.yMax > 0) {
            this.axisLeft?.removeAllLimitLines()
            if (overallWeekTime > 0) {
                val maxLimitLine = createLimitLine(lineColor = "#595959", textColor = "#9e9e9e", value = barData.yMax, paramName = context?.getString(R.string.max) ?: "Max", typeface = typeface)
                this.axisLeft?.addLimitLine(maxLimitLine)

                if (averageHours > 0 && abs(averageHours / barData.yMax) < 0.85) {
                    val avgLimitLine = createLimitLine(lineColor = "#4caf50", textColor = "#4caf50", value = averageHours, paramName = context?.getString(R.string.avg) ?: "Avg", typeface = typeface)
                    this.axisLeft?.addLimitLine(avgLimitLine)
                }
            }
        }
    }
}

fun createLimitLine(lineColor: String, textColor: String, value: Float, paramName: String, typeface: Typeface): LimitLine {
    val limitLine = LimitLine(value, "${formatTimeToString(value)} $paramName ")
    limitLine.lineWidth = 1f
    limitLine.textSize = 12f
    limitLine.lineColor = Color.parseColor(lineColor)
    limitLine.textColor = Color.parseColor(textColor)
    limitLine.enableDashedLine(6f, 10f, 10f)
    limitLine.typeface = typeface
    return limitLine
}

// Highlights:

fun createInverseHighlights(selectedPosition: Int, daysAmount: Int): Array<Highlight> {
    return (0..daysAmount).filter { it != selectedPosition }.map {
        Highlight(0f, 0f, it)
    }.toTypedArray()
}

// Bar Chart:

fun BarChart.disableBarChartDefaults() {
    with(this) {
        description = Description().apply {
            text = ""
        }
        setDrawGridBackground(false)
        setDrawBorders(false)
        setNoDataText("No data")
        setScaleEnabled(false)
        isDragEnabled = false
        isHighlightPerDragEnabled = false
        isHighlightPerTapEnabled = true
        isHighlightFullBarEnabled = true
        legend.isEnabled = false
    }
}

fun BarChart.hideBarChartAxises() {
    with(this) {
        val leftAxis = this.axisLeft
        leftAxis.isEnabled = true
        leftAxis.setDrawGridLines(false)
        leftAxis.setDrawZeroLine(false)
        leftAxis.zeroLineColor = Color.RED
        leftAxis.axisMinimum = 0f
        leftAxis.axisLineColor = Color.TRANSPARENT
        leftAxis.textColor = Color.TRANSPARENT

        val rightAxis = this.axisRight
        rightAxis.axisLineColor = Color.TRANSPARENT
        rightAxis.isEnabled = false

        xAxis.setDrawGridLines(false)
        xAxis.setDrawAxisLine(false)
        xAxis.isGranularityEnabled = false
        xAxis.position = XAxis.XAxisPosition.BOTTOM
    }
}

fun BarChart.setBarChartOffsets(top: Int, bottom: Int, left: Int = 0, right: Int = 0) {
    with(this) {
        setExtraOffsets(0f, 0f, 0f, 0f)
        setPadding(0, 0, 0, 0)
        minOffset = 0f
        setViewPortOffsets(convertDpToPx(context, left).toFloat(), convertDpToPx(context, top).toFloat(), convertDpToPx(context, right).toFloat(), convertDpToPx(context, bottom).toFloat())
    }
}

fun BarChart.setBottomAxisFontSize(textSize: Float) {
    with(this) {
        xAxis.textSize = textSize
    }
}
