package com.fuzzythread.tyming.global.db

import android.content.Context
import android.graphics.Color
import com.fuzzythread.tyming.global.ObservableViewModel
import com.fuzzythread.tyming.global.db.dao.AreaDao
import com.fuzzythread.tyming.global.db.dao.ProjectDao
import com.fuzzythread.tyming.global.db.dao.TaskDao
import com.fuzzythread.tyming.global.db.dao.TimeEntryDao
import com.fuzzythread.tyming.global.db.entities.AreaEntity
import com.fuzzythread.tyming.global.db.entities.ProjectEntity
import com.fuzzythread.tyming.global.db.entities.TaskEntity
import com.fuzzythread.tyming.global.db.entities.TimeEntryEntity
import com.fuzzythread.tyming.global.utils.other.ParentType
import com.fuzzythread.tyming.global.utils.time.formatTimeToString
import com.fuzzythread.tyming.global.utils.time.sumTimeEntriesPeriods
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.ProjectViewModel
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.TaskViewModel
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.TimeEntryViewModel
import org.joda.time.*

open class GeneralRepository(
        private val areaDao: AreaDao,
        private val projectDao: ProjectDao,
        private val taskDao: TaskDao,
        private val timeEntryDao: TimeEntryDao,
        private val context: Context
) : ObservableViewModel() {

    fun getProjectsCount(): Long {
        return projectDao.amount()
    }

    fun getTimeFormattedForDate(date: DateTime): String {
        return formatTimeToString(sumTimeEntriesPeriods(timeEntryDao.getTimeEntriesByDay(date)))
    }

    fun getWeeks(): MutableList<Pair<DateTime, String>> {
        val firstWeek = DateTime(timeEntryDao.getFirstTimeEntry()).dayOfWeek().withMinimumValue()
        val currentWeek = DateTime().dayOfWeek().withMaximumValue()

        // get range of months:
        val weeksRange = Weeks.weeksBetween(firstWeek, currentWeek).weeks
        val weeks = mutableListOf<Pair<DateTime, String>>()
        (0..weeksRange).forEach { weekId ->
            val week = firstWeek.plusWeeks(weekId)
            var weekLabel = "Current Week"

            if (weekId < weeksRange) {
                if (weekId == weeksRange - 1) {
                    weekLabel = "Last Week"
                } else {
                    weekLabel = "${weeksRange - weekId} Weeks Before"
                }
            }
            weeks.add(Pair(week, weekLabel))
        }
        return weeks
    }

    fun getMonths(): MutableList<Pair<DateTime, String>> {
        val firstMonth = DateTime(timeEntryDao.getFirstTimeEntry()).dayOfMonth().withMinimumValue()
        val lastMonth = DateTime().dayOfMonth().withMaximumValue()

        // get range of months:
        val monthsRange = Months.monthsBetween(firstMonth, lastMonth).months
        val months = mutableListOf<Pair<DateTime, String>>()
        (0..monthsRange).forEach { monthId ->
            val month = firstMonth.plusMonths(monthId)
            var monthLabel = "Current Month"

            if (monthId < monthsRange) {
                if (monthId == monthsRange - 1) {
                    monthLabel = "Last Month"
                } else {
                    monthLabel = "${monthsRange - monthId} Month Before"
                }
            }
            months.add(Pair(month, monthLabel))
        }
        return months
    }

    fun createProjectVmFromEntity(projectEntity: ProjectEntity): ProjectViewModel {
        return ProjectViewModel(context).apply {
            id = projectEntity.id
            notes = projectEntity.notes
            title = projectEntity.title
            tasks = projectEntity.tasks
            color = projectEntity.color
            area = getAreaEntity(projectEntity.area.targetId)
            deadlineDate = projectEntity.deadlineDate
            hasDeadline = projectEntity.deadlineDate != null
            trackProgress = projectEntity.trackProgress
            collapsed = projectEntity.isCollapsed
            createdDate = projectEntity.createdDate
            archivedDate = projectEntity.archivedDate
            displayOrder = projectEntity.displayOrder
        }
    }

    fun createTaskVmFromEntity(taskEntity: TaskEntity): TaskViewModel {
        return TaskViewModel(context).apply {
            id = taskEntity.id
            notes = taskEntity.notes
            title = taskEntity.title
            timeEntries = taskEntity.timeEntries
            parent = getProjectEntity(taskEntity.project.targetId)
            deadlineDate = taskEntity.deadlineDate
            hasTags = taskEntity.tags.size > 0
            hasDeadline = taskEntity.deadlineDate != null
            completedDate = taskEntity.completedDate
            createdDate = taskEntity.createdDate
            timeElapsed = taskEntity.timeElapsed
            timerRunning = taskEntity.isTimerRunning
            createdDate = taskEntity.createdDate
            displayOrder = taskEntity.displayOrder
        }
    }

    fun getTaskViewModel(taskId: Long): TaskViewModel? {
        taskDao.get(taskId)?.also {
            val parentByType = when {
                it.project.targetId > 0 -> getProjectEntity(it.project.targetId)
                else -> null
            }
            return TaskViewModel(context).apply {
                id = it.id
                notes = it.notes
                title = it.title
                timeEntries = it.timeEntries
                parent = parentByType
                deadlineDate = it.deadlineDate
                completedDate = it.completedDate
                createdDate = it.createdDate
                hasTags = it.tags.size > 0
                displayOrder = it.displayOrder
            }
        }
        return null
    }

    fun getProjectViewModel(projectId: Long): ProjectViewModel? {
        projectDao.get(projectId)?.also {
            return ProjectViewModel(context).apply {
                id = it.id
                notes = it.notes.capitalize()
                title = it.title.capitalize()
                color = it.color
                tasks = it.tasks
                deadlineDate = it.deadlineDate
                trackProgress = it.trackProgress
                area = getAreaEntity(it.area.targetId)
            }
        }
        return null
    }

    fun getTimeEntryViewModel(timeEntryId: Long): TimeEntryViewModel? {
        getTimeEntryEntity(timeEntryId)?.also {
            return TimeEntryViewModel(context).apply {
                id = it.id
                notes = it.notes.capitalize()
                task = getTaskEntity(it.task.targetId)
                startDate = it.startDate
                endDate = it.endDate
            }
        }
        return null
    }

    private fun getTimeEntryEntity(id: Long): TimeEntryEntity? {
        return if (id > 0) {
            timeEntryDao.get(id)
        } else {
            null
        }
    }

    fun updateTask(taskViewModel: TaskViewModel) {
        taskDao.update(taskDao.get(taskViewModel.id)!!.apply {
            notes = taskViewModel.notes.capitalize()
            title = taskViewModel.title.capitalize()
            deadlineDate = taskViewModel.deadlineDate
            completedDate = taskViewModel.completedDate
            displayOrder = taskViewModel.displayOrder
        }.apply {
            when (taskViewModel.parentType) {
                ParentType.PROJECT -> {
                    project.targetId = taskViewModel.parentId
                }
                ParentType.NONE -> {
                    project.targetId = 0
                }

                ParentType.AREA -> {
                    print("Not Implemented")
                }
            }
        })
    }

    fun getProjectColor(projectId: Long): Int {
        return Color.parseColor(getProjectEntity(projectId)?.color)
    }

    fun getProjectEntity(id: Long): ProjectEntity? {
        return if (id > 0) {
            return projectDao.get(id)
        } else {
            null
        }
    }

    fun getTaskEntity(id: Long): TaskEntity? {
        return if (id > 0) {
            taskDao.get(id)
        } else {
            null
        }
    }

    fun getAreaEntity(id: Long): AreaEntity? {
        return if (id > 0) {
            areaDao.get(id)
        } else {
            null
        }
    }
}
