package com.fuzzythread.tyming.screens.journal.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.databinding.FragmentJournalBinding
import com.fuzzythread.tyming.global.di.ViewModelFactory
import com.fuzzythread.tyming.global.ui.elements.TymingFragment
import com.fuzzythread.tyming.global.utils.other.Constants.Companion.APP_TAG
import com.fuzzythread.tyming.global.utils.other.DisplayMode
import com.fuzzythread.tyming.global.utils.other.PrefUtil
import com.fuzzythread.tyming.global.utils.other.SortBy
import com.fuzzythread.tyming.global.utils.other.TimeFrame
import com.fuzzythread.tyming.screens.journal.model.JournalRepository
import com.fuzzythread.tyming.screens.journal.model.JournalRepository.JournalData
import com.fuzzythread.tyming.screens.journal.ui.adapter.FragmentLifecycle
import com.fuzzythread.tyming.screens.journal.ui.adapter.TimeEntriesAdapter
import kotlinx.coroutines.*
import org.joda.time.DateTime
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class JournalFragment : TymingFragment(), CoroutineScope, FragmentLifecycle {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private val repository: JournalRepository by lazy {
        ViewModelProvider(this, viewModelFactory).get(JournalRepository::class.java)
    }

    // Fragment coroutine:
    var masterJob: Job = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + masterJob

    private var timeFrameTV: AppCompatTextView? = null
    private var noDataContainer: ConstraintLayout? = null
    private var journalNoDataLabel: AppCompatTextView? = null
    private var timelineItemsRV: RecyclerView? = null
    private var timelineAdapter: TimeEntriesAdapter? = null
    private var dateLabel: String = ""
    private var date: DateTime = DateTime()
    private var timeFrame: TimeFrame = TimeFrame.WEEK
    private var sortBy: SortBy = SortBy.DESC
    private var displayMode: DisplayMode = DisplayMode.CLUSTER
    private var journalData: JournalData? = null

    var isFragmentResumed: Boolean = false
    var isCurrentTimeFrame: Boolean = false

    // region Android hooks
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // load passed arguments:
        timeFrame = TimeFrame.valueOf(arguments?.getString(TIME_FRAME) ?: "WEEK")
        sortBy = SortBy.valueOf(arguments?.getString(SORT_BY) ?: "DESC")
        displayMode = DisplayMode.valueOf(arguments?.getString(DISPLAY_MODE) ?: "CLUSTER")
        date = DateTime(arguments?.getString(DATE) ?: "")
        dateLabel = arguments?.getString(DATE_LABEL) ?: ""
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentJournalBinding.inflate(inflater, container, false)
        initializeUI(binding)
        return binding.root
    }

    override fun onDestroy() {
        masterJob.cancel()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        refreshAsync()
        isFragmentResumed = true
    }

    override fun onPause() {
        super.onPause()
        isFragmentResumed = false
    }

    override fun onPauseFragment() {
        Timber.tag(APP_TAG).d("Fragment paused")
    }

    override fun onResumeFragment() {
        Timber.tag(APP_TAG).d("Fragment resumed")
    }
    // endregion

    // region Data
    fun refreshAsync() {
        // reset previous job:
        masterJob.cancel()
        masterJob = Job()

        launch(this.coroutineContext) {
            val dataIsDifferent = async {
                updateFilters()
                refreshData()
            }
            dataIsDifferent.await()
            withContext(Dispatchers.Main) {
                updateUIWithValues()
            }
        }
    }

    private fun updateFilters() {
        val activity = this.activity ?: return
        timeFrame = PrefUtil.getTimeFrame(activity)
        sortBy = PrefUtil.getSortByJournal(activity)
        displayMode = PrefUtil.getDisplayMode(activity)
    }

    private fun refreshData() {
        journalData = repository.getJournalData(date, timeFrame, sortBy, displayMode)
    }
    // endregion

    // region UI
    private fun initializeUI(binding: FragmentJournalBinding) {
        initViews(binding)
        initializeRecyclerViews(binding)
    }

    private fun initViews(binding: FragmentJournalBinding) {
        timeFrameTV = binding.weekTv
        journalNoDataLabel = binding.noData
        timelineItemsRV = binding.monthTimeEntriesRv2
        noDataContainer = binding.noDataContainer
    }

    private fun initializeRecyclerViews(binding: FragmentJournalBinding) {
        timelineAdapter = TimeEntriesAdapter(repository, binding.root.context, this)
        binding.monthTimeEntriesRv2.adapter = timelineAdapter

        // load data for first time frame, then load async other time-frames
        if (isCurrentTimeFrame) {
            refreshData()
            updateUIWithValues()
        }
    }

    fun updateActivityVP() {
        (activity as JournalActivity).updateFragmentsContentAsync()
    }

    private fun updateUIWithValues() {
        journalData?.apply {
            timeFrameTV?.text = timeframeDays

            if (timelineItems.isNotEmpty()) {
                noDataContainer?.visibility = View.GONE
                timelineItemsRV?.visibility = View.VISIBLE
                timelineAdapter?.refresh(timelineItems)
                timelineItemsRV?.scrollToPosition(0)
            } else {
                timelineItemsRV?.visibility = View.GONE
                noDataContainer?.visibility = View.VISIBLE

                if (timeFrame == TimeFrame.WEEK) {
                    journalNoDataLabel?.text = context?.getString(R.string.no_data_collected_this_week)
                } else {
                    journalNoDataLabel?.text = context?.getString(R.string.no_data_collected_this_month)
                }
            }
        }
    }
    // endregion

    companion object {
        const val TIME_FRAME = "TIME_FRAME"
        const val SORT_BY = "SORT_BY"
        const val DISPLAY_MODE = "DISPLAY_MODE"
        const val DATE = "DATE"
        const val DATE_LABEL = "DATE_LABEL"

        fun getInstance(timeFrame: TimeFrame, sortBy: SortBy, displayMode: DisplayMode, date: DateTime, dateLabel: String): JournalFragment {
            val fragment = JournalFragment()
            fragment.arguments = Bundle().apply {
                putString(TIME_FRAME, timeFrame.toString())
                putString(SORT_BY, sortBy.toString())
                putString(DISPLAY_MODE, displayMode.toString())
                putString(DATE, date.toString())
                putString(DATE_LABEL, dateLabel)
            }
            return fragment
        }
    }
}
