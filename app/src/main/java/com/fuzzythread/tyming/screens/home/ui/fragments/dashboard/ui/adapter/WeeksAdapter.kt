package com.fuzzythread.tyming.screens.home.ui.fragments.dashboard.ui.adapter

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.databinding.ItemWeekBinding
import com.fuzzythread.tyming.screens.home.ui.fragments.dashboard.model.DashboardRepository.StatsWeek
import com.fuzzythread.tyming.screens.home.ui.fragments.dashboard.ui.DashboardFragment
import com.fuzzythread.tyming.global.ui.charts.*
import com.fuzzythread.tyming.global.utils.other.WEEK_LENGTH
import com.fuzzythread.tyming.global.utils.time.formatTimeToString
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import org.joda.time.DateTime

open class WeeksAdapter(var fragment: DashboardFragment) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var items: MutableList<StatsWeek> = mutableListOf()
    private var mRecyclerView: RecyclerView? = null
    private var daySelectedCallback: ((DateTime?) -> Unit)? = null
    private var typeface: Typeface? = null


    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        mRecyclerView = recyclerView
        typeface = ResourcesCompat.getFont(recyclerView.context, R.font.quicksand_light)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ItemWeekBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return WeekViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        with(holder as WeekViewHolder) {
            bind(items[position])
        }
    }

    fun refreshAdapterWithData(newStatsPreviewData: MutableList<StatsWeek>) {
        items = newStatsPreviewData
        this.notifyDataSetChanged()
    }

    fun bindDaySelectedCallback(callback: (DateTime?) -> Unit) {
        daySelectedCallback = callback
    }

    fun unbindCallbacks() {
        daySelectedCallback = null
    }

    override fun getItemCount() = items.size

    inner class WeekViewHolder(
        private val binding: ItemWeekBinding,
        val chartWeek: BarChart = binding.weekChart,
        val performanceIncreased: ImageView = binding.performanceIncreased,
        val performanceDecreased: ImageView = binding.performanceDecreased,
        private val weekOverall: AppCompatTextView = binding.weekOverallTime
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(statsWeek: StatsWeek) {
            initChart()
            initBarChartListener(statsWeek)
            updateBarChart(statsWeek)
            showNoDataLabel(statsWeek)
            showPerformance(statsWeek)
        }

        private fun initChart() {
            chartWeek.apply {
                renderer = RoundedBarChartRenderer(this, animator, viewPortHandler, 12f)
                setVisibleXRangeMaximum(7f)
                disableBarChartDefaults()
                hideBarChartAxises()
                setBarChartOffsets(top = 8 , bottom = 32)
                setBottomAxisFontSize(textSize = 12f)
            }
        }

        private fun initBarChartListener(statsWeek: StatsWeek) {
            chartWeek.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
                override fun onNothingSelected() {
                    daySelectedCallback?.invoke(null)
                }

                override fun onValueSelected(e: Entry?, h: Highlight?) {
                    // highlight inverse:
                    if (e != null && e.x >= 0 && e.y > 0) {
                        val invertedHighlights = createInverseHighlights(e.x.toInt(), WEEK_LENGTH - 1)
                        chartWeek.highlightValues(invertedHighlights)
                        // load selected day (day, time, tasks)
                        daySelectedCallback?.invoke(statsWeek.days[e.x.toInt()])
                    } else {
                        daySelectedCallback?.invoke(null)
                    }
                }
            })
        }

        private fun updateBarChart(statsWeek: StatsWeek, animate: Boolean = true) {
            statsWeek.apply {
                chartWeek.data = statsWeek.weekBarData
                if (animate) {
                    chartWeek.animateY(500)
                }

                // max and average of week:
                chartWeek.drawBarChartLimitLines(statsWeek.weekBarData, statsWeek.averageHours, statsWeek.overallWeekTime, typeface!!, fragment.context)

                // highlight current day of week:
                chartWeek.initBarChartDaysLabels(statsWeek.isCurrentWeek, statsWeek.daysNames, 1..7, DateTime().dayOfWeek)

                chartWeek.invalidate()
                weekOverall.text = "${fragment.context?.getString(R.string.total) ?: "Total"} ${formatTimeToString(statsWeek.overallWeekTime)}"
            }
        }

        private fun showNoDataLabel(statsWeek: StatsWeek) {
            if (statsWeek.overallWeekTime > 0) {
                binding.weekNoTime.visibility = View.GONE
            } else {
                binding.weekNoTime.visibility = View.VISIBLE
            }
        }

        private fun showPerformance(statsWeek: StatsWeek){
            if (statsWeek.previousWeekTime > 0 && statsWeek.overallWeekTime > 0){
                if (statsWeek.overallWeekTime > statsWeek.previousWeekTime){
                    performanceDecreased.visibility = View.GONE
                    performanceIncreased.visibility = View.VISIBLE
                } else {
                    performanceIncreased.visibility = View.GONE
                    performanceDecreased.visibility = View.VISIBLE
                }
            } else{
                performanceIncreased.visibility = View.GONE
                performanceDecreased.visibility = View.GONE
            }
        }

        fun removeHighlight() {
            chartWeek.highlightValue(null)
        }
    }
}
