package com.fuzzythread.tyming.global.utils.extensions

import android.content.Intent

val Intent.intentText: String?
    get() {
        return data?.toString() ?: getStringExtra(Intent.EXTRA_TEXT)
    }