package com.fuzzythread.tyming.global.utils.logs

import timber.log.Timber

class DebugTree : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, throwable: Throwable?) {}
}