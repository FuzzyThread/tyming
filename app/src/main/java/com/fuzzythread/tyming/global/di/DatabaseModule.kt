package com.fuzzythread.tyming.global.di

import com.fuzzythread.tyming.global.db.AppDatabase
import android.content.Context
import com.fuzzythread.tyming.global.services.syncing.DatabaseManager
import com.fuzzythread.tyming.global.services.syncing.LocalDbSnapshotFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [DaoModule::class])
class DatabaseModule {
    @Provides
    @Singleton
    fun provideDatabase(context: Context): AppDatabase {
        return AppDatabase(context)
    }
}
