package com.fuzzythread.tyming.global.di

import com.fuzzythread.tyming.global.TymingApplication
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    ApplicationModule::class,
    AndroidBindingModule::class,
    AndroidSupportInjectionModule::class,
    TimerModule::class,
    DatabaseModule::class,
    DaoModule::class,
    DatabaseManagerModule::class,
    NotificationModule::class,
    SyncModule::class
])

interface AppComponent : AndroidInjector<TymingApplication> 
