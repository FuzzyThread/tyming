package com.fuzzythread.tyming.screens.search.ui

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.ViewModelProvider
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.databinding.ActivitySearchBinding
import com.fuzzythread.tyming.global.TymingActivity
import com.fuzzythread.tyming.global.TymingService
import com.fuzzythread.tyming.global.di.ViewModelFactory
import com.fuzzythread.tyming.global.services.TimerService
import com.fuzzythread.tyming.global.utils.other.Constants
import com.fuzzythread.tyming.global.utils.other.PrefUtil
import com.fuzzythread.tyming.global.utils.ui.hideKeyboard
import com.fuzzythread.tyming.global.utils.ui.scrollState
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.adapter.NavigationAdapter
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.adapter.Refreshable
import com.fuzzythread.tyming.screens.home.model.NavigationItem
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.TaskViewModel
import com.fuzzythread.tyming.screens.search.model.SearchRepository
import kotlinx.coroutines.*
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class SearchActivity : TymingActivity(), CoroutineScope, Refreshable {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private val repository: SearchRepository by lazy {
        ViewModelProvider(this, viewModelFactory).get(SearchRepository::class.java)
    }

    private var masterJob = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + masterJob

    private var updateTimers: Boolean = true
    private var searchQuery: String = ""
    private var navigationItems: MutableList<NavigationItem<*>> = mutableListOf()
    private var navigationAdapter: NavigationAdapter? = null
    private var queryTextChangedJob: Job? = null
    private var timerService: TimerService? = null
    private var timerServiceConnection: ServiceConnection? = null
    private var isTimerServiceBound: Boolean = false
    private lateinit var binding: ActivitySearchBinding

    // region Android hooks
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setTopBarPadding(binding.topBar)
        initialize(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.tag(Constants.APP_TAG).d("Search activity destroyed: unbind timer service")
        masterJob.cancel()
        timerService?.unbindTimersCallback()
        queryTextChangedJob?.cancel()
        unbindService(timerServiceConnection, isTimerServiceBound)
    }

    override fun onResume() {
        super.onResume()
        isTimerServiceBound = false
        refreshAsync()
    }
    // endregion

    // region Services:
    private fun unbindService(serviceConnection: ServiceConnection?, serviceBound: Boolean) {
        if (serviceConnection != null && serviceBound) {
            try {
                unbindService(serviceConnection)
            } catch (ignore: Exception) {
                println(ignore)
            }
        }
    }

    private fun bindTimerService() {
        timerServiceConnection = object : ServiceConnection {
            override fun onServiceConnected(componentName: ComponentName?, iBinder: IBinder?) {
                // bind service:
                if (timerService == null) {
                    timerService = (iBinder as TymingService.LocalBinder).service as TimerService
                }

                launch(Dispatchers.Default) {
                    delay(1000)
                    navigationAdapter?.setTimerService(timerService!!)
                    timerService?.bindOnTimersCount(1000L) { timers ->
                        if (timers.isNotEmpty()) {
                            timers.forEach { timer ->
                                // update items:
                                val navTask = navigationAdapter?.items?.find { it.isTask() && (it.getItemEntity() as TaskViewModel).id == timer.key }
                                if (navTask != null && updateTimers) {
                                    runOnUiThread {
                                        (navTask.getItemEntity() as TaskViewModel).timeElapsed = timer.value
                                    }
                                }
                            }
                        }
                    }
                }
            }

            override fun onServiceDisconnected(componentName: ComponentName?) {
                timerService = null
                isTimerServiceBound = false
                Timber.tag(Constants.APP_TAG).v("Timer service disconnected")
            }
        }
        if (!isTimerServiceBound) {
            bindService(Intent(baseContext, TimerService::class.java), timerServiceConnection!!, Context.BIND_AUTO_CREATE)
        }
        isTimerServiceBound = true
    }
    // endregion

    // region Data
    override fun refreshSelf(refreshTimers: Boolean) {
        refreshAsync()
    }

    override fun refreshAll() {
        refreshAsync()
    }

    private fun refreshAsync() {
        // reset previous job:
        masterJob.cancel()
        masterJob = Job()

        launch(this.coroutineContext) {
            val refresh = async {
                refreshData()
            }
            refresh.await()
            withContext(Dispatchers.Main) {
                hideProgress()
                navigationAdapter?.refreshAdapterWithData(navigationItems)
                if (!isTimerServiceBound) {
                    bindTimerService()
                }
            }
        }
    }

    private fun refreshData() {
        if (this::viewModelFactory.isInitialized) {
            updateNavItems()
        }
    }

    fun onQueryTextChange(query: String) {
        queryTextChangedJob?.cancel()
        queryTextChangedJob = launch(Dispatchers.Main) {
            delay(500)
            performSearch(query)
        }
    }

    private fun performSearch(query: String) {
        val sortBy = PrefUtil.getSortByProjects(this)
        navigationItems = if (query.isNotEmpty()) {
            val items = repository.searchNavigationData(query, sortBy)
            if (items.isEmpty()) {
                binding.emptyStage.visibility = View.VISIBLE
            } else {
                binding.emptyStage.visibility = View.GONE
            }
            items
        } else {
            binding.emptyStage.visibility = View.GONE
            repository.getNavigationData(isArchived = false, sortBy = sortBy)
        }
        navigationAdapter?.refreshAdapterWithData(navigationItems)
    }
    // endregion

    // region UI
    private fun initialize(savedInstanceState: Bundle?) {
        initializeUI()
    }

    private fun initializeUI() {
        navigationAdapter = NavigationAdapter(this, this, repository, isArchive = false)
        binding.rv.adapter = navigationAdapter

        binding.searchField.requestFocus()
        val imm: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(binding.searchField, InputMethodManager.SHOW_IMPLICIT)

        binding.searchField.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                searchQuery = s.toString()
                onQueryTextChange(s.toString() ?: "")
            }
        })

        binding.backBtn.setOnClickListener {
            hideKeyboard()
            finish()
        }

        binding.nestedScrollHome.isFocusableInTouchMode = true
        binding.nestedScrollHome.descendantFocusability = ViewGroup.FOCUS_BEFORE_DESCENDANTS

        binding.nestedScrollHome.scrollState(idle = {
            updateTimers = true
        }, scrolled = {
            updateTimers = false
            hideKeyboard()
        })
    }

    private fun updateNavItems() {
        val sortBy = PrefUtil.getSortByProjects(this)
        navigationItems = if (this.searchQuery.isNotEmpty()) {
            repository.searchNavigationData(this.searchQuery, sortBy = sortBy)
        } else {
            repository.getNavigationData(isArchived = false, sortBy = sortBy)
        }
    }

    private fun hideProgress() {
        binding.progressBar.visibility = View.GONE
    }
    // endregion

    companion object {
        fun intent(context: Context): Intent {
            return Intent(context, SearchActivity::class.java)
        }
    }
}

