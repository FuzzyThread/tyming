package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.adapter

interface Refreshable {
    fun refreshSelf(refreshTimers: Boolean = false)
    fun refreshAll()
}
