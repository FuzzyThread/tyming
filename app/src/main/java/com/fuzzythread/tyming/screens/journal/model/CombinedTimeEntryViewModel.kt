package com.fuzzythread.tyming.screens.journal.model

import android.content.Context
import androidx.databinding.Bindable
import com.fuzzythread.tyming.BR
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.global.ObservableViewModel
import com.fuzzythread.tyming.global.db.entities.AreaEntity
import com.fuzzythread.tyming.global.db.entities.ProjectEntity
import com.fuzzythread.tyming.global.db.entities.TaskEntity
import com.fuzzythread.tyming.global.db.entities.interfaces.ITaskParent
import com.fuzzythread.tyming.global.ui.helpers.IAdapterComparable
import com.fuzzythread.tyming.global.utils.other.DisplayMode
import com.fuzzythread.tyming.global.utils.other.ParentType
import com.fuzzythread.tyming.global.utils.time.formatTimeStartEnd
import com.fuzzythread.tyming.global.utils.time.formatTimeToString
import org.joda.time.DateTime
import org.joda.time.Period


class CombinedTimeEntryViewModel(val context: Context) :
        ObservableViewModel(),
        IAdapterComparable<CombinedTimeEntryViewModel> {

    // region Database Model Attributes

    var id: Long = 0

    var timeEntriesIds = mutableListOf<Long>()
    var displayMode = DisplayMode.CLUSTER
        set(value) {
            field = value
            notifyPropertyChanged(BR.displayCluster)
        }

    @get:Bindable
    var notes: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.notes)
        }

    @get:Bindable
    var location: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.location)
        }

    // endregion

    // region Dates

    var endDate: DateTime = DateTime()
    var startDate: DateTime = DateTime()

    var timeTracked: Period = Period()
        set(value) {
            field = value
            notifyPropertyChanged(BR.timeTrackedFormatted)
        }

    @get:Bindable
    var timeTrackedFormatted: String = ""
        get() {
            return when (displayMode) {
                DisplayMode.CLUSTER -> {
                    formatTimeToString(timeTracked)
                }
                DisplayMode.STARTEND -> {
                    formatTimeStartEnd(startDate, endDate)
                }
            }
        }

    @get:Bindable
    val displayCluster: Boolean
        get() {
            return displayMode == DisplayMode.CLUSTER
        }

    var hasDeadline: Boolean = false
    var hasTags: Boolean = false

    // endregion

    // region Relations

    var task: TaskEntity? = null
        set(value) {
            field = value
            if (value != null) {
                taskName = value.title
            }
        }

    @get:Bindable
    var taskName: String = context.getString(R.string.task)
        set(value) {
            field = value
            notifyPropertyChanged(BR.taskName)
        }

    var taskParent: ITaskParent? = null
        set(value) {
            field = value
            if (value != null) {
                taskParentType = value.getParentType()
                when (taskParentType) {
                    ParentType.PROJECT -> {
                        with(value as ProjectEntity) {
                            taskParentName = title
                            taskParentColor = color
                            taskParentId = id
                        }
                    }
                    ParentType.AREA -> {
                        with(value as AreaEntity) {
                            taskParentName = title
                            taskParentColor = color
                            taskParentId = id
                        }
                    }
                    else -> {
                    }
                }
            } else {
                taskParentId = 0
                taskParentType = ParentType.NONE
                taskParentName = context.getString(R.string.agenda)
                taskParentColor = "#1e88e5"
            }
        }

    @get:Bindable
    var taskParentType: ParentType = ParentType.NONE
        set(value) {
            field = value
            notifyPropertyChanged(BR.parentType)
        }

    @get:Bindable
    var taskParentId: Long = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.taskParentId)
        }

    @get:Bindable
    var taskParentColor: String = "#1e88e5"
        get() {
            if (field.isEmpty()) {
                return "#1e88e5"
            }
            return field
        }
        set(value) {
            field = value
            notifyPropertyChanged(BR.taskParentColor)
        }

    @get:Bindable
    var taskParentName: String = context.getString(R.string.project)
        set(value) {
            field = value
            notifyPropertyChanged(BR.taskParentName)
        }

    // endregion

    // region DiffUtil Functions

    override fun areItemsTheSame(itemToCompare: CombinedTimeEntryViewModel): Boolean {
        return itemToCompare.id == id
    }

    override fun areContentsTheSame(itemToCompare: CombinedTimeEntryViewModel): Boolean {
        return itemToCompare.notes == notes &&
                itemToCompare.taskName == taskName
    }

    // endregion
}
