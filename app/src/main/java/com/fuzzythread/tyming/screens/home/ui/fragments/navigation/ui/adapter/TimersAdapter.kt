package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.adapter

import android.content.Context
import android.util.TypedValue
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.fuzzythread.tyming.databinding.ItemTimerBinding
import com.fuzzythread.tyming.global.services.TimerService
import com.fuzzythread.tyming.global.utils.extensions.updateAdapterWithData
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.HomeFragmentRepository
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.TimerViewModel
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.NavigationFragment

open class TimersAdapter(var items: MutableList<TimerViewModel>,
                         var repository: HomeFragmentRepository,
                         var fragment:NavigationFragment,
                         var context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var timerService: TimerService? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemBinding = ItemTimerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TimerViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        with(holder as TimerViewHolder) {
            bind(items[position], position)
        }
    }

    fun setTimerService(timerService: TimerService) {
        this.timerService = timerService
    }

    fun removeItemAt(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, items.size)
    }

    fun refreshAdapterWithData(newTimers: MutableList<TimerViewModel>) {
        val taskViewModelsOld = items.toMutableList()
        items = newTimers
        updateAdapterWithData(taskViewModelsOld, items)
    }

    fun refresh() {
        items = repository.getTimersVMs()
        if (items.size == 0){
            fragment.updateTimersVisibility()
        }
        refreshAdapterWithData(items)
    }

    private fun convertDpToPx(dp: Int): Int {
        return TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp.toFloat(),
                context.resources.displayMetrics
        ).toInt()
    }

    override fun getItemCount() = items.size

    inner class TimerViewHolder(
            private val itemBinding: ItemTimerBinding,
            private val view: View = itemBinding.root
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        var timerVM: TimerViewModel? = null

        fun bind(timerViewModel: TimerViewModel, position: Int) {
            timerVM = timerViewModel

            // bind repository:
            itemBinding.viewmodel = timerViewModel

            // margin for first and last item
            val params = ConstraintLayout.LayoutParams(
                    ConstraintLayout.LayoutParams.MATCH_PARENT,
                    ConstraintLayout.LayoutParams.MATCH_PARENT
            )
            if (position == 0) {
                params.setMargins(convertDpToPx(12), 0, convertDpToPx(16), 0)
            } else if (position == itemCount - 1) {
                params.setMargins(convertDpToPx(16), 0, convertDpToPx(12), 0)
            } else {
                params.setMargins(convertDpToPx(16), 0, convertDpToPx(16), 0)
            }
            if (itemCount == 1) {
                params.setMargins(convertDpToPx(12), 0, convertDpToPx(12), 0)
            }
            view.layoutParams = params

            itemBinding.weekBackBtn.setOnClickListener {
                repository.stopTaskTimer(timerViewModel.id)
                refresh()
                fragment.refreshAll()
                notifyDataSetChanged()
                timerService?.stopTimer(timerViewModel.id)
            }
        }
    }
}
