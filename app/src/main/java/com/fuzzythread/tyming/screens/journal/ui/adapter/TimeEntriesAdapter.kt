package com.fuzzythread.tyming.screens.journal.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.databinding.ItemDayBinding
import com.fuzzythread.tyming.databinding.ItemTimeEntryBinding
import com.fuzzythread.tyming.global.ui.helpers.ItemTouchHelperAdapter
import com.fuzzythread.tyming.global.utils.extensions.updateAdapterWithData
import com.fuzzythread.tyming.global.utils.other.ActionType
import com.fuzzythread.tyming.global.utils.other.EntityType
import com.fuzzythread.tyming.screens.journal.model.CombinedTimeEntryViewModel
import com.fuzzythread.tyming.screens.journal.model.DayViewModel
import com.fuzzythread.tyming.screens.journal.model.JournalRepository
import com.fuzzythread.tyming.screens.journal.model.TimeLineItem
import com.fuzzythread.tyming.screens.journal.ui.JournalFragment
import com.fuzzythread.tyming.screens.home.ui.dialog.NavigationItemDialog
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.ConfirmationDialog
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.TimeEntryDialog
import java.util.*

class TimeEntriesAdapter(
        private val repository: JournalRepository,
        private val context: Context,
        private val journalFragment: JournalFragment
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), ItemTouchHelperAdapter {

    private var items: MutableList<TimeLineItem<*>> = mutableListOf()
    private lateinit var recyclerView: RecyclerView
    private var deletedItem: TimeLineItem<*>? = null
    private var deletedItemPosition: Int = 0

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
        this.recyclerView.layoutManager = LinearLayoutManager(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TimeLineItem.DAY -> {
                val itemBinding = ItemDayBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                DayViewHolder(itemBinding)
            }
            else -> {
                val itemBinding = ItemTimeEntryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                TimeEntryViewHolder(itemBinding)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item: TimeLineItem<*> = items[position]
        with(item) {
            when (this.getItemType()) {
                TimeLineItem.TIMEENTRY -> {
                    with(holder as TimeEntryViewHolder) {
                        bind(items[position])
                    }
                }
                TimeLineItem.DAY -> {
                    with(holder as DayViewHolder) {
                        bind(items[position])
                    }
                }
            }
        }
    }

    override fun onItemDismiss(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
        Collections.swap(items, fromPosition, toPosition)
        notifyItemMoved(fromPosition, toPosition)
        return true
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        when (items.get(position).getItemType()) {
            1 -> return TimeLineItem.TIMEENTRY
            2 -> return TimeLineItem.DAY
            else -> return -1
        }
    }

    fun refresh(timeLineItems: MutableList<TimeLineItem<*>>) {
        val itemsOld = items.toMutableList()
        this.items = timeLineItems
        updateAdapterWithData(itemsOld, timeLineItems)
    }

    fun removeItemAt(position: Int) {
        // show dismiss dialog
        val dialog = ConfirmationDialog.getInstance(ActionType.DELETE, EntityType.TIMERECORD)
        dialog.setConfirmationCallback { isConfirmed ->
            if (isConfirmed) {
                Toast.makeText(this.context, context.getString(R.string.time_record_deleted), Toast.LENGTH_LONG).show()
                deletedItem = items[position]
                deletedItemPosition = position
                items.removeAt(position)
                notifyItemRemoved(position)
                notifyItemRangeChanged(position, items.size)
                repository.removeTimeLineItem(deletedItem!!)
                journalFragment.refreshAsync()
            }
        }
        dialog.show(journalFragment.requireActivity().supportFragmentManager, "Delete Time Record")
    }

    private fun editItemAt(position: Int) {
        if (getItemViewType(position) == TimeLineItem.TIMEENTRY) {
            val combinedTimeEntryVM = items[position].getItemEntity() as CombinedTimeEntryViewModel
            repository.combineTimeEntries(combinedTimeEntryVM)
            val dialog = TimeEntryDialog.getInstance(taskId = combinedTimeEntryVM.task?.id
                    ?: 0, timeEntryId = combinedTimeEntryVM.id)
            dialog.setDismissCallBack {
                journalFragment.refreshAsync()
                journalFragment.updateActivityVP()
            }
            dialog.show(journalFragment.requireActivity().supportFragmentManager, "Delete Time Entry")
        }
    }

    inner class DayViewHolder(
            private val itemBinding: ItemDayBinding,
            private val view: View = itemBinding.root
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(timeLineItem: TimeLineItem<*>) {

            val dayVM = timeLineItem.getItemEntity() as DayViewModel

            // bind repository:
            itemBinding.viewmodel = dayVM
            itemBinding.executePendingBindings()
        }
    }

    inner class TimeEntryViewHolder(
            private val itemBinding: ItemTimeEntryBinding,
            private val view: View = itemBinding.root
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(timeLineItem: TimeLineItem<*>) {
            val timeEntryVM = timeLineItem.getItemEntity() as CombinedTimeEntryViewModel
            itemBinding.viewmodel = timeEntryVM
            itemBinding.executePendingBindings()

            itemBinding.timeDialog.setOnClickListener {
                NavigationItemDialog()
                        .setDialogConfiguration(entityType = EntityType.TIMERECORD)
                        .setDeleteCallBack {
                            removeItemAt(layoutPosition)
                        }
                        .setEditCallBack {
                            editItemAt(layoutPosition)
                        }
                        .show(journalFragment.requireActivity().supportFragmentManager, "Combined Item Dialog")
            }

            if (timeEntryVM.notes.trim().isNotEmpty()) {
                itemBinding.notesTv.visibility = View.VISIBLE
            } else {
                itemBinding.notesTv.visibility = View.GONE
            }
        }
    }
}
