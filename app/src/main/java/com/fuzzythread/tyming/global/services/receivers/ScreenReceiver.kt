package com.fuzzythread.tyming.global.services.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import com.fuzzythread.tyming.global.services.TimerService
import com.fuzzythread.tyming.global.utils.other.Constants
import com.fuzzythread.tyming.global.utils.other.Constants.Companion.APP_TAG
import com.fuzzythread.tyming.global.utils.other.PrefUtil
import timber.log.Timber


class ScreenReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        try {
            Timber.tag(APP_TAG).v("Screen broadcast received")
            Intent(context, TimerService::class.java).apply {
                action = if (intent.action == Intent.ACTION_SCREEN_ON) {
                    Timber.tag(APP_TAG).v("The screen is on")
                    Constants.ACTION.SCREEN_ON
                } else {
                    Timber.tag(APP_TAG).v("The screen is off")
                    Constants.ACTION.SCREEN_OFF
                }
            }.apply {
                context.startService(this)
            }
        } catch (e: Exception) {
            Timber.e("Screen receiver error")
        }
    }
}
