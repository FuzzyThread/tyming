package com.fuzzythread.tyming.global.ui.charts

import android.content.Context
import android.graphics.Canvas
import android.view.View
import android.widget.TextView
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.global.utils.time.formatTimeToString
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF

class CustomMarkerView(context: Context?, layoutResource: Int) : MarkerView(context, layoutResource) {
    private val tvContent: TextView
    private var mOffset: MPPointF? = null

    // callbacks every time the MarkerView is redrawn, can be used to update the content (user-interface)
    override fun refreshContent(e: Entry, highlight: Highlight) {
        tvContent.text = formatTimeToString(e.y)
        // this will perform necessary layout
        super.refreshContent(e, highlight)
    }

//    override fun getOffset(): MPPointF {
//        if (mOffset == null) {
//            // center the marker horizontally and vertically
//            mOffset = MPPointF((-(width / 2)).toFloat(), (-(height - 20)).toFloat())
//        }
//        return mOffset!!
//    }

    override fun draw(canvas: Canvas?, posX: Float, posY: Float) {
        super.draw(canvas, posX, posY)
        getOffsetForDrawingAtPoint(posX, posY)
    }

    init {
        // find your layout components
        tvContent = findViewById<View>(R.id.tvContent) as TextView
    }
}
