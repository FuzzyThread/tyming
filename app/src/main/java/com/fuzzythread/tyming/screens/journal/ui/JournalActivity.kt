package com.fuzzythread.tyming.screens.journal.ui

import android.os.Bundle
import android.view.View
import android.view.ViewTreeObserver
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.databinding.ActivityJournalBinding
import com.fuzzythread.tyming.databinding.SpotlightTargetBinding
import com.fuzzythread.tyming.global.TymingActivity
import com.fuzzythread.tyming.global.di.ViewModelFactory
import com.fuzzythread.tyming.global.utils.other.*
import com.fuzzythread.tyming.global.utils.ui.*
import com.fuzzythread.tyming.screens.journal.model.JournalRepository
import com.fuzzythread.tyming.screens.journal.ui.adapter.FragmentLifecycle
import com.fuzzythread.tyming.screens.journal.ui.dialog.FiltersDialog
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.TimeEntryDialog
import com.takusemba.spotlight.Target
import kotlinx.coroutines.*
import org.joda.time.DateTime
import java.util.ArrayList
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class JournalActivity : TymingActivity(), CoroutineScope {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private val repository: JournalRepository by lazy {
        ViewModelProvider(this, viewModelFactory).get(JournalRepository::class.java)
    }

    var masterJob: Job = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + masterJob

    private var displayMode: DisplayMode = DisplayMode.CLUSTER
    private var sortBy: SortBy = SortBy.DESC
    private var isPageChanged: Boolean = false
    private var weeks: MutableList<Pair<DateTime, String>>? = null
    private var months: MutableList<Pair<DateTime, String>>? = null
    private var timeFrame: TimeFrame = TimeFrame.WEEK
    private var newPos: Int = 0
    private var fragments: MutableList<JournalFragment> = mutableListOf()
    private var adapter: ScreenSlidePagerAdapter? = null
    private lateinit var binding: ActivityJournalBinding

    // region Android hooks
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityJournalBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setTopBarPadding(binding.topBar)
        initialize()
    }

    override fun onDestroy() {
        masterJob.cancel()
        super.onDestroy()
    }

    // endregion

    // region Data
    private fun updateFilters(binding: ActivityJournalBinding) {
        val oldTimeFrame = timeFrame
        val oldSortBy = sortBy
        val oldDisplayMode = displayMode
        loadFilters()

        if (oldSortBy != sortBy || oldDisplayMode != displayMode || oldTimeFrame != timeFrame) {
            if (oldTimeFrame != timeFrame) {
                reloadAllFragmentsAsync()
            } else {
                updateFragmentsContentAsync()
            }
        }
    }

    private fun updateTimeFrames(): Boolean {
        val oldWeekSize = weeks?.size
        val oldMonthsSize = months?.size

        // update week and months:
        weeks = repository.getWeeks()
        months = repository.getMonths()

        // check if frames size changed:
        return oldWeekSize != weeks?.size || oldMonthsSize != months?.size
    }

    private fun loadFilters() {
        timeFrame = PrefUtil.getTimeFrame(this)
        sortBy = PrefUtil.getSortByJournal(this)
        displayMode = PrefUtil.getDisplayMode(this)
    }

    private fun loadFragments() {
        if (timeFrame == TimeFrame.WEEK) {
            val weekFragments = mutableListOf<JournalFragment>()
            weeks?.forEachIndexed { index, pair ->
                val fragment = JournalFragment.getInstance(timeFrame, sortBy, displayMode, pair.first, pair.second)
                if (index == weeks!!.size - 1) {
                    fragment.isCurrentTimeFrame = true
                }
                weekFragments.add(fragment)
            }
            fragments = weekFragments
        } else if (timeFrame == TimeFrame.MONTH) {
            val monthFragments = mutableListOf<JournalFragment>()
            months?.forEachIndexed { index, pair ->
                val fragment = JournalFragment.getInstance(timeFrame, sortBy, displayMode, pair.first, pair.second)
                if (index == months!!.size - 1) {
                    fragment.isCurrentTimeFrame = true
                }
                monthFragments.add(fragment)
            }
            fragments = monthFragments
        }
    }

    private fun reloadAllFragmentsAsync() {
        // reset previous job:
        masterJob.cancel()
        masterJob = Job()

        launch(this.coroutineContext) {
            async { loadFragments() }.await()
            withContext(Dispatchers.Main) {
                adapter?.notifyDataSetChanged()
                if (fragments.size > 0) {
                    binding.journalVp.setCurrentItem(fragments.size - 1, false)
                }
            }
        }
    }

    fun updateFragmentsContentAsync() {
        fragments.filter { it.isFragmentResumed }.forEach {
            it.refreshAsync()
        }
    }

    fun refresh() {
        if (updateTimeFrames()) {
            reloadAllFragmentsAsync()
        } else {
            updateFragmentsContentAsync()
        }
    }
    // endregion

    // region UI
    private fun initialize() {
        updateTimeFrames()
        loadFilters()
        loadFragments()
        // UI:
        initButtons()
        initViewPager()
        showTutorial()
    }

    private fun initButtons() {
        binding.fab.setOnClickListener {
            val dialog = TimeEntryDialog.getInstance()
            dialog.setDismissCallBack {
                refresh()
            }
            dialog.show(this.supportFragmentManager, "Add TimeEntry")
        }

        binding.backBtn.setOnClickListener {
            finish()
        }

        binding.journalDialog.setOnClickListener {
            val dialog = FiltersDialog.getInstance(activityType = ActivityType.JOURNAL)
            dialog.setDismissCallBack {
                updateFilters(binding)
            }
            dialog.show(this.supportFragmentManager, "Journal Dialog")
        }

        binding.today.setOnClickListener {
            binding.journalVp.setCurrentItem(fragments.size - 1, true)
            binding.today.visibility = View.GONE
        }
    }

    private fun initViewPager() {
        adapter = ScreenSlidePagerAdapter(supportFragmentManager)
        binding.journalVp.adapter = adapter
        binding.journalVp.offscreenPageLimit = 10
        binding.journalVp.setCurrentItem(fragments.size - 1, false)
        binding.journalVp.addOnPageChangeListener(object : OnPageChangeListener {
            var currentPosition = 0
            override fun onPageSelected(newPosition: Int) {
                isPageChanged = true
                newPos = newPosition

                // scroll to today fab button:
                if (newPosition != fragments.size - 1) {
                    binding.today.visibility = View.VISIBLE
                } else {
                    binding.today.visibility = View.GONE
                }
            }

            override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}
            override fun onPageScrollStateChanged(state: Int) {
                when (state) {
                    ViewPager.SCROLL_STATE_IDLE -> if (isPageChanged) {
                        val fragmentToShow: FragmentLifecycle = adapter?.getItem(newPos) as FragmentLifecycle
                        fragmentToShow.onResumeFragment()
                        val fragmentToHide: FragmentLifecycle = adapter?.getItem(currentPosition) as FragmentLifecycle
                        fragmentToHide.onPauseFragment()
                        currentPosition = newPos
                        isPageChanged = false
                    }
                    ViewPager.SCROLL_STATE_DRAGGING -> {
                    }
                    ViewPager.SCROLL_STATE_SETTLING -> {
                    }
                }
            }
        })
    }
    // endregion

    // region Tutorial
    private fun showTutorial() {
        if (!isTutorialCompleted(TUTORIAL.JOURNAL)) {
            binding.journalVp.viewTreeObserver?.addOnGlobalLayoutListener(
                    object : ViewTreeObserver.OnGlobalLayoutListener {
                        override fun onGlobalLayout() {
                            binding.journalVp.viewTreeObserver?.removeOnGlobalLayoutListener(this)
                            // RV fully loaded:
                            startTutorial(binding)
                        }
                    }
            )
        }
    }

    private fun startTutorial(binding: ActivityJournalBinding) {
        val targets = createTutorialTargets(binding)
        val spotlight = createSpotlight(targets) {
            markTutorialCompleted(TUTORIAL.JOURNAL)
        }
        spotlight.start()
        addSpotLightListeners(spotlight, targets)
    }

    private fun createTutorialTargets(binding: ActivityJournalBinding): ArrayList<Target> {
        val targets = arrayListOf<Target>()
        val target1 = createTarget(
                anchorView = findViewById<View>(R.id.journal_dialog),
                shapeRadius = 30,
                title = getString(R.string.filters_alt),
                description = getString(R.string.change_sorting_by_date)
        )
        targets.add(target1)

        val location = IntArray(2)
        binding.journalVp.getLocationInWindow(location)
        val x = location[0] + binding.journalVp.width - convertDpToPx(this, 36)
        val y = location[1] + convertDpToPx(this, 100)
        val target2 = createTarget(
                x = x.toFloat(),
                y = y.toFloat(),
                shapeRadius = 30,
                title = getString(R.string.edit_time_record),
                description = getString(R.string.click_to_update_time_record)
        )
        targets.add(target2)

        val target3 = createTarget(
                anchorView = findViewById<View>(R.id.fab),
                shapeRadius = 50,
                title = getString(R.string.add_time_record),
                description = getString(R.string.timerecord_click_to_open)
        )

        target3.overlay?.apply {
            val bindingSpotlight = SpotlightTargetBinding.bind(this)
            bindingSpotlight.nextBtn.text = context.getString(R.string.finish)
            bindingSpotlight.nextBtn.setMarginBottom(240)
        }

        targets.add(target3)

        return targets
    }
    // endregion

    private inner class ScreenSlidePagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
        override fun getItemPosition(`object`: Any): Int = PagerAdapter.POSITION_NONE
        override fun getCount(): Int = fragments.size
        override fun getItem(position: Int): Fragment {
            return if (position < fragments.size) {
                fragments[position]
            } else {
                JournalFragment()
            }
        }
    }
}
