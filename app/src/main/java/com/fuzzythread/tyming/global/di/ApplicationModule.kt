package com.fuzzythread.tyming.global.di

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val applicationContext: Context) {
    @Provides
    fun provideApplicationContext() = applicationContext
}