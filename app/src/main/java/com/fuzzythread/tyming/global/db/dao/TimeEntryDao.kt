package com.fuzzythread.tyming.global.db.dao

import com.fuzzythread.tyming.global.db.dao.util.Dao
import com.fuzzythread.tyming.global.db.entities.TimeEntryEntity
import com.fuzzythread.tyming.global.db.entities.TimeEntryEntity_
import com.fuzzythread.tyming.global.utils.extensions.endOfDay
import com.fuzzythread.tyming.global.utils.time.clearTime
import com.fuzzythread.tyming.screens.journal.model.CombinedTimeEntryViewModel
import io.objectbox.BoxStore
import org.joda.time.DateTime
import java.util.*

class TimeEntryDao(store: BoxStore) : Dao<TimeEntryEntity>(store) {

    override fun addOperation(entity: TimeEntryEntity): Long {
        return timeEntryBox.put(entity)
    }

    override fun updateOperation(vararg entity: TimeEntryEntity) {
        store.runInTx {
            timeEntryBox.put(*entity)
        }
    }

    override fun deleteOperation(entity: TimeEntryEntity) {
        timeEntryBox.remove(entity)
    }

    override fun deleteByIdOperation(id: Long) {
        timeEntryBox.remove(id)
    }

    fun deleteByIds(timeEntriesIds: MutableList<Long>) {
        updateVersion()
        timeEntryBox.removeByKeys(timeEntriesIds)
    }

    override fun get(id: Long): TimeEntryEntity? {
        return timeEntryBox.get(id)
    }

    override fun getAll(): MutableList<TimeEntryEntity> {
        return timeEntryBox.all
    }

    override fun amount(): Long {
        return timeEntryBox.count()
    }

    fun getTimeEntriesByDay(date: DateTime, isArchived: Boolean = false): MutableList<TimeEntryEntity> {
        val dateCleared = date.clearTime()
        val dayStart = dateCleared.withTimeAtStartOfDay().toDate()
        val dayEnd = dateCleared.endOfDay().toDate()
        val query = timeEntryBox.query()

        return query
                .notNull(TimeEntryEntity_.startDate)
                .and()
                .notNull(TimeEntryEntity_.endDate)
                .and()
                .between(TimeEntryEntity_.startDate, dayStart, dayEnd)
                .orderDesc(TimeEntryEntity_.startDate)
                .build()
                .find()
    }

    fun getTimeEntriesByDates(startDate: DateTime, endDate: DateTime): MutableList<TimeEntryEntity> {
        val query = timeEntryBox.query()
        val startDateCleared = startDate.clearTime().withTimeAtStartOfDay().toDate()
        val endDateCleared = endDate.clearTime().endOfDay().toDate()
        return query
                .notNull(TimeEntryEntity_.startDate)
                .and()
                .notNull(TimeEntryEntity_.endDate)
                .and()
                .between(TimeEntryEntity_.endDate, startDateCleared, endDateCleared)
                .orderDesc(TimeEntryEntity_.endDate)
                .build()
                .find()
    }

    fun getFirstTimeEntry(): Date? {
        return timeEntryBox
                .query()
                .notNull(TimeEntryEntity_.startDate)
                .and()
                .notNull(TimeEntryEntity_.endDate)
                .order(TimeEntryEntity_.endDate)
                .build()
                .findFirst()?.endDate
    }

    fun getLastTimeEntry(): Date? {
        return timeEntryBox
                .query()
                .notNull(TimeEntryEntity_.startDate)
                .and()
                .notNull(TimeEntryEntity_.endDate)
                .orderDesc(TimeEntryEntity_.endDate)
                .build()
                .findFirst()?.endDate
    }

    fun combineTimeEntry(combinedTimeEntry: CombinedTimeEntryViewModel) {
        updateVersion()
        if (combinedTimeEntry.timeEntriesIds.size > 1) {
            timeEntryBox.get(combinedTimeEntry.id).apply {
                this.startDate = combinedTimeEntry.startDate.toDate()
                this.endDate = DateTime(this.startDate).plus(combinedTimeEntry.timeTracked).toDate()
                update(this)
                combinedTimeEntry.timeEntriesIds.subList(1, combinedTimeEntry.timeEntriesIds.size).apply {
                    deleteByIds(this)
                }
            }
        }
    }
}
