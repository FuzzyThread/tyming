package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.model

import android.content.Context
import com.fuzzythread.tyming.global.db.GeneralRepository
import com.fuzzythread.tyming.global.db.dao.AreaDao
import com.fuzzythread.tyming.global.db.dao.ProjectDao
import com.fuzzythread.tyming.global.db.dao.TaskDao
import com.fuzzythread.tyming.global.db.dao.TimeEntryDao
import com.fuzzythread.tyming.global.db.entities.TimeEntryEntity
import com.fuzzythread.tyming.global.utils.other.Constants
import com.fuzzythread.tyming.global.utils.time.isBefore
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.TimeEntryViewModel
import timber.log.Timber

class TimeEntryDialogRepository(
        areaDao: AreaDao,
        taskDao: TaskDao,
        private val projectDao: ProjectDao,
        private val timeEntryDao: TimeEntryDao,
        context: Context
) : GeneralRepository(areaDao, projectDao, taskDao, timeEntryDao, context) {

    fun getTasksData(): MutableList<Pair<Any, String>> {
        val data = mutableListOf<Pair<Any, String>>()
        projectDao.getAllSorted(false).forEach { project ->
            if (project.tasks.size > 0) {
                data.add(Pair(project, project.title))
                project.tasks.forEach {
                    data.add(Pair(it, "      ${it.title}"))
                }
            }
        }
        return data
    }

    fun updateTimeEntry(tVM: TimeEntryViewModel) {
        if (tVM.startDate != null && tVM.endDate != null && tVM.startDate!!.isBefore(tVM.endDate!!)) {
            timeEntryDao.update(
                    TimeEntryEntity(
                            notes = tVM.notes.capitalize(),
                            startDate = tVM.startDate,
                            endDate = tVM.endDate,
                            taskId = tVM.task?.id ?: 0
                    ).apply { id = tVM.id }
            )
        } else {
            Timber.tag(Constants.APP_TAG).e("Wrong dates supplied")
        }
    }

    fun addTimeEntry(tVM: TimeEntryViewModel) {
        if (tVM.startDate != null && tVM.endDate != null && tVM.startDate!!.isBefore(tVM.endDate!!)) {
            timeEntryDao.add(
                    TimeEntryEntity(
                            notes = tVM.notes.capitalize(),
                            startDate = tVM.startDate,
                            endDate = tVM.endDate,
                            taskId = tVM.task?.id ?: 0
                    )
            )
        } else {
            Timber.tag(Constants.APP_TAG).e("Wrong dates supplied")
        }
    }
}
