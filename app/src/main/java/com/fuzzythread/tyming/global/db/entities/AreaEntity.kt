package com.fuzzythread.tyming.global.db.entities

import com.fuzzythread.tyming.global.db.entities.interfaces.ITaskParent
import com.fuzzythread.tyming.global.utils.other.ParentType
import io.objectbox.annotation.Backlink
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import java.util.*

@Entity
class AreaEntity : ITaskParent {
    @Id(assignable = true)
    var id: Long = 0
    var title: String = ""
    var notes: String = ""
    var color: String = ""
    var isCollapsed: Boolean = false
    var createdDate: Date? = null
    var archivedDate: Date? = null
    var displayOrder: Long = 0

    // region relations
    @Backlink(to = "area")
    lateinit var projects: MutableList<ProjectEntity>
    // endregion

    constructor()

    constructor(
            title: String,
            notes: String,
            color: String = "",
            isCollapsed: Boolean = false,
            archivedDate: Date? = null,
            createdDate: Date = Date(),
            displayOrder: Long = 0
    ) {
        this.title = title
        this.notes = notes
        this.color = color
        this.archivedDate = archivedDate
        this.createdDate = createdDate
        this.isCollapsed = isCollapsed
        this.displayOrder = displayOrder
    }

    override fun getParentType(): ParentType {
        return ParentType.AREA
    }
}
