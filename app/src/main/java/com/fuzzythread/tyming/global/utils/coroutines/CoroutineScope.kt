package com.fuzzythread.tyming.global.utils.coroutines

import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

class CoroutineScope : CoroutineScope {
    private var job: Job = Job()
    private val exceptionHandler = CoroutineExceptionHandler { _, exception -> println(exception) }
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Default + exceptionHandler + job

    fun cancel() {
        job.cancel()
    }
}
