package com.fuzzythread.tyming.global

import android.app.*
import com.fuzzythread.tyming.BuildConfig
import com.fuzzythread.tyming.global.utils.logs.ReleaseTree
import com.squareup.leakcanary.LeakCanary
import dagger.android.DispatchingAndroidInjector
import net.danlew.android.joda.JodaTimeAndroid
import timber.log.Timber
import javax.inject.Inject
import android.content.Intent
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import com.fuzzythread.tyming.global.di.ApplicationModule
import com.fuzzythread.tyming.global.di.DaggerAppComponent
import com.fuzzythread.tyming.global.services.TimerService
import com.fuzzythread.tyming.global.services.SyncService
import com.fuzzythread.tyming.global.utils.other.Constants
import com.fuzzythread.tyming.global.utils.other.Constants.Companion.APP_TAG
import com.fuzzythread.tyming.global.utils.other.ServiceUtil
import dagger.android.AndroidInjector
import dagger.android.HasAndroidInjector

//HasActivityInjector, HasServiceInjector, HasSupportFragmentInjector,

open class TymingApplication :  LifecycleObserver, HasAndroidInjector, Application() {

    @Inject
    lateinit var notificationRegistrar: NotificationRegistrar

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()

//        if (!installLeakCanary()) return

        initializeUtils()
        configureDependencyInjection()
        configureLogging()
        notificationRegistrar.registerApp()
        startServices()
        ProcessLifecycleOwner
                .get()
                .lifecycle
                .addObserver(this)
    }

    protected fun startServices(){
        if (ServiceUtil.isMyServiceRunning(this, TimerService::class.java)) {
            startService(Intent(this, TimerService::class.java))
        }
        if (ServiceUtil.isMyServiceRunning(this, SyncService::class.java)) {
            startService(Intent(this, SyncService::class.java))
        }
    }

    protected open fun installLeakCanary(): Boolean {
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return false
        }
        LeakCanary.install(this)
        return true
    }

    private fun initializeUtils() {
        JodaTimeAndroid.init(this)
    }

    private fun configureLogging() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(ReleaseTree())
        }
    }

    protected open fun configureDependencyInjection() {
        DaggerAppComponent
                .builder()
                .applicationModule(ApplicationModule(applicationContext))
                .build()
                .inject(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onMoveToForeground() {
        Timber.tag(APP_TAG).d("App Moved to foreground")
        Intent(this, TimerService::class.java).apply {
            action = Constants.ACTION.STOP_FOREGROUND
        }.also {
            startService(it)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onMoveToBackground() {
        Timber.d("App Moved to background")
        Intent(this, TimerService::class.java).apply {
            action = Constants.ACTION.START_FOREGROUND
        }.also {
            startService(it)
        }
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector
    }
}
