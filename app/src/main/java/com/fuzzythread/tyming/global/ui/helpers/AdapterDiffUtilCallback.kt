package com.fuzzythread.tyming.global.ui.helpers

import androidx.recyclerview.widget.DiffUtil

class AdapterDiffUtilCallback<T>(private val oldList: MutableList<T> ,
                              private val newList: MutableList<T>) : DiffUtil.Callback() where T: IAdapterComparable<T> {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].areItemsTheSame(newList[newItemPosition])
    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].areContentsTheSame(newList[newItemPosition])
    }
}