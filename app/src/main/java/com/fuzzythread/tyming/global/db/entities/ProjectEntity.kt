package com.fuzzythread.tyming.global.db.entities

import com.fuzzythread.tyming.global.db.entities.interfaces.ITaskParent
import com.fuzzythread.tyming.global.utils.other.ParentType
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import io.objectbox.relation.ToOne
import io.objectbox.annotation.Backlink
import java.nio.ByteOrder
import java.util.*

@Entity
class ProjectEntity : ITaskParent {
    @Id(assignable = true)
    var id: Long = 0
    var title: String = ""
    var notes: String = ""
    var color: String = ""
    var trackProgress: Boolean = false
    var isCollapsed: Boolean = true
    var createdDate: Date? = null
    var deadlineDate: Date? = null
    var archivedDate: Date? = null
    var displayOrder: Long = 0

    // region relations
    lateinit var area: ToOne<AreaEntity>

    @Backlink(to = "project")
    lateinit var tasks: MutableList<TaskEntity>
    // endregion

    constructor()

    constructor(
            title: String,
            notes: String,
            color: String,
            deadlineDate: Date? = null,
            createdDate: Date = Date(),
            archivedDate: Date? = null,
            areaId: Long = 0,
            trackProgress: Boolean = false,
            isCollapsed: Boolean = true,
            displayOrder: Long = 0
    ) {
        this.title = title
        this.notes = notes
        this.color = color
        this.archivedDate = archivedDate
        this.deadlineDate = deadlineDate
        this.createdDate = createdDate
        this.trackProgress = trackProgress
        this.area.targetId = areaId
        this.isCollapsed = isCollapsed
        this.displayOrder = displayOrder
    }

    override fun getParentType(): ParentType {
        return ParentType.PROJECT
    }
}
