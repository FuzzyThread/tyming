<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <variable
            name="viewmodel"
            type="com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.ProjectViewModel" />

        <import type="android.graphics.Color" />

        <import type="org.joda.time.DateTime" />

        <import type="android.view.View" />
    </data>

    <FrameLayout

        android:id="@+id/item"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginTop="1dp"
        android:layout_marginBottom="2dp"
        android:background="@null"
        android:clickable="true"
        android:focusable="true"
        android:focusableInTouchMode="true"
        android:foreground="?selectableItemBackground"
        android:minHeight="60dp">

        <androidx.constraintlayout.widget.ConstraintLayout
            android:id="@+id/surface_view_t"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:layout_marginEnd="8dp"
            android:animateLayoutChanges="true"
            android:background="@drawable/bg_item_rounded"
            android:descendantFocusability="beforeDescendants"
            android:focusable="true"
            android:focusableInTouchMode="true">

            <androidx.appcompat.widget.AppCompatEditText
                android:id="@+id/title_tv"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_gravity="center_vertical"
                android:layout_marginTop="17dp"
                android:background="@null"
                android:ellipsize="end"
                android:hint="@string/project"
                android:justificationMode="inter_word"
                android:maxLength="100"
                android:maxLines="3"
                android:text="@={viewmodel.title}"
                android:textColor="@color/grey_300"
                android:textColorHighlight="@color/blue_600"
                android:textColorHint="@color/grey_600"
                android:textCursorDrawable="@drawable/bg_cursor"
                android:textSize="18sp"
                android:visibility="visible"
                app:backgroundTint="@color/blue_600"
                app:layout_constraintEnd_toStartOf="@+id/guideline3"
                app:layout_constraintStart_toStartOf="@+id/guideline"
                app:layout_constraintTop_toTopOf="parent"
                tools:targetApi="o" />

            <androidx.appcompat.widget.AppCompatEditText
                android:id="@+id/notes"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginTop="4dp"
                android:background="@null"
                android:ellipsize="end"
                android:hint="@string/notes"
                android:justificationMode="inter_word"
                android:paddingStart="0dp"
                android:paddingEnd="8dp"
                android:text="@={viewmodel.notes}"
                android:textAlignment="textStart"
                android:textColor="@color/grey_500"
                android:textColorHighlight="@color/accent"
                android:textColorHint="@color/grey_600"
                android:textCursorDrawable="@drawable/bg_cursor"
                android:textSize="15sp"
                app:layout_constraintEnd_toStartOf="@+id/guideline3"
                app:layout_constraintStart_toStartOf="@+id/guideline"
                app:layout_constraintTop_toBottomOf="@+id/title_tv"
                app:layout_constraintVertical_chainStyle="spread_inside"
                app:layout_goneMarginBottom="16dp"
                tools:targetApi="o" />

            <androidx.appcompat.widget.AppCompatImageView
                android:id="@+id/deadline"
                android:layout_width="16dp"
                android:layout_height="16dp"
                android:layout_centerVertical="true"
                android:layout_toStartOf="@+id/title_tv"
                android:elevation="2dp"
                android:src="@drawable/ic_flag"
                android:tint="@color/blue_600"
                android:visibility="@{viewmodel.hasDeadline ? View.VISIBLE : View.GONE}"
                app:layout_constraintBottom_toBottomOf="@+id/deadline_date"
                app:layout_constraintStart_toStartOf="@+id/guideline"
                app:layout_constraintTop_toTopOf="@+id/deadline_date" />

            <TextView
                android:id="@+id/deadline_date"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginTop="32dp"
                android:layout_marginEnd="8dp"
                android:ellipsize="end"
                android:paddingStart="0dp"
                android:paddingEnd="8dp"
                android:text="@{viewmodel.deadlineFormatted}"
                android:textAlignment="textStart"
                android:textColor="@color/grey_500"
                android:textSize="15sp"
                android:visibility="@{viewmodel.hasDeadline ? View.VISIBLE : View.GONE}"
                app:layout_constraintEnd_toStartOf="@+id/disable_deadline"
                app:layout_constraintStart_toStartOf="@+id/is_completable_tv"
                app:layout_constraintTop_toBottomOf="@+id/notes"
                app:layout_goneMarginBottom="16dp" />

            <View
                android:id="@+id/split1"
                android:layout_width="0dp"
                android:layout_height="1dp"
                android:layout_marginStart="8dp"
                android:layout_marginTop="24dp"
                android:layout_marginEnd="8dp"
                android:background="@color/grey_800"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/deadline_add"
                app:layout_goneMarginTop="32dp" />

            <androidx.appcompat.widget.LinearLayoutCompat
                android:id="@+id/checkbox_wrapper"
                android:layout_width="0dp"
                android:layout_height="0dp"
                android:orientation="vertical"
                android:translationZ="1000dp"
                app:layout_constraintBottom_toBottomOf="@+id/is_completable_tv"
                app:layout_constraintEnd_toEndOf="@+id/is_completable_tv"
                app:layout_constraintStart_toStartOf="@+id/toggle_completable"
                app:layout_constraintTop_toTopOf="@+id/is_completable_tv">

            </androidx.appcompat.widget.LinearLayoutCompat>

            <TextView
                android:id="@+id/is_completable_tv"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginStart="16dp"
                android:layout_marginTop="28dp"
                android:layout_marginEnd="8dp"
                android:ellipsize="end"
                android:lineSpacingExtra="4dp"
                android:text="@string/completable"
                android:textAlignment="textStart"
                android:textColor="@color/grey_500"
                android:textSize="15sp"
                android:translationZ="1dp"
                app:layout_constraintEnd_toStartOf="@+id/guideline3"
                app:layout_constraintStart_toEndOf="@+id/toggle_completable"
                app:layout_constraintTop_toBottomOf="@+id/deadline_date"
                app:layout_goneMarginBottom="16dp" />

            <androidx.appcompat.widget.AppCompatCheckBox
                android:id="@+id/toggle_completable"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:backgroundTint="@color/blue_600"
                android:checked="@={viewmodel.trackProgress}"
                android:clickable="@{!(viewmodel.trackProgress &amp;&amp; viewmodel.tasksCompletedNumber > 0)}"
                android:scaleX="0.8"
                android:scaleY="0.8"
                android:translationZ="1dp"
                app:layout_constraintBottom_toBottomOf="@+id/is_completable_tv"
                app:layout_constraintEnd_toEndOf="@+id/project_color"
                app:layout_constraintStart_toStartOf="@+id/project_color"
                app:layout_constraintTop_toTopOf="@+id/is_completable_tv" />

            <LinearLayout
                android:id="@+id/deadline_add"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="24dp"
                android:layout_toStartOf="@+id/title_tv"
                android:background="@drawable/bg_badge_rounded_checked"
                android:gravity="center_vertical"
                android:maxWidth="175dp"
                android:orientation="horizontal"
                android:padding="0dp"
                android:visibility="@{!viewmodel.hasDeadline &amp;&amp; viewmodel.trackProgress ? View.VISIBLE : View.GONE}"
                app:layout_constraintStart_toStartOf="@+id/parent_container"
                app:layout_constraintTop_toBottomOf="@+id/color"
                app:layout_goneMarginBottom="24dp"
                app:layout_goneMarginTop="24dp">

                <ImageView
                    android:id="@+id/deadline_add_icon"
                    android:layout_width="16dp"
                    android:layout_height="16dp"
                    android:layout_marginStart="12dp"
                    android:contentDescription="@string/add_deadline"
                    android:elevation="2dp"
                    android:src="@drawable/ic_flag"
                    android:visibility="@{!viewmodel.hasDeadline ? View.VISIBLE : View.GONE}"
                    app:tint="@color/blue_600" />

                <TextView
                    android:id="@+id/add_deadline_txt"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="8dp"
                    android:layout_marginTop="7dp"
                    android:layout_marginEnd="12dp"
                    android:layout_marginBottom="7dp"
                    android:ellipsize="end"
                    android:includeFontPadding="false"
                    android:maxLines="1"
                    android:padding="0dp"
                    android:text="@string/add_deadline"
                    android:textColor="@color/grey_300"
                    android:textSize="17sp" />
            </LinearLayout>

            <LinearLayout
                android:id="@+id/save"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:orientation="vertical"
                android:paddingStart="16dp"
                android:paddingEnd="20dp"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintTop_toTopOf="@+id/split1">

                <Button
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="16dp"
                    android:layout_marginBottom="16dp"
                    android:background="@drawable/bg_button_rounded"
                    android:backgroundTint="@color/blue_600"
                    android:clickable="false"
                    android:minWidth="0dp"
                    android:minHeight="0dp"
                    android:paddingStart="15dp"
                    android:paddingEnd="15dp"
                    android:paddingTop="7dp"
                    android:paddingBottom="6dp"
                    android:text="@{viewmodel.editing ? @string/update : @string/save}"
                    android:textColor="@color/grey_300"
                    android:textSize="16sp" />

            </LinearLayout>

            <ImageButton
                android:id="@+id/disable_deadline"
                style="@style/Widget.AppCompat.Button.Borderless"
                android:layout_width="40dp"
                android:layout_height="40dp"
                android:background="@null"
                android:contentDescription="@string/close"
                android:onClick="@{() -> viewmodel.removeDeadline()}"
                android:scaleType="centerInside"
                android:scaleX="0.9"
                android:scaleY="0.9"
                android:src="@drawable/ic_close"
                android:visibility="@{viewmodel.hasDeadline ? View.VISIBLE : View.GONE}"
                app:layout_constraintBottom_toBottomOf="@+id/deadline"
                app:layout_constraintEnd_toStartOf="@+id/guideline3"
                app:layout_constraintTop_toTopOf="@+id/deadline" />

            <LinearLayout
                android:id="@+id/parent_container"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginStart="18dp"
                android:background="@drawable/bg_badge_rounded"
                android:gravity="center_vertical"
                android:maxWidth="175dp"
                android:orientation="horizontal"
                android:padding="0dp"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/split1">

                <ImageView
                    android:id="@+id/parent_icon"
                    src="@{viewmodel.parentDrawable, default=@drawable/ic_root}"
                    android:layout_width="16dp"
                    android:layout_height="16dp"
                    android:layout_marginStart="12dp"
                    android:layout_marginTop="4dp"
                    android:layout_marginBottom="4dp"
                    android:contentDescription="@string/parent_icon"
                    android:tint="@{Color.parseColor(viewmodel.parentColor),default=@color/blue_600}"
                    tools:ignore="UseAppTint" />

                <TextView
                    android:id="@+id/parent_name"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="8dp"
                    android:layout_marginTop="7dp"
                    android:layout_marginEnd="12dp"
                    android:layout_marginBottom="7dp"
                    android:ellipsize="end"
                    android:maxWidth="150dp"
                    android:maxLines="1"
                    android:padding="0dp"
                    android:text="@{viewmodel.parentName}"
                    android:textColor="@color/grey_300"
                    android:textSize="17sp"/>
            </LinearLayout>

            <ImageView
                android:id="@+id/project_color"
                android:layout_width="16dp"
                android:layout_height="16dp"
                android:background="@drawable/bg_badge_rounded_alt"
                android:backgroundTint="@{Color.parseColor(viewmodel.color)}"
                android:contentDescription="@string/click_to_change_color"
                app:layout_constraintBottom_toBottomOf="@+id/color"
                app:layout_constraintStart_toStartOf="@+id/guideline"
                app:layout_constraintTop_toTopOf="@+id/color"
                app:layout_goneMarginBottom="32dp"
                app:layout_goneMarginTop="32dp" />

            <TextView
                android:id="@+id/color"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginTop="28dp"
                android:layout_marginEnd="8dp"
                android:ellipsize="end"
                android:paddingStart="0dp"
                android:paddingEnd="8dp"
                android:text="@string/click_to_change_color"
                android:textAlignment="textStart"
                android:textColor="@color/grey_500"
                android:textSize="15sp"
                app:layout_constraintEnd_toStartOf="@+id/guideline3"
                app:layout_constraintStart_toStartOf="@+id/is_completable_tv"
                app:layout_constraintTop_toBottomOf="@+id/is_completable_tv"
                app:layout_goneMarginBottom="16dp" />

            <androidx.constraintlayout.widget.Guideline
                android:id="@+id/guideline"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:orientation="vertical"
                app:layout_constraintGuide_begin="25dp" />

            <androidx.constraintlayout.widget.Guideline
                android:id="@+id/guideline2"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:orientation="horizontal"
                android:visibility="visible"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintGuide_begin="60dp" />

            <androidx.constraintlayout.widget.Guideline
                android:id="@+id/guideline3"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:orientation="vertical"
                app:layout_constraintGuide_end="20dp" />

        </androidx.constraintlayout.widget.ConstraintLayout>

    </FrameLayout>

</layout>
