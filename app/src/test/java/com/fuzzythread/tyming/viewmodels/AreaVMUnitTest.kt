package com.fuzzythread.tyming.viewmodels

import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.AreaViewModel
import org.junit.Test

import org.junit.Assert.*
import java.util.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class AreaVMUnitTest {

    @Test
    fun checkAreaVmField() {
        assertEquals(title, AREA_VM.title)
        assertEquals(notes, AREA_VM.notes)
        assertEquals(id, AREA_VM.id)
        assertEquals(createdDate, AREA_VM.createdDate)
        assertEquals(archivedDate, AREA_VM.archivedDate)
        assertEquals(color, AREA_VM.color)
    }

    @Test
    fun checkAreaVmFieldsEmpty() {

        val tempAreaVM = AreaViewModel(context = null)

        assertEquals("", tempAreaVM.title)
        assertEquals("", tempAreaVM.notes)
        assertEquals(0, tempAreaVM.id)
        assertEquals(null, tempAreaVM.createdDate)
        assertEquals(null, tempAreaVM.archivedDate)
        assertEquals("#1e88e5", tempAreaVM.color)
    }

    companion object {

        private val AREA_VM = AreaViewModel(context = null)

        private const val title = "Test title"
        private const val notes = "Test note"
        private const val id = 1.toLong()
        private val createdDate = Date()
        private val archivedDate = Date()
        private val color = "#FFFFFF"

        init {
            AREA_VM.notes = notes
            AREA_VM.title = title
            AREA_VM.id = id
            AREA_VM.createdDate = createdDate
            AREA_VM.archivedDate = archivedDate
            AREA_VM.color = color
        }
    }
}
