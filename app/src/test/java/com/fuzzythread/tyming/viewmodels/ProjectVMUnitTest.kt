package com.fuzzythread.tyming.viewmodels

import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.ProjectViewModel
import org.junit.Test

import org.junit.Assert.*
import java.util.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ProjectVMUnitTest {
    @Test
    fun checkProjectVmField() {
        assertEquals(title, PROJECT_VM.title)
        assertEquals(notes, PROJECT_VM.notes)
        assertEquals(id, PROJECT_VM.id)
        assertEquals(createdDate, PROJECT_VM.createdDate)
        assertEquals(archivedDate, PROJECT_VM.archivedDate)
        assertEquals(color, PROJECT_VM.color)
    }

    @Test
    fun checkProjectVmFieldsEmpty() {

        val tempProjectVM = ProjectViewModel(context = null)

        assertEquals("", tempProjectVM.title)
        assertEquals("", tempProjectVM.notes)
        assertEquals(0, tempProjectVM.id)
        assertEquals(null, tempProjectVM.createdDate)
        assertEquals(null, tempProjectVM.archivedDate)
        assertEquals("#1e88e5", tempProjectVM.color)
    }

    companion object {

        private val PROJECT_VM = ProjectViewModel(context = null)

        private const val title = "Test title"
        private const val notes = "Test note"
        private const val id = 1.toLong()
        private val createdDate = Date()
        private val archivedDate = Date()
        private val color = "#FFFFFF"

        init {
            PROJECT_VM.notes = notes
            PROJECT_VM.title = title
            PROJECT_VM.id = id
            PROJECT_VM.createdDate = createdDate
            PROJECT_VM.archivedDate = archivedDate
            PROJECT_VM.color = color
        }
    }
}
