package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog

import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import com.fuzzythread.tyming.R
import android.view.Gravity
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.flask.colorpicker.ColorPickerView
import com.flask.colorpicker.builder.ColorPickerDialogBuilder
import com.fuzzythread.tyming.global.di.ViewModelFactory
import dagger.android.support.DaggerAppCompatDialogFragment
import javax.inject.Inject
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.AreaViewModel
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.model.AreaDialogVM
import com.fuzzythread.tyming.databinding.DialogAreaBinding
import com.fuzzythread.tyming.global.exception.TitleNotSpecifiedException
import com.fuzzythread.tyming.global.utils.other.ActionType
import com.fuzzythread.tyming.global.utils.ui.hideKeyboard

class AreaDialog : DaggerAppCompatDialogFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val areaDialogVM: AreaDialogVM by lazy {
        ViewModelProvider(this, viewModelFactory).get(AreaDialogVM::class.java)
    }

    private var areaViewModel: AreaViewModel? = null
    private var callback: (() -> Unit)? = null
    private var areaId: Long = 0
    private var actionType: ActionType = ActionType.ADD

    fun getInstance(areaId: Long = 0): AreaDialog {
        val fragment = AreaDialog()
        fragment.arguments = Bundle().apply {
            putLong(AREA_ID, areaId)
        }
        return fragment
    }

    fun setDismissCallBack(func: () -> Unit): AreaDialog {
        callback = func
        return this
    }

    private fun removeDismissCallBack() {
        callback = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        // restore bundle:
        areaId = arguments?.getLong(AREA_ID) ?: 0

        areaViewModel = if (areaId > 0) {
            actionType = ActionType.EDIT
            areaDialogVM.getAreaViewModel(areaId)?.apply { editing = true }
        } else {
            AreaViewModel(this.context?.applicationContext)
        }
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {

        // bind ViewModel:
        val itemBinding = DialogAreaBinding.inflate(inflater, container, false)
        val view = itemBinding.root
        itemBinding.viewmodel = areaViewModel

        itemBinding.color.setOnClickListener {
            hideKeyboard()
            ColorPickerDialogBuilder
                    .with(context, R.style.Theme_Dialog)
                    .setTitle(context?.getString(R.string.choose_color))
                    .showAlphaSlider(false)
                    .initialColor(Color.parseColor(areaViewModel!!.color))
                    .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
                    .density(12)
                    .setOnColorSelectedListener { selectedColor ->
                    }
                    .setPositiveButton(context?.getString(R.string.ok)) { dialog, selectedColor, allColors ->
                        areaViewModel!!.color = "#" + Integer.toHexString(selectedColor)
                        dialog.dismiss()
                    }
                    .setNegativeButton(context?.getString(R.string.cancel)) { dialog, which ->
                        dialog.dismiss()
                    }
                    .build()
                    .show()
        }

        view.setOnClickListener {
            hideKeyboard()
        }

        itemBinding.actionBtn.setOnClickListener {
            try {
                areaViewModel!!.validate()
                // db update:
                if (actionType == ActionType.ADD) {
                    areaDialogVM.addArea(areaViewModel!!)
                    Toast.makeText(this.context, context?.getString(R.string.area_added), Toast.LENGTH_LONG).show()
                } else {
                    areaDialogVM.updateArea(areaViewModel!!)
                    Toast.makeText(this.context, context?.getString(R.string.area_updated), Toast.LENGTH_LONG).show()
                }

                // update parent view by callback:
                callback?.invoke()
                dialog?.dismiss()
            } catch (titleException: TitleNotSpecifiedException) {
                Toast.makeText(context, titleException.message, Toast.LENGTH_LONG).show()
            }
        }

        setDialogPosition()
        return view
    }

    override fun onStart() {
        super.onStart()
        val window = dialog?.window
        val params = window!!.attributes
        params.dimAmount = 0.6f
        window.attributes = params
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setLayout(width, height)
        window.setBackgroundDrawableResource(android.R.color.transparent)
        dialog?.setCanceledOnTouchOutside(true)
    }

    private fun setDialogPosition() {
        val window = dialog?.window
        window!!.setGravity(Gravity.CENTER_HORIZONTAL or Gravity.CENTER_VERTICAL)
        val params = window.attributes
        params.windowAnimations = R.style.DialogSlideBottom
        window.attributes = params
    }

    override fun onDismiss(dialog: DialogInterface) {
        callback?.invoke()
        removeDismissCallBack()
        super.onDismiss(dialog)
    }

    companion object {
        const val AREA_ID = "AREA_ID"
    }
}
