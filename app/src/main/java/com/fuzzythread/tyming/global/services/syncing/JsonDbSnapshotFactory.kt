package com.fuzzythread.tyming.global.services.syncing

import com.fuzzythread.tyming.global.utils.extensions.createGsonBuilder
import com.fuzzythread.tyming.global.utils.other.Constants
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import timber.log.Timber
import java.util.*


class JsonDbSnapshotFactory(val json: String) : DbSnapshotFactory {

    private var snapshot:DbSnapshot? = null
        get() {
            return if (field == null) {
                createFromJson(json)
            } else {
                field
            }
        }

    val version:Long
        get() {
            return snapshot?.version ?: 0
        }

    private fun createFromJson(json: String): DbSnapshot? {
        return try {
            createGsonBuilder().fromJson(json, DbSnapshot::class.java)
        } catch (ex: JsonSyntaxException) {
            Timber.tag(Constants.APP_TAG).d("Json issue: $ex")
            null
        }
    }

    override fun createDbSnapshot(): DbSnapshot {
        return snapshot ?: DbSnapshot(0, Date(), mutableListOf())
    }
}
