package com.fuzzythread.tyming.global.services.syncing

import com.fuzzythread.tyming.global.db.dao.AreaDao
import com.fuzzythread.tyming.global.db.dao.ProjectDao
import com.fuzzythread.tyming.global.db.dao.TaskDao
import com.fuzzythread.tyming.global.db.dao.TimeEntryDao
import com.fuzzythread.tyming.global.db.dao.util.Dao
import com.fuzzythread.tyming.global.db.entities.AreaEntity
import com.fuzzythread.tyming.global.db.entities.ProjectEntity
import com.fuzzythread.tyming.global.db.entities.TaskEntity
import com.fuzzythread.tyming.global.db.entities.TimeEntryEntity
import java.util.*
import javax.inject.Inject

class DatabaseManager @Inject constructor(
        var areaDao: AreaDao,
        var projectDao: ProjectDao,
        var taskDao: TaskDao,
        var timeEntryDao: TimeEntryDao
) {

    fun create(structureFactory: DbSnapshotFactory) {
        val dbStructure = structureFactory.createDbSnapshot()
        this.createDatabase(dbStructure)
    }

    fun drop() {
        (areaDao as Dao<*>).dropDatabase()
    }

    private fun createDatabase(createDbStructure: DbSnapshot) {
        updateVersion(createDbStructure.version, createDbStructure.date)
        createAreas(createDbStructure.areas)
    }

    private fun updateVersion(versionNumber: Long, date: Date) {
        (areaDao as Dao<*>).setVersion(versionNumber, date)
    }

    private fun createAreas(areas: List<Area>) {
        areas.forEach { area ->
            if (area.singularProjectsPlaceholder) {
                createProjects(area.projects)
            } else {
                AreaEntity(
                        title = area.title,
                        notes = area.notes,
                        color = area.color,
                        isCollapsed = area.isCollapsed,
                        createdDate = area.createdDate ?: Date(),
                        archivedDate = area.archivedDate
                ).also {
                    val areaId = areaDao.add(it, ignoreVersion = true)
                    createProjects(area.projects, areaId)
                }
            }
        }
    }

    private fun createProjects(projects: List<Project>, areaId: Long = 0) {
        projects.forEach { project ->
            ProjectEntity(
                    title = project.title,
                    notes = project.notes,
                    color = project.color,
                    trackProgress = project.trackProgress,
                    isCollapsed = true,
                    createdDate = project.createdDate ?: Date(),
                    deadlineDate = project.deadlineDate,
                    archivedDate = project.archivedDate,
                    areaId = areaId
            ).also {
                val projectId = projectDao.add(it, ignoreVersion = true)
                createTasks(project.tasks, projectId)
            }
        }
    }

    private fun createTasks(tasks: List<Task>, projectId: Long = 0) {
        tasks.forEach { task ->
            TaskEntity(
                    title = task.title,
                    notes = task.notes,
                    createdDate = task.createdDate ?: Date(),
                    deadlineDate = task.deadlineDate,
                    completedDate = task.completedDate,
                    timeElapsed = task.timeElapsed,
                    isTimerRunning = task.isTimerRunning,
                    projectId = projectId
            ).also {
                val taskId = taskDao.add(it, ignoreVersion = true)
                createTimeEntries(task.timeEntries, taskId)
            }
        }
    }

    private fun createTimeEntries(timeEntries: List<TimeEntry>, taskId: Long) {
        timeEntries.forEach { timeEntry ->
            TimeEntryEntity(
                    notes = timeEntry.notes,
                    location = timeEntry.location,
                    startDate = timeEntry.startDate,
                    endDate = timeEntry.endDate,
                    archivedDate = timeEntry.archivedDate,
                    taskId = taskId
            ).also {
                timeEntryDao.add(it, ignoreVersion = true)
            }
        }
    }
}


