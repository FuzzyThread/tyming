package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model

import android.content.Context
import androidx.databinding.Bindable
import com.fuzzythread.tyming.BR
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.global.ObservableViewModel
import com.fuzzythread.tyming.global.db.entities.AreaEntity
import com.fuzzythread.tyming.global.db.entities.ProjectEntity
import com.fuzzythread.tyming.global.db.entities.TaskEntity
import com.fuzzythread.tyming.global.db.entities.interfaces.ITaskParent
import com.fuzzythread.tyming.global.exception.DurationNotSelectedException
import com.fuzzythread.tyming.global.exception.TaskNotSelectedException
import com.fuzzythread.tyming.global.ui.helpers.IAdapterComparable
import com.fuzzythread.tyming.global.utils.other.ParentType
import com.fuzzythread.tyming.global.utils.time.formatTimeStartEnd
import com.fuzzythread.tyming.global.utils.time.formatTimeToString
import org.joda.time.DateTime
import org.joda.time.DateTimeComparator
import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import java.util.*

class TimeEntryViewModel(val context: Context?) :
        ObservableViewModel(),
        IAdapterComparable<TimeEntryViewModel> {

    // region Database Model Attributes

    var id: Long = 0

    @get:Bindable
    var notes: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.notes)
        }

    @get:Bindable
    var location: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.location)
        }

    var startDate: Date? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.startDateFormatted)
        }

    var endDate: Date? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.endDateFormatted)
        }

    // endregion

    // region Dates

    @get:Bindable
    var startDateFormatted: String = ""
        get() {
            return if (startDate != null) {
                // update end month
                endDate = DateTime(startDate)
                        .plusHours(durationHours)
                        .plusMinutes(durationMinutes)
                        .toDate()

                if (dateComparator.compare(startDate, Date()) == 0) {
                    "Today"
                } else {
                    dateFormatter.print(DateTime(startDate))
                }
            } else {
                "Start Date"
            }
        }

    @get:Bindable
    var startTimeFormatted: String = ""
        get() {
            return if (startDate != null) {
                // update end month
                endDate = DateTime(startDate)
                        .plusHours(durationHours)
                        .plusMinutes(durationMinutes)
                        .toDate()
                val startDateDT = DateTime(startDate)
                notifyPropertyChanged(BR.startEndTimeFormatted)
                return String.format("%02d:%02d", startDateDT.hourOfDay, startDateDT.minuteOfHour)
            } else {
                context?.getString(R.string.start_time) ?: "Start Time"
            }
        }

    @get:Bindable
    val startEndTimeFormatted: String
        get() {
            return if (isStartEnd && startDate != null && endDate != null) {
                if (durationHours > 0 || durationMinutes > 0){
                    formatTimeStartEnd(startDate!!, endDate!!)
                } else{
                    ""
                }
            } else {
                ""
            }
        }

    @get:Bindable
    var endDateFormatted: String = ""
        get() {
            return if (startDate != null) {
                dateFormatter.print(DateTime(endDate))
            } else {
                context?.getString(R.string.end_date) ?:"End Date"
            }
        }

    @get:Bindable
    var timeTracked: String? = ""
        get() {
            return if (startDate != null && endDate != null) {
                val period = Period(DateTime(startDate), DateTime(endDate))
                formatTimeToString(period)
            } else {
                ""
            }
        }
        set(value) {
            field = value
            notifyPropertyChanged(BR.timeTracked)
        }

    @get:Bindable
    var durationFormatted: String? = ""
        get() {
            return String.format("%02d:%02d", durationHours, durationMinutes)
        }

    var durationMinutes = 0
        set(value) {
            field = value
            endDate = DateTime(startDate)
                    .plusHours(durationHours)
                    .plusMinutes(value)
                    .toDate()
            notifyPropertyChanged(BR.durationFormatted)
            notifyPropertyChanged(BR.startTimeFormatted)
        }

    var durationHours = 0
        set(value) {
            field = value
            endDate = DateTime(startDate)
                    .plusMinutes(durationMinutes)
                    .plusHours(value)
                    .toDate()
            notifyPropertyChanged(BR.durationFormatted)
            notifyPropertyChanged(BR.startTimeFormatted)
        }

    var startDateMinutes = 0
        set(value) {
            field = value
            startDate = DateTime(startDate)
                    .withMinuteOfHour(value)
                    .withHourOfDay(startDateHours)
                    .toDate()
            notifyPropertyChanged(BR.startTimeFormatted)
        }

    var startDateHours = 0
        set(value) {
            field = value
            startDate = DateTime(startDate)
                    .withMinuteOfHour(startDateMinutes)
                    .withHourOfDay(value)
                    .toDate()
            notifyPropertyChanged(BR.startTimeFormatted)
        }

    var hasDeadline: Boolean = false
    var isStartEnd: Boolean = false

    // endregion

    // region Relations

    var task: TaskEntity? = null
        set(value) {
            field = value
            if (value != null) {
                taskName = value.title
            }
        }

    @get:Bindable
    var taskName: String = context?.getString(R.string.select_task) ?: "Select Task"
        set(value) {
            field = value
            notifyPropertyChanged(BR.taskName)
        }

    // region View States

    @get:Bindable
    var editing: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.editing)
        }

    @get:Bindable
    var expanded: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.expanded)
        }

    @get:Bindable
    var editingStartTime: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.editingStartTime)
        }

    // endregion

    private var dateComparator = DateTimeComparator.getDateOnlyInstance()
    private var dateFormatter = DateTimeFormat.forPattern("EEEE, d MMM yyyy")


    // region DiffUtil Functions

    override fun areItemsTheSame(itemToCompare: TimeEntryViewModel): Boolean {
        return itemToCompare.id == id
    }

    override fun areContentsTheSame(itemToCompare: TimeEntryViewModel): Boolean {
        return itemToCompare.notes == notes &&
                itemToCompare.taskName == taskName
    }

    fun validate() {
        validateTask()
        validateDuration()
    }

    private fun validateTask() {
        if (task == null) {
            throw TaskNotSelectedException()
        }
    }

    private fun validateDuration() {
        if (durationHours == 0 && durationMinutes == 0) {
            throw DurationNotSelectedException()
        }
    }
    // endregion
}
