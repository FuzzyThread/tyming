package com.fuzzythread.tyming.screens.journal.model

import android.content.Context
import androidx.databinding.Bindable
import com.fuzzythread.tyming.BR
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.global.ObservableViewModel
import com.fuzzythread.tyming.global.ui.helpers.IAdapterComparable
import java.util.*

class DayViewModel(val context: Context) :
        ObservableViewModel(),
        IAdapterComparable<DayViewModel> {

    // region Database Model Attributes

    var id: UUID = UUID.randomUUID()

    @get:Bindable
    var weekDay: String = context.getString(R.string.today)
        set(value) {
            field = value.capitalize()
            notifyPropertyChanged(BR.weekDay)
        }

    @get:Bindable
    var timeTrackedFormatted: String = "00:00"
        set(value) {
            field = value
            notifyPropertyChanged(BR.timeTrackedFormatted)
        }

    @get:Bindable
    var today: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.today)
        }

    // region DiffUtil Functions

    override fun areItemsTheSame(itemToCompare: DayViewModel): Boolean {
        return itemToCompare.id == id
    }

    override fun areContentsTheSame(itemToCompare: DayViewModel): Boolean {
        return itemToCompare.id == id &&
                itemToCompare.weekDay == weekDay &&
                itemToCompare.timeTrackedFormatted == timeTrackedFormatted
    }

    // endregion
}
