package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model

import android.content.Context
import androidx.databinding.Bindable
import com.fuzzythread.tyming.BR
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.global.ObservableViewModel
import com.fuzzythread.tyming.global.db.entities.AreaEntity
import com.fuzzythread.tyming.global.db.entities.ProjectEntity
import com.fuzzythread.tyming.global.db.entities.TimeEntryEntity
import com.fuzzythread.tyming.global.db.entities.interfaces.ITaskParent
import com.fuzzythread.tyming.global.exception.ProjectNotSelectedException
import com.fuzzythread.tyming.global.exception.TitleNotSpecifiedException
import com.fuzzythread.tyming.global.ui.helpers.IAdapterComparable
import com.fuzzythread.tyming.global.utils.extensions.endOfDay
import com.fuzzythread.tyming.global.utils.extensions.startOfDay
import com.fuzzythread.tyming.global.utils.other.EntityType
import com.fuzzythread.tyming.global.utils.other.ParentType
import com.fuzzythread.tyming.global.utils.time.formatTimeToString
import org.joda.time.DateTime
import org.joda.time.Duration
import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import java.util.*
import kotlin.math.round

class TaskViewModel(val context: Context?) :
        ObservableViewModel(),
        IAdapterComparable<TaskViewModel> {

    // region Database Model Attributes

    var id: Long = 0

    @get:Bindable
    var title: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.title)
        }

    @get:Bindable
    var notes: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.notes)
        }

    @get:Bindable
    var displayOrder: Long = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.displayOrder)
        }

    @get:Bindable
    var createdDate: Date? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.createdDate)
        }

    var completedDate: Date? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.completed)
        }

    var deadlineDate: Date? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.hasDeadline)
            notifyPropertyChanged(BR.deadlineFormatted)
        }

    var parent: ITaskParent? = null
        set(value) {
            field = value
            if (value != null) {
                parentType = value.getParentType()
                when (parentType) {
                    ParentType.PROJECT -> {
                        with(value as ProjectEntity) {
                            parentName = title
                            parentColor = color
                            parentId = id
                            parentDrawable = R.drawable.ic_todo
                        }
                    }
                    ParentType.AREA -> {
                        with(value as AreaEntity) {
                            parentName = title
                            parentColor = color
                            parentId = id
                            parentDrawable = R.drawable.ic_area
                        }
                    }
                    else -> {
                    }
                }
            } else {
                parentId = 0
                parentType = ParentType.NONE
                parentName = context?.getString(R.string.not_selected) ?: "Not Selected"
                parentColor = "#1e88e5"
                parentDrawable = R.drawable.ic_task
            }
        }

    var timeEntries: MutableList<TimeEntryEntity> = mutableListOf()
        set(value) {
            field = value
            notifyPropertyChanged(BR.timeTrackedFormatted)
            notifyPropertyChanged(BR.lastWorkedFormatted)
            notifyPropertyChanged(BR.timeTracked)
        }

    // endregion

    // region Dates

    private var dateFormatter = DateTimeFormat.forPattern("d MMM yyyy")
    private val currentDate = DateTime().startOfDay()

    @get:Bindable
    var deadlineFormatted: String = ""
        get() {
            var value = ""
            if (deadlineDate != null) {
                val deadlineDate = DateTime(deadlineDate).endOfDay()
                val duration = Duration(currentDate.withHourOfDay(0), deadlineDate)

                value = when {
                    currentDate.isAfter(deadlineDate) -> context?.getString(R.string.overdue) ?: "Overdue"
                    duration.toStandardHours().hours in 0..24 -> context?.getString(R.string.today) ?: "Today"
                    duration.toStandardDays().days >= 1 -> dateFormatter.print(deadlineDate)
                    else -> ""
                }
            }
            return value
        }

    @get:Bindable
    var timeTrackedFormatted: String = ""
        get() {
            var value = ""
            if (!timeEntries.isEmpty()) {
                val periods = sumPeriods(timeEntries)
                val hours = periods.toInt()
                val minutes = round((periods % 1) * 60).toInt()

                if (hours > 0) {
                    value += "$hours ${context?.getString(R.string.hours)?.toLowerCase() ?: "hours"}"
                }
                if (minutes > 0) {
                    value += "$minutes ${context?.getString(R.string.minutes) ?: "minutes"}"
                }
                if (hours == 0 && minutes == 0) {
                    value += "0 ${context?.getString(R.string.minutes) ?: "minutes"}"
                }
            } else {
                value = context?.getString(R.string.no_time_spent) ?: "No Time Spent"
            }
            return value
        }

    var timeElapsed: Long = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.lastWorkedFormatted)
            notifyPropertyChanged(BR.timerRunning)
        }

    @get:Bindable
    var lastWorkedFormatted: String = ""
        get() {
            var value = ""
            if (completedDate != null) {
                value = "${context?.getString(R.string.completed) ?: "Completed"}: ${dateFormatter.print(DateTime(completedDate))}"
            } else {
                if (timeElapsed > 0) {
                    value += "${context?.getString(R.string.timer_running) ?: "Timer running"}: "

                    val seconds = ((timeElapsed + 10) / 10)
                    val minutes = (seconds / 60)
                    val hours = (minutes / 60)
                    if (hours > 0) {
                        value += "$hours ${if (hours > 1) {
                            context?.getString(R.string.hours)?.toLowerCase() ?: "hours"
                        } else {
                            context?.getString(R.string.hour) ?: "hour"
                        }} "
                    }
                    if (minutes > 0 && hours < 1) {
                        value += "$minutes ${if (minutes > 1) {
                            context?.getString(R.string.minutes) ?: "minutes"
                        } else {
                            context?.getString(R.string.minute) ?: "minute"
                        }} "
                    }
                    if (seconds > 0 && minutes < 1 && hours < 1) {
                        value += "< 1 ${context?.getString(R.string.minute) ?: "minute"}"
                    }
                } else {
                    if (timeEntries.isNotEmpty()) {
                        val latestTimeEntry = timeEntries.filter { it.startDate != null }.maxBy { it.startDate!! }
                        if (latestTimeEntry?.startDate != null) {
                            val period = Period(DateTime(latestTimeEntry.startDate), DateTime())

                            if (period.minutes < 1) {
                                value = context?.getString(R.string.just_know) ?: "just know"
                            }

                            if (period.minutes > 0) {
                                value = "${period.minutes} ${if (period.minutes > 1) {
                                    context?.getString(R.string.minutes) ?: "minutes"
                                } else {
                                    context?.getString(R.string.minute) ?: "minute"
                                }} ${context?.getString(R.string.ago) ?: "ago"}"
                            }

                            if (period.hours > 0) {
                                value = "${period.hours} ${if (period.hours > 1) {
                                    context?.getString(R.string.hours) ?: "hours"
                                } else {
                                    context?.getString(R.string.hour) ?: "hour"
                                }} ${context?.getString(R.string.ago) ?: "ago"}"
                            }

                            if (period.days > 0) {
                                value = "${period.days} ${if (period.days > 1) {
                                    context?.getString(R.string.days) ?: "days"
                                } else {
                                    context?.getString(R.string.day) ?: "day"
                                }} ${context?.getString(R.string.ago) ?: "ago"}"
                            }

                            if (period.months > 0) {
                                value = "${period.months} ${if (period.months > 1) {
                                    context?.getString(R.string.months) ?: "months"
                                } else {
                                    context?.getString(R.string.month) ?: "month"
                                }} ${context?.getString(R.string.ago) ?: "ago"}"
                            }

                            if (period.years > 0) {
                                value = "${period.years} ${if (period.years > 1) {
                                    context?.getString(R.string.years) ?: "years"
                                } else {
                                    context?.getString(R.string.year) ?: "year"
                                }} ${context?.getString(R.string.ago) ?: "ago"}"
                            }
                        }
                    } else {
                        value = ""
                    }
                }
            }
            if (timeEntries.isNotEmpty()) {
                value = " • $value"
            }

            return value
        }

    @get:Bindable
    var timeTracked: String = "00:00"
        get() {
            return if (!timeEntries.isEmpty()) {
                val periods = sumPeriods(timeEntries)
                formatTimeToString(periods)
            } else {
                "Oh"
            }
        }

    @get:Bindable
    var hasDeadline: Boolean = false
        get() {
            return deadlineDate != null
        }

    // endregion

    // region Relations

    @get:Bindable
    var parentType: ParentType = ParentType.NONE
        set(value) {
            field = value
            notifyPropertyChanged(BR.parentType)
        }

    @get:Bindable
    var parentId: Long = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.parentId)
        }

    @get:Bindable
    var parentColor: String = "#1e88e5"
        get() {
            if (field.isEmpty()) {
                return "#1e88e5"
            }
            return field
        }
        set(value) {
            field = value
            notifyPropertyChanged(BR.parentColor)
        }

    @get:Bindable
    var parentName: String = context?.getString(R.string.not_selected) ?: "Not Selected"
        set(value) {
            field = value
            notifyPropertyChanged(BR.parentName)
        }

    @get:Bindable
    var parentDrawable: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.parentDrawable)
        }

    @get:Bindable
    var hasTags: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.hasTags)
        }

    // endregion

    // region View States

    @get:Bindable
    var editing: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.editing)
        }

    @get:Bindable
    var timerRunning: Boolean = false
        set(value) {
            field = value
            if (!value) {
                timeElapsed = 0
            }
            notifyPropertyChanged(BR.timerRunning)
        }

    @get:Bindable
    var completed: Boolean = false
        get() {
            return completedDate != null
        }

    fun removeDeadline() {
        deadlineDate = null
    }

    fun completeTask() {
        completedDate = if (completedDate != null) {
            null
        } else {
            Date()
        }
    }

    // endregion

    // region DiffUtil Functions

    override fun areItemsTheSame(itemToCompare: TaskViewModel): Boolean {
        return itemToCompare.id == id
    }

    override fun areContentsTheSame(itemToCompare: TaskViewModel): Boolean {
        return itemToCompare.title == title &&
                itemToCompare.notes == notes &&
                itemToCompare.createdDate == createdDate &&
                itemToCompare.completedDate == completedDate &&
                itemToCompare.deadlineDate == deadlineDate &&
                itemToCompare.parentId == parentId &&
                itemToCompare.parentName == parentName &&
                itemToCompare.timeEntries == timeEntries &&
                itemToCompare.displayOrder == displayOrder &&
                itemToCompare.parentColor == parentColor
    }

    private fun sumPeriods(list: List<TimeEntryEntity>): Float {
        var sum = 0f
        list.forEach {
            val period = Period(DateTime(it.startDate), DateTime(it.endDate))
            sum += period.hours + period.days * 24
            sum += period.minutes / 60f
        }
        return sum
    }

    fun validate() {
        validateProjectName()
        validateTaskProject()
    }

    private fun validateProjectName() {
        if (title.isEmpty() || title.isBlank()) {
            throw TitleNotSpecifiedException(EntityType.TASK.toString())
        }
    }

    private fun validateTaskProject() {
        if (parent == null) {
            throw ProjectNotSelectedException()
        }
    }
    // endregion
}
