@file:JvmName("Converter")

package com.fuzzythread.tyming.global.utils.time

import android.content.res.Resources
import com.fuzzythread.tyming.global.db.entities.TimeEntryEntity
import org.joda.time.DateTime
import org.joda.time.Period
import java.util.*
import kotlin.math.round
import kotlin.math.roundToInt


fun fromTenthsToSeconds(tenths: Long): String {
    return if (tenths < 600) {
        String.format("%.1f", tenths / 10.0)
    } else {
        val secondsAll = (tenths / 10)
        val seconds = secondsAll % 60
        val minutes = secondsAll / 60 % 60
        val hours = secondsAll / (60 * 60) % 24

        if (hours > 0) {
            String.format("%02d:%02d:%02d", hours, minutes, seconds)
        } else {
            String.format("%02d:%02d", minutes, seconds)
        }
    }
}

fun fromTenthsToSecondsProgress(timeElapsed: Long): Int {
    return ((timeElapsed) % 600).toInt()
}

fun fromTenthsToMinutesProgress(timeElapsed: Long): Int {
    return (((timeElapsed / 10) / 60).toInt()) % 60
}

fun fromTenthsToHoursProgress(timeElapsed: Long): Int {
    val minutes = ((timeElapsed / 10) / 60).toInt()
    return minutes / 60
}

fun cleanSecondsString(seconds: String): Long {
    // Remove letters and other characters
    val filteredValue = seconds.replace(Regex("""[^\d:.]"""), "")
    if (filteredValue.isEmpty()) return 0
    val elements: List<Int> = filteredValue.split(":").map { it -> round(it.toDouble()).toInt() }

    var result: Long
    return when {
        elements.size > 2 -> 0
        elements.size > 1 -> {
            result = (elements[0] * 60).toLong()
            result += elements[1]
            result * 10
        }
        else -> (elements[0] * 10).toLong()
    }
}

fun formatTimeToString(time: Float): String {
    return String.format("%02d:%02d", time.toInt(), round((time - time.toInt()) * 60).toInt())
}

fun formatTimeToString(time: Double): String {
    return String.format("%02d:%02d", time.toInt(), round((time - time.toInt()) * 60).toInt())
}

fun formatTimeToString(period: Period): String {
    return String.format("%02d:%02d", period.toStandardHours().hours + (period.toStandardDays().days * 24), period.toStandardMinutes().minutes % 60)
}

fun formatTimeStartEnd(startDate: DateTime, endDate: DateTime): String {
    return String.format("%02d:%02d - %02d:%02d", startDate.hourOfDay, startDate.minuteOfHour, endDate.hourOfDay, endDate.minuteOfHour)
}

fun formatTimeStartEnd(startDate: Date, endDate: Date): String {
    val startDateDT = DateTime(startDate)
    val endDateDT = DateTime(endDate)
    return String.format("%02d:%02d - %02d:%02d", startDateDT.hourOfDay, startDateDT.minuteOfHour, endDateDT.hourOfDay, endDateDT.minuteOfHour)
}

fun formatTimeToString(hours: Int, minutes: Int): String {
    return String.format("%02d:%02d", hours, minutes)
}

fun formatTimeToDouble(period: Period): Double {
    return (period.toStandardHours().hours + (period.toStandardDays().days * 24) + (period.toStandardMinutes().minutes % 60F / 60).toDouble())
}

fun formatTimeHrsMin(time: Float): String {
    val hours = time.toInt()
    val minutes = round((time - time.toInt()) * 60).toInt()
    return if (minutes > 0) {
        "${hours}h ${minutes}m"
    } else {
        "${hours}h"
    }
}


fun sumTimeEntriesPeriods(list: List<TimeEntryEntity>): Float {
    var sum = 0f
    list.forEach {
        val period = Period(DateTime(it.startDate), DateTime(it.endDate)).withSeconds(0)
        sum += period.hours + period.days * 24
        sum += period.minutes / 60f
    }
    return sum
}


fun DateTime.clearTime(): DateTime {
    return this
            .withMillisOfSecond(0)
            .withSecondOfMinute(0)
            .withMinuteOfHour(0)
            .withHourOfDay(0)
}

fun Date.isBefore(dateToCompare: Date): Boolean {
    return DateTime(this).isBefore(DateTime(dateToCompare))
}

fun Int.toPx(): Int {
    val metrics = Resources.getSystem().displayMetrics
    val px = this * (metrics.densityDpi / 160f)
    return px.roundToInt()
}

fun DateTime.isCurrentWeek (today:DateTime): Boolean {
    return today.clearTime().withDayOfWeek(1) == this.clearTime().withDayOfWeek(1)
}

