package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model

import android.content.Context
import com.fuzzythread.tyming.global.db.GeneralRepository
import com.fuzzythread.tyming.global.db.dao.AreaDao
import com.fuzzythread.tyming.global.db.dao.ProjectDao
import com.fuzzythread.tyming.global.db.dao.TaskDao
import com.fuzzythread.tyming.global.db.dao.TimeEntryDao
import com.fuzzythread.tyming.global.db.entities.ProjectEntity
import com.fuzzythread.tyming.global.services.TimerService
import com.fuzzythread.tyming.global.utils.extensions.toEntity
import com.fuzzythread.tyming.global.utils.other.DisplayCompletedTasks
import com.fuzzythread.tyming.global.utils.other.SortBy
import com.fuzzythread.tyming.screens.home.model.NavigationItem

open class HomeFragmentRepository(
        private val areaDao: AreaDao,
        private val projectDao: ProjectDao,
        private val taskDao: TaskDao,
        timeEntryDao: TimeEntryDao,
        private val context: Context
) : GeneralRepository(areaDao, projectDao, taskDao, timeEntryDao, context) {

    fun getNavigationData(isArchived: Boolean = false, displayCompletedTasks: DisplayCompletedTasks = DisplayCompletedTasks.VISIBLE, sortBy: SortBy = SortBy.DESC): MutableList<NavigationItem<*>> {
        val list: MutableList<NavigationItem<*>> = mutableListOf()
        val singularProjectsNavItems = createProjectsNavigationItems(projectDao.getSingularProjects(isArchived = isArchived, sortBy = sortBy), displayCompletedTasks, sortBy)

        // get areas and their projects
        val areaNavItems = mutableListOf<NavigationItem<*>>()
        areaDao.getAllSorted(sortBy = sortBy).forEach {
            val areaVM = AreaViewModel(it, context)
            val areaProjectsSorted = areaDao.getAreaProjectsSorted(it.id, isArchived = isArchived, sortBy = sortBy)
            val areaProjectsNavigationItems = createProjectsNavigationItems(areaProjectsSorted, displayCompletedTasks, sortBy = sortBy)

            if (areaVM.isArchived == isArchived || areaProjectsSorted.isNotEmpty()) {
                areaNavItems.add(createAreaNavItem(areaVM, areaProjectsSorted.size))
                if (!areaVM.collapsed) {
                    areaNavItems.addAll(areaProjectsNavigationItems)
                }
            }
        }
        // order: singular projects, areas
        list.addAll(singularProjectsNavItems)
        list.addAll(areaNavItems)
        return list
    }

    fun createProjectsNavigationItems(projects: MutableList<ProjectEntity>, displayCompletedTasks: DisplayCompletedTasks = DisplayCompletedTasks.VISIBLE, sortBy: SortBy = SortBy.DESC): MutableList<NavigationItem<*>> {
        val projectNavItems = mutableListOf<NavigationItem<*>>()

        if (sortBy == SortBy.CUSTOM){
            projects.toViewModels()
                    .forEach { projectNavItems.addAll(createProjectWithTasksNavigationItem(it, displayCompletedTasks, sortBy).navItems)  }
        } else {
            projects.toViewModels()
                    .filter { !it.completed }
                    .forEach { projectNavItems.addAll(createProjectWithTasksNavigationItem(it, displayCompletedTasks, sortBy).navItems)  }

            // put completed projects at the end:
            projects.toViewModels()
                    .filter { it.completed }
                    .sortedByDescending { it.completedDate }
                    .forEach { projectNavItems.addAll(createProjectWithTasksNavigationItem(it, displayCompletedTasks, sortBy).navItems) }
        }

        return projectNavItems
    }

    fun createProjectWithTasksNavigationItem(projectVM: ProjectViewModel, displayCompletedTasks: DisplayCompletedTasks = DisplayCompletedTasks.VISIBLE, sortBy: SortBy = SortBy.DESC): ProjectNavigationData {
        val projectTasksNavItems = mutableListOf<NavigationItem<*>>()
        projectTasksNavItems.add(createProjectNavItem(projectVM))
        if (!projectVM.collapsed) {
            projectTasksNavItems.addAll(getProjectTasksNavigationItems(projectVM, displayCompletedTasks, sortBy))
        }
        return ProjectNavigationData(navItems = projectTasksNavItems, isCompleted = projectVM.completed)
    }

    fun getProjectTasksNavigationItems(projectVM: ProjectViewModel, displayCompletedTasks: DisplayCompletedTasks = DisplayCompletedTasks.VISIBLE, sortBy: SortBy = SortBy.DESC): MutableList<NavigationItem<TaskViewModel>> {
        val tasksNavItems = mutableListOf<NavigationItem<TaskViewModel>>()

        projectDao.getProjectTasksSorted(projectVM.id, false, sortBy).forEach {
            tasksNavItems.add(createTaskNavItem(createTaskVmFromEntity(it), projectVM))
        }

        // completed tasks:
        if (displayCompletedTasks == DisplayCompletedTasks.VISIBLE || projectVM.completed) {
            projectDao.getProjectTasksSorted(projectVM.id, true).forEach {
                tasksNavItems.add(createTaskNavItem(createTaskVmFromEntity(it), projectVM))
            }
        }
        return tasksNavItems
    }

    fun markAreaArchived(areaId: Long, timerService: TimerService? = null, markArchived: Boolean) {
        areaDao.archive(areaId, timerService, markArchived = markArchived)
    }

    fun markProjectArchived(projectId: Long, timerService: TimerService? = null, markArchived: Boolean) {
        projectDao.archive(projectId, timerService, markArchived = markArchived)
    }

    fun deleteNavigationItem(navigationItem: NavigationItem<*>, timerService: TimerService? = null) {
        when (navigationItem.getItemType()) {
            NavigationItem.AREA -> {
                areaDao.deleteById((navigationItem.getItemEntity() as AreaViewModel).id)
            }
            NavigationItem.TASK -> {
                taskDao.deleteById((navigationItem.getItemEntity() as TaskViewModel).id)
            }
            else -> {
                projectDao.deleteByIdWithTimerService((navigationItem.getItemEntity() as ProjectViewModel).id, timerService)
            }
        }
    }

    fun createAreaNavItem(areaVM: AreaViewModel, projectsCount: Int): NavigationItem<AreaViewModel> {
        areaVM.projectCount = projectsCount
        return NavigationItem(areaVM).apply {
            id = areaVM.id
            title = areaVM.title
            color = areaVM.color
            childNumber = areaVM.projectCount
            isCollapsed = areaVM.collapsed
            displayOrder = areaVM.displayOrder
        }
    }

    fun createProjectNavItem(projectVM: ProjectViewModel): NavigationItem<ProjectViewModel> {
        return NavigationItem(projectVM).apply {
            id = projectVM.id
            title = projectVM.title
            color = projectVM.color
            timeTracked = projectVM.timeTracked
            lastWorked = projectVM.lastWorkedFormatted
            childNumber = projectVM.tasks.size
            progress = projectVM.progress
            trackProgress = projectVM.trackProgress
            deadlineDate = projectVM.deadlineDate
            completed = projectVM.completed
            isCollapsed = projectVM.collapsed
            displayOrder = projectVM.displayOrder
        }
    }

    fun createTaskNavItem(taskVM: TaskViewModel, projectVM: ProjectViewModel): NavigationItem<TaskViewModel> {
        return NavigationItem(taskVM).apply {
            id = taskVM.id
            title = taskVM.title
            color = projectVM.color
            timeTracked = taskVM.timeTracked
            lastWorked = taskVM.lastWorkedFormatted
            deadlineDate = taskVM.deadlineDate
            completed = taskVM.completed
            displayOrder = taskVM.displayOrder
        }
    }

    fun getTimersVMs(): MutableList<TimerViewModel> {
        val list: MutableList<TimerViewModel> = mutableListOf()
        val tasksEntities = taskDao.getRunningTasks()
        tasksEntities.forEach { taskEntity ->
            val parentByType = when {
                taskEntity.project.targetId > 0 -> getProjectEntity(taskEntity.project.targetId)
                else -> null
            }
            TimerViewModel(context).apply {
                id = taskEntity.id
                notes = taskEntity.notes
                title = taskEntity.title
                parent = parentByType
                timeEntries = taskEntity.timeEntries
                deadlineDate = taskEntity.deadlineDate
                hasTags = taskEntity.tags.size > 0
                completedDate = taskEntity.completedDate
                timeElapsed = taskEntity.timeElapsed
            }.also {
                list.add(it)
            }
        }
        return list
    }

    fun stopTaskTimer(taskId: Long) {
        taskDao.get(taskId)?.apply {
            isTimerRunning = false
        }?.also {
            taskDao.update(it)
        }
    }

    fun collapseArea(areaId: Long) {
        areaDao.collapse(areaId)
    }

    fun collapseProject(projectId: Long) {
        projectDao.collapse(projectId)
    }

    fun collapseProjects() {
        projectDao.collapseAll()
    }

    fun getRunningTasksCount(): Long = taskDao.getRunningTasksCount()

    fun MutableList<ProjectEntity>.toViewModels(): List<ProjectViewModel> {
        return this.map { createProjectVmFromEntity(it) }
    }

    fun updateDisplayOrder(navItems: MutableList<NavigationItem<*>>) {
        var singularProjectsOrder = 1L
        var areasOrder = 1L

        var areaVM: AreaViewModel? = null
        var singularProjectVM: ProjectViewModel? = null
        val areaProjectsVMs = mutableListOf<ProjectViewModel>()
        val projectTasksVMs = mutableListOf<TaskViewModel>()

        navItems.forEachIndexed { index, navigationItem ->
            when (navigationItem.getItemType()) {
                NavigationItem.PROJECT -> {
                    if (areaVM != null) {
                        // area - project
                        if (areaProjectsVMs.isNotEmpty()) {
                            saveProjectOrder(areaProjectsVMs.last(), projectTasksVMs)
                            projectTasksVMs.clear()
                        }
                        areaProjectsVMs.add(navigationItem.getItemEntity() as ProjectViewModel)
                    } else {
                        // singular project
                        singularProjectVM?.apply {
                            displayOrder = singularProjectsOrder++
                            area = null
                            saveProjectOrder(this, projectTasksVMs)
                            areaProjectsVMs.clear()
                            projectTasksVMs.clear()
                        }
                        singularProjectVM = navigationItem.getItemEntity() as ProjectViewModel
                    }
                }
                NavigationItem.AREA -> {
                    areaVM?.apply {
                        displayOrder = areasOrder++
                        saveAreaOrder(this, areaProjectsVMs, projectTasksVMs)
                        areaProjectsVMs.clear()
                        projectTasksVMs.clear()
                    }
                    areaVM = navigationItem.getItemEntity() as AreaViewModel

                    // singular project reset:
                    singularProjectVM?.apply {
                        displayOrder = singularProjectsOrder++
                        area = null
                        saveProjectOrder(this, projectTasksVMs)
                        areaProjectsVMs.clear()
                        projectTasksVMs.clear()
                    }
                    singularProjectVM = null
                }
                NavigationItem.TASK -> {
                    // project - tasks:
                    if (areaProjectsVMs.isNotEmpty() || singularProjectVM != null) {
                        projectTasksVMs.add(navigationItem.getItemEntity() as TaskViewModel)
                    }
                }
            }

            // last item:
            if (index == navItems.size - 1) {
                if (singularProjectVM != null) {
                    singularProjectVM?.apply {
                        displayOrder = singularProjectsOrder++
                        saveProjectOrder(this, projectTasksVMs)
                    }
                } else if (areaVM != null) {
                    areaVM?.apply {
                        displayOrder = areasOrder++
                        saveAreaOrder(this, areaProjectsVMs, projectTasksVMs)
                    }
                }
            }
        }
    }

    private fun saveProjectOrder(projectVM: ProjectViewModel, tasksVMs: MutableList<TaskViewModel>) {
        projectDao.update(projectVM.toEntity())

        var taskDisplayOrder = 1L
        val tasksEntities = tasksVMs.map { it.toEntity().apply { project.targetId = projectVM.id; displayOrder = taskDisplayOrder++ } }
        taskDao.update(*tasksEntities.toTypedArray())
    }

    private fun saveAreaOrder(areaVM: AreaViewModel, projectsVMs: MutableList<ProjectViewModel>, tasksVMs: MutableList<TaskViewModel>) {
        areaDao.update(areaVM.toEntity())

        var projectDisplayOrder = 1L
        val projectEntities = projectsVMs.map { it.toEntity().apply { area.targetId = areaVM.id; displayOrder = projectDisplayOrder++ } }
        projectDao.update(*projectEntities.toTypedArray())

        if (projectEntities.isNotEmpty()) {
            var taskDisplayOrder = 1L
            val tasksEntities = tasksVMs.map { it.toEntity().apply { project.targetId = projectEntities.last().id; displayOrder = taskDisplayOrder++ } }
            taskDao.update(*tasksEntities.toTypedArray())
        }
    }

    fun clearTutorialData() {
        areaDao.deleteById(1L, ignoreVersion = true)
        projectDao.deleteById(1L, ignoreVersion = true)
        projectDao.deleteById(2L, ignoreVersion = true)
        projectDao.deleteById(3L, ignoreVersion = true)
    }

    data class ProjectNavigationData(val navItems: MutableList<NavigationItem<*>>, val isCompleted: Boolean)
}

