package com.fuzzythread.tyming.global.db.dao

import com.fuzzythread.tyming.global.db.dao.util.Dao
import com.fuzzythread.tyming.global.db.entities.TaskEntity
import com.fuzzythread.tyming.global.db.entities.TaskEntity_
import com.fuzzythread.tyming.global.db.entities.TimeEntryEntity
import com.fuzzythread.tyming.global.db.entities.TimeEntryEntity_
import com.fuzzythread.tyming.global.utils.extensions.endOfDay
import com.fuzzythread.tyming.global.utils.extensions.startOfDay
import io.objectbox.BoxStore
import io.objectbox.android.ObjectBoxLiveData
import org.joda.time.DateTime
import java.util.*

class TaskDao(store: BoxStore) : Dao<TaskEntity>(store) {

    override fun addOperation(entity: TaskEntity): Long {
        return taskBox.put(entity)
    }

    override fun updateOperation(vararg entity: TaskEntity) {
        store.runInTx {
            taskBox.put(*entity)
        }
    }

    override fun deleteOperation(entity: TaskEntity) {
        deleteTaskTimeEntriesById(entity.id)
        taskBox.remove(entity.id)
    }

    override fun deleteByIdOperation(id: Long) {
        deleteTaskTimeEntriesById(id)
        taskBox.remove(id)
    }

    override fun get(id: Long): TaskEntity? {
        return taskBox.get(id)
    }

    override fun getAll(): MutableList<TaskEntity> {
        return taskBox.all
    }

    override fun amount(): Long {
        return taskBox.count()
    }

    private fun deleteTaskTimeEntriesById(taskId: Long) {
        store.runInTx {
            timeEntryBox
                    .query()
                    .equal(TimeEntryEntity_.taskId, taskId)
                    .build()
                    .find()
                    .forEach {
                        timeEntryBox.remove(it)
                    }
        }
    }

    fun getRunningTasks(): MutableList<TaskEntity> {
        return taskBox
                .query()
                .equal(TaskEntity_.isTimerRunning, true)
                .isNull(TaskEntity_.completedDate)
                .order(TaskEntity_.timeElapsed)
                .build()
                .find()
    }

    fun getRunningTasksCount(): Long {
        return taskBox
                .query()
                .equal(TaskEntity_.isTimerRunning, true)
                .isNull(TaskEntity_.completedDate)
                .order(TaskEntity_.timeElapsed)
                .build()
                .count()
    }

    fun getCompletedTasksByDate(startDate: Date, endDate: Date): MutableList<TaskEntity> {
        return taskBox
                .query()
                .between(TaskEntity_.completedDate, startDate, endDate)
                .notNull(TaskEntity_.completedDate)
                .orderDesc(TaskEntity_.completedDate)
                .build()
                .find()
    }

    fun getCompletedTasks(): MutableList<TaskEntity> {
        return taskBox
                .query()
                .notNull(TaskEntity_.completedDate)
                .orderDesc(TaskEntity_.completedDate)
                .build()
                .find()
    }

    fun getTasksEndingTodayDeadline(): MutableList<TaskEntity> {
        val todayStart = DateTime().startOfDay().toDate()
        val todayEnd = DateTime().endOfDay().toDate()

        return taskBox
                .query()
                .notNull(TaskEntity_.deadlineDate)
                .and()
                .between(TaskEntity_.deadlineDate, todayStart, todayEnd)
                .orderDesc(TaskEntity_.createdDate)
                .build()
                .find()
    }

    fun getTasksByDeadline(date: DateTime): MutableList<TaskEntity> {
        val dayStart = DateTime(date).startOfDay().toDate()
        val dayEnd = DateTime(date).endOfDay().toDate()

        return taskBox
                .query()
                .notNull(TaskEntity_.deadlineDate)
                .and()
                .between(TaskEntity_.deadlineDate, dayStart, dayEnd)
                .orderDesc(TaskEntity_.createdDate)
                .build()
                .find()
    }

    fun getTasksAmountByDeadline(date: DateTime): Int {
        val dayStart = DateTime(date).startOfDay().toDate()
        val dayEnd = DateTime(date).endOfDay().toDate()

        return taskBox
                .query()
                .notNull(TaskEntity_.deadlineDate)
                .and()
                .between(TaskEntity_.deadlineDate, dayStart, dayEnd)
                .orderDesc(TaskEntity_.createdDate)
                .build()
                .find()
                .count()
    }

    fun getTasksWithDeadline(): MutableList<TaskEntity> {
        return taskBox
                .query()
                .notNull(TaskEntity_.deadlineDate)
                .orderDesc(TaskEntity_.deadlineDate)
                .build()
                .find()
    }

    fun getTasksWithDeadlineStartDate(): Date? {
        return taskBox
                .query()
                .notNull(TaskEntity_.deadlineDate)
                .order(TaskEntity_.deadlineDate)
                .build()
                .findFirst()?.deadlineDate
    }

    fun getTasksWithDeadlineEndDate(): Date? {
        return taskBox
                .query()
                .notNull(TaskEntity_.deadlineDate)
                .orderDesc(TaskEntity_.deadlineDate)
                .build()
                .findFirst()?.deadlineDate
    }

    fun updateTasks(runningTimers: Map<Long, Long>) {
        store.runInTx {
            runningTimers.forEach {
                get(it.key)?.apply {
                    this.timeElapsed = it.value
                }?.also { taskEntity ->
                    this@TaskDao.update(taskEntity, ignoreVersion = true)
                }
            }
        }
    }

    fun swapTasks(fromTask: Long, toTask: Long) {
        store.runInTx {
            val taskFrom = taskBox.get(fromTask)
            val taskTo = taskBox.get(toTask)
            if (taskFrom != null && taskTo != null){
                val taskFromDate = taskFrom.createdDate
                val taskToDate = taskTo.createdDate

                taskFrom.createdDate = taskToDate
                taskBox.put(taskFrom)

                taskTo.createdDate = taskFromDate
                taskBox.put(taskTo)
            }
        }
    }
}
