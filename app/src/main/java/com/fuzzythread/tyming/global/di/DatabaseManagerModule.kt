package com.fuzzythread.tyming.global.di

import com.fuzzythread.tyming.global.db.AppDatabase
import android.content.Context
import com.fuzzythread.tyming.global.db.dao.AreaDao
import com.fuzzythread.tyming.global.db.dao.ProjectDao
import com.fuzzythread.tyming.global.db.dao.TaskDao
import com.fuzzythread.tyming.global.db.dao.TimeEntryDao
import com.fuzzythread.tyming.global.services.syncing.DatabaseManager
import com.fuzzythread.tyming.global.services.syncing.LocalDbSnapshotFactory
import com.fuzzythread.tyming.global.utils.time.DefaultTimer
import dagger.Module
import dagger.Provides
import javax.inject.Inject
import javax.inject.Singleton

@Module
class DatabaseManagerModule {
    @Provides
    @Singleton
    fun provideDatabaseBuilder(areaDao: AreaDao, projectDao: ProjectDao, taskDao: TaskDao, timeEntryDao: TimeEntryDao): DatabaseManager {
        return DatabaseManager(areaDao, projectDao, taskDao, timeEntryDao)
    }

    @Provides
    @Singleton
    fun provideLocalDatabaseFactory(areaDao: AreaDao, projectDao: ProjectDao, taskDao: TaskDao, timeEntryDao: TimeEntryDao): LocalDbSnapshotFactory {
        return LocalDbSnapshotFactory(areaDao, projectDao, taskDao, timeEntryDao)
    }
}
