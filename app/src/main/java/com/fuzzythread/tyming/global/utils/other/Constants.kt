package com.fuzzythread.tyming.global.utils.other

class Constants {
    interface ACTION {
        companion object {
            const val START_FOREGROUND = "com.fuzzythread.tyming.notification.action.start_foreground"
            const val STOP_FOREGROUND = "com.fuzzythread.tyming.notification.action.stop_foreground"
            const val SCREEN_OFF = "com.fuzzythread.tyming.notification.action.screen_off"
            const val SCREEN_ON = "com.fuzzythread.tyming.notification.action.screen_on"
            const val PAUSE_TIMER = "com.fuzzythread.tyming.notification.action.stop_timer"
            const val RESUME_TIMER = "com.fuzzythread.tyming.notification.action.resume_timer"
            const val DISMISS = "com.fuzzythread.tyming.notification.action.dismiss"
        }
    }

    interface NOTIFICATION {
        companion object {
            const val CHANNEL_ID = "com.fuzzythread.tyming.notification.channel_id"
            const val ID = 101
            const val CHANNEL_NAME = "Timer foreground notification channel"
        }
    }

    companion object {
        const val APP_TAG = "TYMING_APP"
        const val TIMERS_LIMIT = 5
        const val RC_SIGN_IN = 9001
    }
}

interface TUTORIAL {
    companion object {
        const val PROJECTS = "Projects"
        const val DASHBOARD = "Dashboard"
        const val JOURNAL = "Journal"
        const val STATISTICS = "Statistics"
    }
}

const val WEEK_LENGTH = 7

enum class ParentType { PROJECT, AREA, NONE }

enum class EditingType { TASK, PROJECT }

enum class EntityType { TASK, PROJECT, AREA, TIMERECORD }

enum class GoalStatus { GOOD, OVER, UNDER, EMPTY, UPCOMING }

enum class ActionType { ADD, EDIT, DELETE }

enum class ActivityType { JOURNAL, STATS, PROJECTS }

enum class TimeFrame { WEEK, MONTH }

enum class SortBy { DESC, ASC, CUSTOM }

enum class DisplayMode { CLUSTER, STARTEND }

enum class DisplayCompletedTasks { VISIBLE, HIDDEN }

