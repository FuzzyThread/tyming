package com.fuzzythread.tyming.global.ui.elements

import android.content.Context
import androidx.coordinatorlayout.widget.CoordinatorLayout
import android.util.AttributeSet
import android.widget.FrameLayout

import com.fuzzythread.tyming.global.ui.behaviour.MoveUpwardBehavior

@CoordinatorLayout.DefaultBehavior(MoveUpwardBehavior::class)
class CustomFrameLayout : FrameLayout {
    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}
}
