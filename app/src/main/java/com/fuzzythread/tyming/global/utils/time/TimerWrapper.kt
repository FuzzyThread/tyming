package com.fuzzythread.tyming.global.utils.time

import java.util.*
import javax.inject.Inject

interface TimerInterface {

    fun reset()
    fun start(task: TimerTask)
    fun count()
    fun getCounter(): Long
    fun setCounter(value: Long)
}

private const val TIMER_PERIOD_MS = 100L

class DefaultTimer @Inject constructor() : TimerInterface {

    var timerCounter = 0L

    private var timer = java.util.Timer()

    override fun count() {
        timerCounter++
    }

    override fun setCounter(value: Long) {
        timerCounter = value
    }

    override fun getCounter(): Long {
        return timerCounter
    }

    override fun reset() {
        timer.cancel()
    }

    override fun start(task: TimerTask) {
        timer = java.util.Timer()
        timer.scheduleAtFixedRate(task, 0, TIMER_PERIOD_MS)
    }
}
