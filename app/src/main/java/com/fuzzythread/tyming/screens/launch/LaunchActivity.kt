package com.fuzzythread.tyming.screens.launch

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.ViewModelProvider
import com.fuzzythread.tyming.BuildConfig
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.databinding.ActivityLaunchBinding
import com.fuzzythread.tyming.global.TymingActivity
import com.fuzzythread.tyming.global.di.ViewModelFactory
import com.fuzzythread.tyming.global.services.syncing.Synchronizator
import com.fuzzythread.tyming.global.utils.extensions.allPermissionsGranted
import com.fuzzythread.tyming.global.utils.other.Constants
import com.fuzzythread.tyming.global.utils.other.Constants.Companion.RC_SIGN_IN
import com.fuzzythread.tyming.global.utils.other.PrefUtil
import com.fuzzythread.tyming.global.utils.other.SortBy
import com.fuzzythread.tyming.global.utils.ui.markAllTutorialsCompleted
import com.fuzzythread.tyming.screens.launch.model.LaunchRepository
import com.fuzzythread.tyming.screens.home.ui.HomeActivity
import com.fuzzythread.tyming.screens.launch.fragments.PermissionsFragment
import com.fuzzythread.tyming.screens.launch.fragments.SignUpFragment
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import timber.log.Timber
import javax.inject.Inject

class LaunchActivity : TymingActivity() {

    @Inject
    lateinit var synchronizator: Synchronizator

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private val repository: LaunchRepository by lazy {
        ViewModelProvider(this, viewModelFactory).get(LaunchRepository::class.java)
    }

    private val fragments: MutableList<Fragment> = mutableListOf()
    private var auth: FirebaseAuth? = null
    private var googleSignInClient: GoogleSignInClient? = null
    private lateinit var binding: ActivityLaunchBinding


    // region Android hooks
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLaunchBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initAuth()

        val permissionsFragment = PermissionsFragment()
        val signUpFragment = SignUpFragment()
        fragments.add(permissionsFragment)
        fragments.add(signUpFragment)

        val adapter = ScreenSlidePagerAdapter(supportFragmentManager)
        binding.launchAdapter.disableScroll(true)
        binding.launchAdapter.adapter = adapter
        binding.launchAdapter.offscreenPageLimit = 3

        if (allPermissionsGranted(this)){
            showSignUpFragment()
        } else {
            // no permissions granted yet:
            if (!PrefUtil.getIsAppInitialized(this)){
                setUpDotsIndicator()
            }
        }
    }

    override fun onDestroy() {
        synchronizator.removeCallback()
        super.onDestroy()
    }
    // endregion

    // region UI
    private fun setUpDotsIndicator(){
        binding.dotsIndicator.visibility = View.VISIBLE
        binding.dotsIndicator.setViewPager(binding.launchAdapter)
    }
    // endregion

    // region Data
    private fun initAuth() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

        googleSignInClient = GoogleSignIn.getClient(this, gso)
        auth = FirebaseAuth.getInstance()
    }

    fun signIn() {
        val signInIntent = googleSignInClient?.signInIntent
        startActivityForResult(signInIntent!!, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Result returned from launching the Intent
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)!!
                firebaseAuthWithGoogle(account.idToken!!)
            } catch (e: ApiException) {
                Timber.tag(Constants.APP_TAG).e("Google sign in failed")
                Toast.makeText(this, getString(R.string.google_sign_in_failed), Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        auth?.signInWithCredential(credential)?.addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                Timber.tag(Constants.APP_TAG).d("SignInWithCredential:success")
                auth?.currentUser?.email?.also { currentEmail ->
                    PrefUtil.setUserEmail(currentEmail, this)
                    Toast.makeText(this, getString(R.string.welcome_back), Toast.LENGTH_LONG).show()
                    PrefUtil.setIsAppInitialized(true, this)
                    initializeApp(isNewUser = false)
                }
            } else {
                Timber.tag(Constants.APP_TAG).d("SignInWithCredential:failure")
                Toast.makeText(this, getString(R.string.auth_failed), Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun showNavigation() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        finish()
    }

    fun showSignUpFragment() {
        if (PrefUtil.getIsAppInitialized(this)) {
            showNavigation()
        } else {
            binding.launchAdapter.setCurrentItem(1, true)
        }
    }

    fun startNewUserJourney() {
        initializeApp(isNewUser = true)
    }

    private fun initializeApp(isNewUser: Boolean) {
        PrefUtil.setIsAppInitialized(true, this)

        // Based on app versions:
        PrefUtil.setMultipleTimersEnabled(BuildConfig.IS_PREMIUM, this)
        PrefUtil.setShowDeadlineNotifications(true, this)
        PrefUtil.setSortByProjects(SortBy.CUSTOM, this)
        PrefUtil.setIsTutorialDataCleared(!isNewUser, this)

        if (isNewUser){
            if (this::viewModelFactory.isInitialized) {
                repository.generateTutorialData()
            }
        } else { // existing user:
            markAllTutorialsCompleted()
            PrefUtil.setAutoSync(true, this)
            synchronizator.sync()
        }
        showNavigation()
    }
    // endregion

    private inner class ScreenSlidePagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
        override fun getCount(): Int = 2
        override fun getItem(position: Int): Fragment = fragments[position]
    }
}
