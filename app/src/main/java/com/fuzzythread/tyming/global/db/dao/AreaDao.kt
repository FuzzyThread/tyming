package com.fuzzythread.tyming.global.db.dao

import com.fuzzythread.tyming.global.db.dao.util.Dao
import com.fuzzythread.tyming.global.db.entities.*
import com.fuzzythread.tyming.global.services.TimerService
import com.fuzzythread.tyming.global.utils.other.SortBy
import io.objectbox.BoxStore
import io.objectbox.android.ObjectBoxLiveData
import java.util.*

class AreaDao(store: BoxStore) : Dao<AreaEntity>(store) {

    override fun addOperation(entity: AreaEntity): Long {
        return areaBox.put(entity)
    }

    override fun updateOperation(vararg entity: AreaEntity) {
        store.runInTx {
            areaBox.put(*entity)
        }
    }

    override fun deleteOperation(entity: AreaEntity) {
        areaBox.remove(entity)
    }

    override fun deleteByIdOperation(id: Long) {
        areaBox.remove(id)
    }

    override fun get(id: Long): AreaEntity? {
        return if (id > 0) {
            areaBox.get(id)
        } else {
            null
        }
    }

    override fun getAll(): MutableList<AreaEntity> {
        return areaBox.all
    }

    override fun amount(): Long {
        return areaBox.count()
    }

    fun getAllSorted(isArchived: Boolean, sortBy: SortBy = SortBy.DESC): MutableList<AreaEntity> {
        val query = areaBox.query()

        if (isArchived) {
            query.notNull(AreaEntity_.archivedDate)
        } else {
            query.isNull(AreaEntity_.archivedDate)
        }

        when (sortBy) {
            SortBy.DESC -> {
                query.orderDesc(AreaEntity_.createdDate)
            }
            SortBy.ASC -> {
                query.order(AreaEntity_.createdDate)
            }
            SortBy.CUSTOM -> {
                query.order(AreaEntity_.displayOrder)
            }
        }

        return query
                .build()
                .find()
    }

    fun getAllSorted(sortBy: SortBy = SortBy.DESC): MutableList<AreaEntity> {
        val query = areaBox.query()

        when (sortBy) {
            SortBy.DESC -> {
                query.orderDesc(AreaEntity_.createdDate)
            }
            SortBy.ASC -> {
                query.order(AreaEntity_.createdDate)
            }
            SortBy.CUSTOM -> {
                query.order(AreaEntity_.displayOrder)
            }
        }

        return query
                .build()
                .find()
    }

    fun getAreaProjectsSorted(areaId: Long, isArchived: Boolean, sortBy: SortBy = SortBy.DESC): MutableList<ProjectEntity> {
        val query = projectBox.query()

        if (isArchived) {
            query.notNull(ProjectEntity_.archivedDate)
        } else {
            query.isNull(ProjectEntity_.archivedDate)
        }

        query.and().equal(ProjectEntity_.areaId, areaId)

        when (sortBy) {
            SortBy.DESC -> {
                query.orderDesc(ProjectEntity_.createdDate)
            }
            SortBy.ASC -> {
                query.order(ProjectEntity_.createdDate)
            }
            SortBy.CUSTOM -> {
                query.order(ProjectEntity_.displayOrder)
            }
        }

        return query.build().find()
    }

    fun collapse(areaId: Long) {
        areaBox.get(areaId)?.apply {
            if (projects.size > 0) {
                isCollapsed = !isCollapsed
                areaBox.put(this)

                // collapse area projects:
                store.runInTx {
                    this.projects.forEach { projectEntity ->
                        projectEntity.isCollapsed = true
                        projectBox.put(projectEntity)
                    }
                }
            } else {
                isCollapsed = true
                areaBox.put(this)
            }
        }
    }

    fun archive(areaId: Long, timerService: TimerService? = null, markArchived: Boolean) {
        var archiveDate: Date? = null
        if (markArchived) {
            archiveDate = Date()
        }
        store.runInTx {
            updateVersion()
            areaBox.get(areaId)?.apply {
                archivedDate = archiveDate
                isCollapsed = true
                areaBox.put(this)

                this.projects.forEach { projectEntity ->
                    projectEntity.archivedDate = archiveDate
                    projectBox.put(projectEntity)

                    projectEntity.tasks.forEach { taskEntity ->

                        if (taskEntity.isTimerRunning) {
                            // Stop and save timer:
                            timerService?.stopTimer(taskEntity.id, showToast = false)
                            taskEntity.isTimerRunning = false
                            taskBox.put(taskEntity)
                        }
                        taskEntity.timeEntries.forEach { timeEntryEntity ->
                            timeEntryEntity.archivedDate = archiveDate
                            timeEntryBox.put(timeEntryEntity)
                        }
                    }
                }
            }
        }
    }
}
