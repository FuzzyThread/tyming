package com.fuzzythread.tyming.global.ui.binding

import android.animation.ObjectAnimator
import android.content.res.Resources
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.widget.*
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.global.ui.elements.PieProgressDrawable
import com.fuzzythread.tyming.global.utils.other.EditingType
import com.fuzzythread.tyming.global.utils.other.GoalStatus
import com.fuzzythread.tyming.global.utils.other.ParentType
import com.fuzzythread.tyming.global.utils.ui.convertDpToPx


class BindingAdapters {

    companion object {

        @JvmStatic
        @BindingAdapter("margin_start")
        fun setLayoutMarginStart(view: View, startMargin: Int) {
            val layoutParams = view.layoutParams as MarginLayoutParams
            layoutParams.setMargins(convertDpToPx(view.context, startMargin), layoutParams.topMargin,
                    layoutParams.rightMargin, layoutParams.bottomMargin)
            view.layoutParams = layoutParams
        }

//        @JvmStatic
//        @BindingAdapter("layout_width")
//        fun setLayoutWidth(view: View, width: Float) {
//            val layoutParams = view.layoutParams
//            layoutParams.width = convertDpToPx(view.context, width)
//            view.layoutParams = layoutParams
//        }

        @JvmStatic
        @BindingAdapter("pie_progress", "pie_color", "completable")
        fun setPieProgress(imageView: ImageView, progress: Int, pieColor: String, completable: Boolean) {
            val progressDrawable = PieProgressDrawable(imageView.context.resources.displayMetrics)
            imageView.setImageDrawable(progressDrawable)
            progressDrawable.setColor(Color.parseColor(pieColor))
            progressDrawable.level = progress
            progressDrawable.completable = completable
            imageView.invalidate()
        }

        @JvmStatic
        @BindingAdapter("timer_running")
        fun setPieProgress(imageView: ImageView, timerRunning: Boolean) {
            if (!timerRunning) {
                imageView.setBackgroundResource(R.drawable.ic_timer_start)
            } else {
                imageView.setBackgroundResource(R.drawable.ic_timer_stop)
            }
            imageView.invalidate()
        }

        @JvmStatic
        @BindingAdapter("width")
        fun setWidth(imageView: ImageView, double: Double) {
            if (double < 0.1) {
                imageView.layoutParams.width = (imageView.layoutParams.width * 0.1).toInt()
            } else {
                imageView.layoutParams.width = (imageView.layoutParams.width * double).toInt()
            }
            imageView.invalidate()
        }

        @JvmStatic
        @BindingAdapter("parent", "parent_color")
        fun setParentDrawable(imageView: ImageView, parentType: ParentType, parentColor: String) {
            when (parentType) {
                ParentType.PROJECT -> {
                    val progressDrawable = PieProgressDrawable(imageView.context.resources.displayMetrics)
                    imageView.setImageDrawable(progressDrawable)
                    progressDrawable.setColor(Color.parseColor(parentColor))
                    progressDrawable.level = 100
                    imageView.invalidate()
                }
                ParentType.AREA -> {
                    val drawable = AppCompatResources.getDrawable(imageView.context, R.drawable.ic_area)!!
                    imageView.setImageDrawable(drawable)
                    imageView.invalidate()
                }
                ParentType.NONE -> {
                    val drawable = AppCompatResources.getDrawable(imageView.context, R.drawable.ic_root)!!
                    imageView.setImageDrawable(drawable)
                    imageView.invalidate()
                }
            }
        }

        @JvmStatic
        @BindingAdapter("parent", "parent_color", "editing_type")
        fun setParentDrawable(imageView: ImageView, parentType: ParentType, parentColor: String, editingType: EditingType) {
            when (parentType) {
                ParentType.PROJECT -> {
                    val progressDrawable = PieProgressDrawable(imageView.context.resources.displayMetrics)
                    imageView.setImageDrawable(progressDrawable)
                    progressDrawable.setColor(Color.parseColor(parentColor))
                    progressDrawable.level = 100
                    imageView.invalidate()
                }
                ParentType.AREA -> {
                    val drawable = AppCompatResources.getDrawable(imageView.context, R.drawable.ic_area)!!
                    imageView.setImageDrawable(drawable)
                    imageView.invalidate()
                }
                ParentType.NONE -> {
                    val drawable = if (editingType == EditingType.TASK) {
                        val progressDrawable = PieProgressDrawable(imageView.context.resources.displayMetrics)
                        imageView.setImageDrawable(progressDrawable)
                        progressDrawable.setColor(Color.parseColor(parentColor))
                        progressDrawable.level = 100
                        progressDrawable
                    } else {
                        AppCompatResources.getDrawable(imageView.context, R.drawable.ic_root)!!
                    }
                    imageView.setImageDrawable(drawable)
                    imageView.invalidate()
                }
            }
        }

        @JvmStatic
        @BindingAdapter("no_parent_type")
        fun setNoParentDrawable(imageView: ImageView, editingType: EditingType) {
            val drawable: Drawable

            if (editingType == EditingType.PROJECT) {
                drawable = AppCompatResources.getDrawable(imageView.context, R.drawable.ic_root)!!
            } else {
                drawable = PieProgressDrawable(imageView.context.resources.displayMetrics)
                imageView.setImageDrawable(drawable)
                drawable.setColor((ContextCompat.getColor(imageView.context, R.color.blue_600)))
                drawable.level = 100
            }

            imageView.setImageDrawable(drawable)
            imageView.invalidate()
        }


        @JvmStatic
        @BindingAdapter("android:layout_marginBottom")
        fun setBottomMargin(view: View, bottomMargin: Float) {
            val layoutParams = view.layoutParams as MarginLayoutParams
            layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin,
                    layoutParams.rightMargin, Math.round(bottomMargin))
            view.layoutParams = layoutParams
        }

        @JvmStatic
        @BindingAdapter("layout_width")
        fun setLayoutWidth(view: View, width: Float) {
            val layoutParams = view.layoutParams
            layoutParams.width = convertDpToPx(view.context,width)
            view.layoutParams = layoutParams
        }

        @JvmStatic
        @BindingAdapter("src")
        fun setImageViewResource(imageView: ImageView, resource: Int) {
            imageView.setImageDrawable(AppCompatResources.getDrawable(imageView.context, resource))
        }

        @JvmStatic
        @BindingAdapter("goal_status")
        fun setImageViewResource(imageView: AppCompatImageView, goalStatus: GoalStatus) {
            when (goalStatus) {
                GoalStatus.EMPTY -> {
                    imageView.setImageResource(0)
                }
                GoalStatus.GOOD -> {
                    updateLayoutParams(imageView, marginStart = 0, marginEnd = 0, marginBottom = 6, width = 24f, height = 24f)
                    imageView.setImageResource(R.drawable.ic_good_goal_icon)
                }
                GoalStatus.OVER -> {
                    updateLayoutParams(imageView, marginStart = 1, marginEnd = 0, marginBottom = 6, width = 20f, height = 20f)
                    imageView.setImageResource(R.drawable.ic_over_goal_icon)
                }
                GoalStatus.UNDER -> {
                    updateLayoutParams(imageView, marginStart = 0, marginEnd = 1, marginBottom = 6, width = 20f, height = 20f)
                    imageView.setImageResource(R.drawable.ic_under_goal_icon)
                }
                GoalStatus.UPCOMING -> {
                    updateLayoutParams(imageView, marginStart = 0, marginEnd = 0, marginBottom = 6, width = 15f, height = 15f)
                    imageView.setImageResource(R.drawable.ic_empty)
                }
            }
            imageView.invalidate()
        }


        private fun updateLayoutParams(view: View, marginStart: Int, marginEnd: Int, marginBottom: Int, width: Float, height: Float) {
            val layoutParams = view.layoutParams as ConstraintLayout.LayoutParams
            layoutParams.width = convertDpToPx(view.context, width)
            layoutParams.height = convertDpToPx(view.context, height)
            layoutParams.setMargins(
                    convertDpToPx(view.context, marginStart),
                    layoutParams.topMargin,
                    convertDpToPx(view.context, marginEnd),
                    convertDpToPx(view.context, marginBottom))
            view.layoutParams = layoutParams
        }

        @JvmStatic
        @BindingAdapter("strike_through")
        fun setTextViewStrikeThrough(textView: TextView, value: Boolean) {
            if (value) {
                textView.paintFlags = textView.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            } else {
                textView.paintFlags = 0
            }
        }

        @JvmStatic
        @BindingAdapter("format_24_hours")
        fun set24HoursResource(imageView: TimePicker, value: Boolean) {
            imageView.setIs24HourView(value)
        }

        @JvmStatic
        @BindingAdapter("format_day_month")
        fun setDayMonthFormat(imageView: DatePicker, value: Boolean) {
            val year = imageView.findViewById<View>(Resources.getSystem().getIdentifier("android:id/year", null, null))
            if (year != null) {
                year.visibility = View.GONE
            }
        }

        @JvmStatic
        @BindingAdapter("timer_progress")
        fun setTimerProgress(progressBar: ProgressBar, progress: Long) {
            val seconds = ((progress + 10) / 10) - 1
            val actualSeconds = seconds % 60
            val animation: ObjectAnimator = ObjectAnimator.ofInt(progressBar, "progress", progressBar.progress, actualSeconds.toInt() * 100)
            animation.duration = 950
            animation.interpolator = DecelerateInterpolator()
            animation.start()
        }
    }
}
