package com.fuzzythread.tyming.global.db.entities

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import io.objectbox.relation.ToMany
import java.util.*

@Entity
class TagEntity {
    @Id(assignable = true)
    var id: Long = 0
    var name: String = ""
    var createdDate: Date? = null

    // region relations
    lateinit var tasks: ToMany<TaskEntity>
    // endregion

    constructor()

    constructor(name: String, createdDate: Date, taskId: Long) {
        this.name = name
        this.createdDate = createdDate
    }

}