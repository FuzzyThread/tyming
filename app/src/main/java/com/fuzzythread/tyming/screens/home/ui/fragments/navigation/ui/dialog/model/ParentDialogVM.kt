package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.dialog.model

import android.content.Context
import androidx.databinding.Bindable
import com.fuzzythread.tyming.BR
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.global.db.GeneralRepository
import com.fuzzythread.tyming.global.db.dao.AreaDao
import com.fuzzythread.tyming.global.db.dao.ProjectDao
import com.fuzzythread.tyming.global.db.dao.TaskDao
import com.fuzzythread.tyming.global.db.dao.TimeEntryDao
import com.fuzzythread.tyming.global.utils.other.EditingType
import com.fuzzythread.tyming.global.utils.other.ParentType
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.ParentViewModel

class ParentDialogVM(
        private val areaDao: AreaDao,
        private val projectDao: ProjectDao,
        taskDao: TaskDao,
        timeEntryDao: TimeEntryDao,
        private val context: Context
) : GeneralRepository(areaDao, projectDao, taskDao, timeEntryDao, context) {

    var parentId: Long = 0
    var areasVMs: MutableList<ParentViewModel> = mutableListOf()
    var projectsVMs: MutableList<ParentViewModel> = mutableListOf()

    init {
        areasVMs = getParentAreaVMs(isArchived = false)
        projectsVMs = getParentProjectsVMs(isArchived = false)
    }

    @get:Bindable
    var parentType: ParentType = ParentType.NONE
        set(value) {
            field = value
            notifyPropertyChanged(BR.parentType)
            notifyPropertyChanged(BR.parentTitle)
            notifyPropertyChanged(BR.parentColor)
            notifyPropertyChanged(BR.areaTitle)
            notifyPropertyChanged(BR.projectTitle)
            notifyPropertyChanged(BR.editingType)
        }

    @get:Bindable
    var editingType: EditingType = EditingType.TASK
        set(value) {
            field = value
            notifyPropertyChanged(BR.editingType)
        }

    @get:Bindable
    var parentColor: String = "#1e88e5"
        get() {
            return when (parentType) {
                ParentType.PROJECT -> {
                    getProjectEntity(parentId)?.color ?: "#1e88e5"
                }
                ParentType.AREA -> {
                    getAreaEntity(parentId)?.color ?: "#1e88e5"
                }
                ParentType.NONE -> {
                    "#1e88e5"
                }
            }
        }

    @get:Bindable
    var parentTitle: String = context.getString(R.string.not_selected)
        get() {
            return when (parentType) {
                ParentType.PROJECT -> {
                    getProjectEntity(parentId)?.title ?: context.getString(R.string.choose_project)
                }
                ParentType.AREA -> {
                    getAreaEntity(parentId)?.title ?: context.getString(R.string.choose_area)
                }
                ParentType.NONE -> {
                    return when (editingType) {
                        EditingType.TASK -> {
                            context.getString(R.string.not_selected)
                        }
                        EditingType.PROJECT -> {
                            context.getString(R.string.root)
                        }
                    }
                }
            }
        }

    @get:Bindable
    val noParentTitle: String
        get() {
            return if (editingType == EditingType.TASK) {
                context.getString(R.string.not_selected)
            } else {
                context.getString(R.string.root)
            }
        }

    @get:Bindable
    val areaTitle: String
        get() {
            return if (parentType == ParentType.AREA) {
                getAreaEntity(parentId)?.title ?: context.getString(R.string.select_area)
            } else {
                context.getString(R.string.select_area)
            }
        }

    @get:Bindable
    val projectTitle: String
        get() {
            return if (parentType == ParentType.PROJECT) {
                getProjectEntity(parentId)?.title ?: context.getString(R.string.select_project)
            } else {
                context.getString(R.string.select_project)
            }
        }

    @get:Bindable
    var editing: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.editing)
        }

    // region custom functions

    fun initialize(parentId: Long, parentType: ParentType) {
        this.parentId = parentId
        this.parentType = parentType
    }

    // endregion

    // region Database

    private fun getParentAreaVMs(isArchived: Boolean): MutableList<ParentViewModel> {
        val list: MutableList<ParentViewModel> = mutableListOf()

        areaDao.getAllSorted(isArchived = isArchived).forEach {
            ParentViewModel().apply {
                id = it.id
                title = it.title.capitalize()
                color = it.color
                type = ParentType.AREA
            }.also {
                list.add(it)
            }
        }
        return list
    }

    private fun getParentProjectsVMs(isArchived: Boolean): MutableList<ParentViewModel> {
        val list: MutableList<ParentViewModel> = mutableListOf()

        projectDao.getAllSorted(isArchived = isArchived).forEach {
            ParentViewModel().apply {
                id = it.id
                title = it.title.capitalize()
                color = it.color
                type = ParentType.PROJECT
            }.also {
                list.add(it)
            }
        }

        return list
    }

    // endregion
}
