package com.fuzzythread.tyming.global.ui.charts

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Path
import android.graphics.RectF
import com.github.mikephil.charting.animation.ChartAnimator
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.interfaces.dataprovider.BarDataProvider
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.renderer.BarChartRenderer
import com.github.mikephil.charting.utils.Utils
import com.github.mikephil.charting.utils.ViewPortHandler


class RoundedBarChartRenderer(chart: BarDataProvider,
                              animator: ChartAnimator,
                              viewPortHandler: ViewPortHandler,
                              private val mRadius: Float,
                              private val isMonthly: Boolean = false) :
        BarChartRenderer(chart, animator, viewPortHandler) {


    override fun drawHighlighted(c: Canvas?, indices: Array<out Highlight>?) {

        val barData = mChart.barData

        for (high in indices!!) {
            val set = barData.getDataSetByIndex(high.dataSetIndex)
            if (set == null || !set.isHighlightEnabled) continue
            val e = set.getEntryForXValue(high.x, high.y)
            if (!isInBoundsX(e, set)) continue
            val trans = mChart.getTransformer(set.axisDependency)
            mHighlightPaint.color = Color.parseColor("#282828")
            mHighlightPaint.alpha = 200
            val isStack = if (high.stackIndex >= 0 && e.isStacked) true else false
            val y1: Float
            val y2: Float
            if (isStack) {
                if (mChart.isHighlightFullBarEnabled) {
                    y1 = e.positiveSum
                    y2 = -e.negativeSum
                } else {
                    val range = e.ranges[high.stackIndex]
                    y1 = range.from
                    y2 = range.to
                }
            } else {
                y1 = e.y
                y2 = 0f
            }
            prepareBarHighlight(e.x, y1 + 0.1f, y2, barData.barWidth / 1.9f, trans)
            setHighlightDrawPos(high, mBarRect)
            c!!.drawRect(mBarRect, mHighlightPaint)
        }
    }

    private val mBarShadowRectBuffer = RectF()

    override fun drawDataSet(c: Canvas?, dataSet: IBarDataSet?, index: Int) {

        if (c == null || dataSet == null) return

        val trans = mChart.getTransformer(dataSet.axisDependency)

        mBarBorderPaint.color = dataSet.barBorderColor
        mBarBorderPaint.strokeWidth = Utils.convertDpToPixel(dataSet.barBorderWidth)

        val drawBorder = dataSet.barBorderWidth > 0f

        val phaseX = mAnimator.phaseX
        val phaseY = mAnimator.phaseY

        if (mBarBuffers != null) {
            // initialize the buffer
            val buffer = mBarBuffers[index]

            buffer.setPhases(phaseX, phaseY)
            buffer.setDataSet(index)
            buffer.setInverted(mChart.isInverted(dataSet.axisDependency))
            buffer.setBarWidth(mChart.barData.barWidth)

            buffer.feed(dataSet)

            trans.pointValuesToPixel(buffer.buffer)

            val isSingleColor = dataSet.colors.size == 1

            if (isSingleColor) {
                mRenderPaint.color = dataSet.color
            }

            var j = 0
            while (j < buffer.size()) {

                if (!mViewPortHandler.isInBoundsLeft(buffer.buffer[j + 2])) {
                    j += 4
                    continue
                }

                if (!mViewPortHandler.isInBoundsRight(buffer.buffer[j]))
                    break

                if (!isSingleColor) {
                    // Set the color for the currently drawn value. If the index
                    // is out of bounds, reuse colors.
                    mRenderPaint.color = dataSet.getColor(j / 4)
                }


                // TODO: define

                var path2: Path

                if (dataSet.stackSize > 1) {
                    if ((j + 4) / 4 == 1) {
                        // bottom part:
                        path2 = roundRect(RectF(buffer.buffer[j], buffer.buffer[j + 1], buffer.buffer[j + 2],
                                buffer.buffer[j + 3]), mRadius, mRadius, false, false, true, true)
                    } else if ((j + 4) / 4 == dataSet.stackSize) {
                        path2 = roundRect(RectF(buffer.buffer[j], buffer.buffer[j + 1], buffer.buffer[j + 2],
                                buffer.buffer[j + 3]), mRadius, mRadius, true, true, false, false)
                    } else {
                        // middle part:
                        path2 = roundRect(RectF(buffer.buffer[j], buffer.buffer[j + 1], buffer.buffer[j + 2],
                                buffer.buffer[j + 3]), mRadius, mRadius, false, false, false, false)
                    }
                } else {
                    path2 = roundRect(RectF(buffer.buffer[j], buffer.buffer[j + 1], buffer.buffer[j + 2],
                            buffer.buffer[j + 3]), mRadius, mRadius, true, true, true, true)
                }

                c.drawPath(path2, mRenderPaint)

//            if (drawBorder) {
//                c.drawRoundRect(RectF(buffer.buffer[j], buffer.buffer[j + 1], buffer.buffer[j + 2],
//                        buffer.buffer[j + 3]), mRadius, mRadius, mBarBorderPaint)
//            }
                j += 4
            }
        }
    }

    override fun drawValue(c: Canvas?, valueText: String?, x: Float, y: Float, color: Int) {
        if (isMonthly) {
            mValuePaint.color = color
            c?.save()
            c?.rotate(90f, x, y)
            c?.drawText(valueText ?: "", x -15f, y + 6f, mValuePaint)
            c?.restore()
        } else {
            mValuePaint.color = color
            c?.save()
            c?.drawText(valueText ?: "", x -5f, y, mValuePaint)
            c?.restore()
        }
    }

    private fun roundRect(rect: RectF, rx: Float, ry: Float, tl: Boolean, tr: Boolean, br: Boolean, bl: Boolean): Path {
        var rx = rx
        var ry = ry
        val top = rect.top
        val left = rect.left
        val right = rect.right
        val bottom = rect.bottom
        val path = Path()
        if (rx < 0) rx = 0f
        if (ry < 0) ry = 0f
        val width = right - left
        val height = bottom - top
        if (rx > width / 2) rx = width / 2
        if (ry > height / 2) ry = height / 2
        val widthMinusCorners = width - 2 * rx
        val heightMinusCorners = height - 2 * ry

        path.moveTo(right, top + ry)
        if (tr)
            path.rQuadTo(0F, -ry, -rx, -ry)//top-right corner
        else {
            path.rLineTo(0F, -ry)
            path.rLineTo(-rx, 0F)
        }
        path.rLineTo(-widthMinusCorners, 0F)
        if (tl)
            path.rQuadTo(-rx, 0F, -rx, ry) //top-left corner
        else {
            path.rLineTo(-rx, 0F)
            path.rLineTo(0F, ry)
        }
        path.rLineTo(0F, heightMinusCorners)

        if (bl)
            path.rQuadTo(0F, ry, rx, ry)//bottom-left corner
        else {
            path.rLineTo(0F, ry)
            path.rLineTo(rx, 0F)
        }

        path.rLineTo(widthMinusCorners, 0F)
        if (br)
            path.rQuadTo(rx, 0F, rx, -ry) //bottom-right corner
        else {
            path.rLineTo(rx, 0F)
            path.rLineTo(0F, -ry)
        }

        path.rLineTo(0F, -heightMinusCorners)

        path.close()//Given close, last lineto can be removed.

        return path
    }
}
