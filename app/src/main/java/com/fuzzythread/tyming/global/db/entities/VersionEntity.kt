package com.fuzzythread.tyming.global.db.entities

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import java.util.*

@Entity
data class VersionEntity(@Id(assignable = true) var id: Long = 0, var date: Date?, var number: Long) {}
