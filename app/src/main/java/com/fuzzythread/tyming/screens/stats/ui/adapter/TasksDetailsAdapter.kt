package com.fuzzythread.tyming.screens.stats.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fuzzythread.tyming.databinding.ItemTaskStatsBinding
import com.fuzzythread.tyming.global.utils.extensions.updateAdapterWithData
import com.fuzzythread.tyming.screens.stats.model.TaskStatsViewModel

class TasksDetailsAdapter(
        private val context: Context
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var recyclerView: RecyclerView? = null
    private var items: MutableList<TaskStatsViewModel> = mutableListOf()

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
        this.recyclerView?.layoutManager = LinearLayoutManager(context)
        this.recyclerView?.isNestedScrollingEnabled = false
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemBinding = ItemTaskStatsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TasksDetailsViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        with(holder as TasksDetailsViewHolder) {
            bind(items[position])
        }
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun refresh(items: MutableList<TaskStatsViewModel>) {
        val oldItems = this.items
        this.items = items
        // update RV using DiffUtil
        updateAdapterWithData(oldItems, this.items)
    }

    inner class TasksDetailsViewHolder(
            private val itemBinding: ItemTaskStatsBinding,
            private val view: View = itemBinding.root
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(parentVM: TaskStatsViewModel) {
            // bind repository:
            itemBinding.viewmodel = parentVM
            itemBinding.executePendingBindings()
        }
    }
}
