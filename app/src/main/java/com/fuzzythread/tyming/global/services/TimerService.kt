package com.fuzzythread.tyming.global.services

import android.app.PendingIntent
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import android.view.View
import android.widget.RemoteViews
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.global.db.dao.TaskDao
import com.fuzzythread.tyming.global.db.dao.TimeEntryDao
import com.fuzzythread.tyming.global.db.entities.TimeEntryEntity
import com.fuzzythread.tyming.global.utils.other.Constants
import com.fuzzythread.tyming.global.utils.other.PrefUtil
import kotlinx.coroutines.*
import org.joda.time.LocalDateTime
import timber.log.Timber
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicLong
import javax.inject.Inject
import android.content.IntentFilter
import android.content.BroadcastReceiver
import android.widget.Toast
import com.fuzzythread.tyming.global.TymingService
import com.fuzzythread.tyming.global.services.receivers.ScreenReceiver
import com.fuzzythread.tyming.global.services.timer.Timer
import com.fuzzythread.tyming.global.utils.other.Constants.Companion.APP_TAG
import com.fuzzythread.tyming.screens.home.ui.HomeActivity
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import kotlin.coroutines.CoroutineContext

class TimerService : TymingService() {

    private val timers: ConcurrentHashMap<Long, Timer> = ConcurrentHashMap()


    private var boundCallback: ((Map<Long, Long>) -> Unit)? = null


    private lateinit var remoteViews: RemoteViews
    private lateinit var notificationBuilder: NotificationCompat.Builder
    private var screenReceiver: BroadcastReceiver? = null
    private val gson = Gson()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + masterJob

    @Inject
    lateinit var timeEntryDao: TimeEntryDao

    @Inject
    lateinit var taskDao: TaskDao

    // region TIMER: Controls

    fun startTimer(taskId: Long) {
        if (!timers.containsKey(taskId)) {
            createTimer(taskId)
        }

        if (!isTimerRunning(taskId)) {
            timers[taskId]?.start()
            updateTask(taskId, 1)
        }
    }

    fun pauseTimer(taskId: Long) {
        if (timers.containsKey(taskId) && isTimerRunning(taskId)) {
            timers[taskId]?.pause()
        }
    }

    fun stopTimer(taskId: Long, showToast: Boolean = true) {
        if (timers.containsKey(taskId)) {
            updateTask(taskId, 0)
            val elapsedTime = timers[taskId]?.timeElapsed?.get() ?: 0
            timers[taskId]?.stop()
            timers.remove(taskId)

            val elapsedTimeInMinutes = elapsedTime.toInt() / 10 / 60
            if (elapsedTimeInMinutes > 0) {
                if (showToast) {
                    Toast.makeText(this.applicationContext, getString(R.string.saved_time_record), Toast.LENGTH_SHORT).show()
                }
                saveTimeEntry(taskId, elapsedTime)
            } else {
                if (showToast) {
                    Toast.makeText(this.applicationContext, getString(R.string.time_record_not_saved), Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun stopTimers() {
        timers.forEach {
            stopTimer(it.key)
        }
    }

    fun bindOnTimersCount(refreshTimeMills: Long = TIMER_REPEAT_MILLIS, callback: ((Map<Long, Long>) -> Unit)) {
        boundCallback = callback
        resetJob()
        launch(this.coroutineContext) {
            while (true) {
                val runningTimers = timers
                        .filter { it.value.timerState == TimerState.STARTED }
                        .map { it.key to it.value.timeElapsed.toLong() }
                        .toMap()

                // update time entry (only when bound) -> more gradient change:
                // TODO: check if persistent taskDao.updateTasks(runningTimers)
                boundCallback?.invoke(runningTimers)
                delay(refreshTimeMills)
            }
        }
    }

    private fun isTimerRunning(taskId: Long): Boolean {
        val timer = timers.get(key = taskId)
        return timer != null && timer.timerState == TimerState.STARTED
    }

    private fun createTimer(taskId: Long) {
        timers[taskId] = Timer(timeElapsed = AtomicLong(0), timerState = TimerState.PAUSED, coroutineScope = this)
    }

    fun unbindTimersCallback() {
        boundCallback = null
        masterJob.cancel()
    }
    // endregion

    // region PERSISTENCE: DB and PrefUtil

    private fun restoreTimersFromDB() {
        taskDao.getRunningTasks().forEach { task ->
            timers[task.id] = Timer(timeElapsed = AtomicLong(task.timeElapsed), timerState = TimerState.STARTED, coroutineScope = this)
        }
    }

    private fun saveTimersInDB() {
        timers.forEach { timer ->
            updateTask(timer.key, timer.value.timeElapsed.get())
        }
    }

    private fun saveTimeEntry(taskId: Long, timeElapsed: Long?) {
        if (taskId > 0 && timeElapsed != null)
            timeEntryDao.add(TimeEntryEntity(
                    notes = "",
                    location = "",
                    startDate = LocalDateTime.now().minusSeconds(timeElapsed.toInt() / 10).toDate(),
                    endDate = LocalDateTime.now().toDate(),
                    taskId = taskId)
            )
    }

    private fun updateTask(taskId: Long, timeElapsed: Long, ignoreVersion: Boolean = false) {
        taskDao.get(taskId)?.apply {
            this.timeElapsed = timeElapsed
            this.isTimerRunning = this.timeElapsed > 0
        }?.also {
            taskDao.update(it, ignoreVersion = ignoreVersion)
        }
    }

    private fun restoreTimersOnIdle() {
        val timersJSON = PrefUtil.getTimers(this)
        val gsonBuilder = GsonBuilder()
        val type = object : TypeToken<HashMap<Long, HashMap<String, Long>>>() {}.type
        val timersHashMap: HashMap<Long, HashMap<String, Long>> = gsonBuilder
                .create()
                .fromJson(timersJSON, type)

        val idleTime = ((System.currentTimeMillis() - PrefUtil.getIdleTime(this)) / 100)
        Timber.tag(APP_TAG).v("IDLE: $idleTime, RECEIVED JSON: ${gson.toJson(timersHashMap)}")
        for ((taskId, timerDetails) in timersHashMap) {
            val timerTimeElapsed = (timerDetails[TIMER_TIME_ELAPSED] ?: 0) + idleTime
            timers[taskId]?.timeElapsed?.set(timerTimeElapsed)
            updateTask(taskId, timerTimeElapsed)
        }
    }

    private fun saveTimersOnIdle() {
        PrefUtil.setIdleTime(System.currentTimeMillis(), this@TimerService)
        val timersHashMap = HashMap<Long, HashMap<String, Long>>()
        timers.forEach {
            val timerDetails = HashMap<String, Long>()
            timerDetails[TIMER_STATE] = it.value.timerState.ordinal.toLong()
            timerDetails[TIMER_TIME_ELAPSED] = it.value.timeElapsed.get()
            timersHashMap[it.key] = timerDetails
        }
        PrefUtil.setTimers(
                timersJSON = gson.toJson(timersHashMap),
                context = this
        )
        Timber.tag(APP_TAG).v("SEND JSON: ${gson.toJson(timersHashMap)}")
    }

    fun refreshAfterSync() {
        timers.clear()
        restoreTimersFromDB()
    }

    // endregion

    // region SERVICE: Overridden Methods

    override fun onCreate() {
        super.onCreate()
        restoreTimersFromDB()
        remoteViews = RemoteViews(packageName, R.layout.notification_timer)
        notificationBuilder = NotificationCompat.Builder(applicationContext, Constants.NOTIFICATION.CHANNEL_ID)

        // Detect screen off
        val filter = IntentFilter(Intent.ACTION_SCREEN_ON)
        filter.addAction(Intent.ACTION_SCREEN_OFF)
        screenReceiver = ScreenReceiver()
        registerReceiver(screenReceiver, filter)
    }

    override fun onUnbind(intent: Intent?): Boolean {
        super.onUnbind(intent)
        unbindTimersCallback()
        saveTimersInDB()
        return true
    }

    override fun getServiceName(): String {
        return "Timer"
    }

    private fun prepareAndStartForeground() {
        try {
            Timber.tag(APP_TAG).v("Started foreground notification")
            val intent = Intent(this, TimerService::class.java)
            ContextCompat.startForegroundService(this, intent)
            configureNotification()
            startForeground(Constants.NOTIFICATION.ID, notificationBuilder.build())
        } catch (e: Exception) {
            Timber.tag(APP_TAG).d("startForegroundNotification: %s", e.message)
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (timers.size > 0) {
            when {
                intent?.action.equals(Constants.ACTION.START_FOREGROUND) -> {
                    prepareAndStartForeground()
                }
                intent?.action.equals(Constants.ACTION.STOP_FOREGROUND) -> {
                    stopForeground(true)
                }
                intent?.action.equals(Constants.ACTION.SCREEN_ON) -> {
                    Timber.tag(APP_TAG).v("Screen on from service")
                    restoreTimersOnIdle()
                }
                intent?.action.equals(Constants.ACTION.SCREEN_OFF) -> {
                    Timber.tag(APP_TAG).v("Screen off from service")
                    saveTimersOnIdle()
                }
                intent?.action.equals(Constants.ACTION.DISMISS) -> {
                    stopTimers()
                    stopForeground(true)
                }
            }
        }
        if (intent == null) {
            return START_STICKY_COMPATIBILITY
        }
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        saveTimersInDB()
        masterJob.cancel()
        timers.clear()
        unregisterReceiver(screenReceiver)
    }

    // endregion

    // region NOTIFICATIONS

    private fun configureNotification() {
        val notificationIntent = Intent(this, HomeActivity::class.java)
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        val viewPI = PendingIntent.getActivity(this, 0, notificationIntent, 0)
        configureNotificationViews()
        notificationBuilder.setOngoing(true)
                .setSmallIcon(R.drawable.ic_timer)
                .setContent(remoteViews)
                .setContentIntent(viewPI)
                .setPriority(NotificationCompat.FLAG_ONLY_ALERT_ONCE)
    }

    private fun configureNotificationViews() {
        // open app:
        val pauseI = Intent(this, HomeActivity::class.java)
        pauseI.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        val pausePI = PendingIntent.getActivity(this, 0, pauseI, 0)

        remoteViews.setViewVisibility(R.id.pause_btn, View.VISIBLE)
        remoteViews.setOnClickPendingIntent(R.id.pause_btn, pausePI)

        // Project Task Name
        remoteViews.setTextViewText(R.id.project_name, "")
        remoteViews.setTextViewText(R.id.task_name, "Open timers")

        // dismiss timer
        val dismissI = Intent(this, TimerService::class.java)
        dismissI.action = Constants.ACTION.DISMISS
        val dismissPI = PendingIntent.getService(this, 0, dismissI, 0)
        remoteViews.setOnClickPendingIntent(R.id.dismiss_btn, dismissPI)

        // update text view
        remoteViews.setTextViewText(
                R.id.notification_count_down,
                "${timers.size} ${if (timers.size > 1) "timers are" else "timer is"} running")
    }

    // endregion

    companion object {
        const val TIMER_REPEAT_MILLIS = 100L
        const val TIMER_STATE = "TIMER_STATE"
        const val TIMER_TIME_ELAPSED = "TIMER_TIME_ELAPSED"
    }

    enum class TimerState { STOPPED, STARTED, PAUSED }
}





