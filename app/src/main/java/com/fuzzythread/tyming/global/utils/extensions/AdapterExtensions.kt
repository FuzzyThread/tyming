package com.fuzzythread.tyming.global.utils.extensions

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.fuzzythread.tyming.global.ui.helpers.AdapterDiffUtilCallback
import com.fuzzythread.tyming.global.ui.helpers.IAdapterComparable

/**
 * Compares two lists and refreshes only DIFFERENT tasks (or adds new / deletes removed tasks).
 * Items should implement [IAdapterComparable].
 * IMPORTANT: Before calling this function set your navigationAdapter data to the newList
 */
fun <VH : RecyclerView.ViewHolder?, T> RecyclerView.Adapter<VH>.updateAdapterWithData(oldList: MutableList<T>, newList: MutableList<T>)
        where T : IAdapterComparable<T> {
    val cb = AdapterDiffUtilCallback(oldList, newList)
    val result = DiffUtil.calculateDiff(cb)
    result.dispatchUpdatesTo(this)
}

/**
 * Compare data heavy operation to check if UI requires update
 * Use this function on background thread
 */
fun <T : IAdapterComparable<T>> isDataDifferent(oldList: MutableList<T>, newList: MutableList<T>): Boolean {
    var isDifferent = false
    if (oldList.size != newList.size) {
        isDifferent = true
    } else {
        oldList.forEachIndexed { index, item ->
            if (!newList[index].areItemsTheSame(item)) {
                isDifferent = true
                return@forEachIndexed
            }
            if (!newList[index].areContentsTheSame(item)) {
                isDifferent = true
                return@forEachIndexed
            }
        }
    }
    return isDifferent
}
