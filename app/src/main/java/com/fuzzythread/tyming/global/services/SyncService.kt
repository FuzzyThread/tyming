package com.fuzzythread.tyming.global.services

import android.content.Intent
import com.fuzzythread.tyming.global.TymingService
import com.fuzzythread.tyming.global.services.syncing.Synchronizator
import com.fuzzythread.tyming.global.utils.other.PrefUtil
import kotlinx.coroutines.*
import javax.inject.Inject


class SyncService : TymingService() {
    private var syncJob: Job = Job()

    @Inject
    lateinit var synchronizator: Synchronizator

    private fun startAutoSynchronization() {
        resetJob()
        launch(this.coroutineContext) {
            while (true) {
                if (PrefUtil.getIsAutoSyncEnabled(this@SyncService)) {
                    val asyncJob = async {
                        synchronizator.sync()
                    }
                    asyncJob.await()
                }
                delay(SYNC_REPEAT_MILLIS)
            }
        }
    }

    override fun onCreate() {
        super.onCreate()
        startAutoSynchronization()
    }

    override fun onDestroy() {
        synchronizator.removeCallback()
        syncJob.cancel()
        super.onDestroy()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent == null) {
            return START_STICKY_COMPATIBILITY
        }
        return START_STICKY
    }

    fun bindOnSyncing(callback: ((Boolean, Boolean) -> Unit)) {
        this.synchronizator.setCallback(callback)
    }

    override fun getServiceName(): String {
        return "Sync"
    }

    companion object {
        const val SYNC_REPEAT_MILLIS = 1000L * 10  // every 10 seconds
    }
}
