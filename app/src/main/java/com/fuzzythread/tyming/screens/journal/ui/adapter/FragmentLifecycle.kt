package com.fuzzythread.tyming.screens.journal.ui.adapter

interface FragmentLifecycle {
    fun onPauseFragment()
    fun onResumeFragment()
}
