package com.fuzzythread.tyming.viewmodels

import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.TimeEntryViewModel
import org.junit.Test

import org.junit.Assert.*
import java.util.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class TimeEntryVMUnitTest {
    @Test
    fun checkProjectVmField() {
        assertEquals(notes, TASK_VM.notes)
        assertEquals(id, TASK_VM.id)
        assertEquals(startDate, TASK_VM.startDate)
        assertEquals(endDate, TASK_VM.endDate)
    }

    @Test
    fun checkProjectVmFieldsEmpty() {

        val tempProjectVM = TimeEntryViewModel(context = null)

        assertEquals("", tempProjectVM.notes)
        assertEquals(0, tempProjectVM.id)
        assertEquals(null, tempProjectVM.startDate)
        assertEquals(null, tempProjectVM.endDate)
    }

    companion object {

        private val TASK_VM = TimeEntryViewModel(context = null)

        private const val notes = "Test note"
        private const val id = 1.toLong()
        private val startDate = Date()
        private val endDate = Date()

        init {
            TASK_VM.notes = notes
            TASK_VM.id = id
            TASK_VM.startDate = startDate
            TASK_VM.endDate = endDate
        }
    }
}
