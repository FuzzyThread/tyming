package com.fuzzythread.tyming.global.services.syncing

import com.fuzzythread.tyming.global.db.dao.AreaDao
import com.fuzzythread.tyming.global.db.dao.ProjectDao
import com.fuzzythread.tyming.global.db.dao.TaskDao
import com.fuzzythread.tyming.global.db.dao.TimeEntryDao
import com.fuzzythread.tyming.global.db.dao.util.Dao
import com.fuzzythread.tyming.global.db.entities.ProjectEntity
import com.fuzzythread.tyming.global.db.entities.TaskEntity
import com.fuzzythread.tyming.global.db.entities.TimeEntryEntity
import com.fuzzythread.tyming.global.db.entities.VersionEntity
import java.util.*
import javax.inject.Inject

class LocalDbSnapshotFactory @Inject constructor(
        var areaDao: AreaDao,
        var projectDao: ProjectDao,
        var taskDao: TaskDao,
        var timeEntryDao: TimeEntryDao
) : DbSnapshotFactory {

    override fun createDbSnapshot(): DbSnapshot {
        val areas = getAreas()
        val version = getLatestVersion()
        return DbSnapshot(version = version.number, date = version.date ?: Date(), areas = areas)
    }

    private fun getAreas(): MutableList<Area> {
        val areas = mutableListOf<Area>()
        // get areas and projects:
        areas.addAll(getAreasProjects())
        areas.add(getSingularProjects())
        return areas
    }

    private fun getAreasProjects(): MutableList<Area> {
        val areas = mutableListOf<Area>()
        // areas with projects
        areaDao.getAll().forEach { areaEntity ->
            Area(areaEntity.title,
                    areaEntity.notes,
                    areaEntity.color,
                    areaEntity.isCollapsed,
                    areaEntity.createdDate,
                    areaEntity.archivedDate,
                    projects = getProjects(areaEntity.projects)
            ).apply {
                areas.add(this)
            }
        }
        return areas
    }

    private fun getSingularProjects(): Area {
        val projects = getProjects(projectDao.getSingularProjects())
        return Area(singularProjectsPlaceholder = true, projects = projects)
    }

    private fun getProjects(projectEntities: List<ProjectEntity>): MutableList<Project> {
        val projects = mutableListOf<Project>()
        // projects
        projectEntities.forEach { projectEntity ->
            Project(projectEntity.title,
                    projectEntity.notes,
                    projectEntity.color,
                    projectEntity.trackProgress,
                    projectEntity.isCollapsed,
                    projectEntity.createdDate,
                    projectEntity.deadlineDate,
                    projectEntity.archivedDate,
                    getTasks(projectEntity.tasks)
            ).apply {
                projects.add(this)
            }
        }
        return projects
    }

    private fun getTasks(taskEntities: List<TaskEntity>): MutableList<Task> {
        val tasks: MutableList<Task> = mutableListOf()
        // tasks
        taskEntities.forEach { taskEntity ->
            Task(taskEntity.title,
                    taskEntity.notes,
                    taskEntity.createdDate,
                    taskEntity.deadlineDate,
                    taskEntity.completedDate,
                    taskEntity.timeElapsed,
                    taskEntity.isTimerRunning,
                    getTimeEntries(taskEntity.timeEntries)
            ).apply {
                tasks.add(this)
            }
        }
        return tasks
    }

    private fun getTimeEntries(timeEntryEntities: List<TimeEntryEntity>): MutableList<TimeEntry> {
        val timeEntries: MutableList<TimeEntry> = mutableListOf()
        // time entries
        timeEntryEntities.forEach { timeEntryEntity ->
            TimeEntry(timeEntryEntity.notes,
                    timeEntryEntity.location,
                    timeEntryEntity.startDate,
                    timeEntryEntity.endDate,
                    timeEntryEntity.archivedDate).apply {
                timeEntries.add(this)
            }
        }
        return timeEntries
    }

    fun getLatestVersion(): VersionEntity {
        return (areaDao as Dao<*>).getLatestVersion()
    }
}
