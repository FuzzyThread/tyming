package com.fuzzythread.tyming.global.db.entities.interfaces

import com.fuzzythread.tyming.global.utils.other.ParentType

interface ITaskParent {
    fun getParentType(): ParentType
}