package com.fuzzythread.tyming.db

import com.fuzzythread.tyming.global.db.entities.TaskEntity
import com.fuzzythread.tyming.global.db.entities.TaskEntity_
import com.fuzzythread.tyming.global.db.entities.TimeEntryEntity

import org.junit.Test

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull

class TaskUnitTest : AbstractObjectBoxTest() {

    @Test
    fun emptyTaskTest() {
        val taskBox = store.boxFor(TaskEntity::class.java)
        assertEquals(0, taskBox.all.size)
    }

    @Test
    fun addTaskTest() {
        val taskBox = store.boxFor(TaskEntity::class.java)
        taskBox.put(TaskUnitTest.TASK)
        assertEquals(1, taskBox.all.size)
    }

    @Test
    fun deleteTaskTest() {

        val taskBox = store.boxFor(TaskEntity::class.java)
        taskBox.put(TaskUnitTest.TASK)

        val dbTask = taskBox.query().build().findFirst()
        assertNotNull(dbTask)
        assertEquals(TaskUnitTest.TASK.notes, dbTask!!.notes)
        assertEquals(TaskUnitTest.TASK.title, dbTask.title)

        taskBox.remove(dbTask)
        assertEquals(0, taskBox.all.size)
    }

    @Test
    fun contentTaskTest() {

        val taskBox = store.boxFor(TaskEntity::class.java)
        taskBox.put(TaskUnitTest.TASK)

        val dbTask = taskBox.query().build().findFirst()
        assertNotNull(dbTask)
        assertEquals(TaskUnitTest.TASK.notes, dbTask!!.notes)
        assertEquals(TaskUnitTest.TASK.title, dbTask.title)
    }

    @Test
    fun findQueryTaskTest() {

        val taskBox = store.boxFor(TaskEntity::class.java)
        taskBox.put(TaskUnitTest.TASK)

        val dbTask = taskBox.query().equal(TaskEntity_.id, TaskUnitTest.TASK.id)
                .build().findFirst()
        assertNotNull(dbTask)
        assertEquals(TaskUnitTest.TASK.notes, dbTask!!.notes)
        assertEquals(TaskUnitTest.TASK.title, dbTask.title)
    }

    companion object {

        private val TASK = TaskEntity()

        init {
            TASK.notes = "Test note"
            TASK.title = "Test title"
        }
    }
}
