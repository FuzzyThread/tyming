package com.fuzzythread.tyming.global.services.syncing

interface DbSnapshotFactory {
    fun createDbSnapshot() : DbSnapshot
}
