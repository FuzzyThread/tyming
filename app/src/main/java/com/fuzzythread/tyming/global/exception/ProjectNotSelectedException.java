package com.fuzzythread.tyming.global.exception;

public class ProjectNotSelectedException extends RuntimeException {
    public ProjectNotSelectedException() {
        super("Please select a project");
    }
}
