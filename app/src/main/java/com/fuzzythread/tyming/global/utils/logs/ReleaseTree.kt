package com.fuzzythread.tyming.global.utils.logs

import android.util.Log.ERROR
import android.util.Log.WARN
import com.google.firebase.crashlytics.FirebaseCrashlytics
import timber.log.Timber

class ReleaseTree : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        if (priority == ERROR || priority == WARN) {
            t?.apply {
                FirebaseCrashlytics.getInstance().recordException(t)
            }
        }
    }
}
