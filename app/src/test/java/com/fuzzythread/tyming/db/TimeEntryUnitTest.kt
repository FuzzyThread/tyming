package com.fuzzythread.tyming.db

import com.fuzzythread.tyming.global.db.entities.TimeEntryEntity
import com.fuzzythread.tyming.global.db.entities.TimeEntryEntity_

import org.junit.Test

import java.util.Date

import io.objectbox.Box

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull

class TimeEntryUnitTest : AbstractObjectBoxTest() {

    @Test
    fun emptyTimeEntryTest() {
        val timeEntryBox = store.boxFor(TimeEntryEntity::class.java)
        assertEquals(0, timeEntryBox.all.size)
    }

    @Test
    fun addTimeEntryTest() {
        val timeEntryBox = store.boxFor(TimeEntryEntity::class.java)
        timeEntryBox.put(TIME_ENTRY)
        assertEquals(1, timeEntryBox.all.size)
    }

    @Test
    fun deleteTimeEntryTest() {

        val timeEntryBox = store.boxFor(TimeEntryEntity::class.java)
        timeEntryBox.put(TIME_ENTRY)

        val dbTimeEntry = timeEntryBox.query().build().findFirst()
        assertNotNull(dbTimeEntry)
        assertEquals(TIME_ENTRY.notes, dbTimeEntry!!.notes)
        assertEquals(TIME_ENTRY.endDate!!.time, dbTimeEntry.endDate!!.time)
        assertEquals(TIME_ENTRY.startDate!!.time, dbTimeEntry.startDate!!.time)

        timeEntryBox.remove(dbTimeEntry)
        assertEquals(0, timeEntryBox.all.size)
    }

    @Test
    fun contentTimeEntryTest() {

        val timeEntryBox = store.boxFor(TimeEntryEntity::class.java)
        timeEntryBox.put(TIME_ENTRY)

        val dbTimeEntry = timeEntryBox.query().build().findFirst()
        assertNotNull(dbTimeEntry)
        assertEquals(TIME_ENTRY.notes, dbTimeEntry!!.notes)
        assertEquals(TIME_ENTRY.endDate!!.time, dbTimeEntry.endDate!!.time)
        assertEquals(TIME_ENTRY.startDate!!.time, dbTimeEntry.startDate!!.time)
    }

    @Test
    fun findQueryTimeEntryTest() {

        val timeEntryBox = store.boxFor(TimeEntryEntity::class.java)
        timeEntryBox.put(TIME_ENTRY)

        val dbTimeEntry = timeEntryBox.query().equal(TimeEntryEntity_.id, TIME_ENTRY.id)
                .build().findFirst()
        assertNotNull(dbTimeEntry)
        assertEquals(TIME_ENTRY.notes, dbTimeEntry!!.notes)
        assertEquals(TIME_ENTRY.endDate!!.time, dbTimeEntry.endDate!!.time)
        assertEquals(TIME_ENTRY.startDate!!.time, dbTimeEntry.startDate!!.time)
    }

    companion object {

        private val TIME_ENTRY = TimeEntryEntity()

        init {
            TIME_ENTRY.notes = "Test note"
            TIME_ENTRY.endDate = Date()
            TIME_ENTRY.startDate = Date()
        }
    }
}
