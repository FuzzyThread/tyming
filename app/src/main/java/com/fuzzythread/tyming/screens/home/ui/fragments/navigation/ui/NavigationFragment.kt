package com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.ProgressBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.ViewCompat
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.databinding.FragmentNavigationBinding
import com.fuzzythread.tyming.databinding.SpotlightTargetBinding
import com.fuzzythread.tyming.global.TymingActivity
import com.fuzzythread.tyming.global.TymingService
import com.fuzzythread.tyming.global.di.ViewModelFactory
import com.fuzzythread.tyming.global.services.SyncService
import com.fuzzythread.tyming.global.services.TimerService
import com.fuzzythread.tyming.global.ui.elements.TaggableFragment
import com.fuzzythread.tyming.global.utils.other.*
import com.fuzzythread.tyming.global.utils.ui.*
import com.fuzzythread.tyming.screens.home.model.NavigationItem
import com.fuzzythread.tyming.screens.home.ui.HomeActivity
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.HomeFragmentRepository
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.TaskViewModel
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.model.TimerViewModel
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.adapter.CustPageTransformer
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.adapter.NavigationAdapter
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.adapter.Refreshable
import com.fuzzythread.tyming.screens.home.ui.fragments.navigation.ui.adapter.TimersAdapter
import com.fuzzythread.tyming.screens.journal.ui.dialog.FiltersDialog
import com.fuzzythread.tyming.screens.search.ui.SearchActivity
import com.takusemba.spotlight.Target
import kotlinx.coroutines.*
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class NavigationFragment : TaggableFragment(), CoroutineScope, Refreshable {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private val repository: HomeFragmentRepository by lazy {
        ViewModelProvider(this, viewModelFactory).get(HomeFragmentRepository::class.java)
    }

    private var masterJob = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + masterJob

    private var separator: View? = null
    private var clearTutorialData: ConstraintLayout? = null
    private var dismissBtn: TextView? = null
    private var searchContainer: ConstraintLayout? = null
    private var sortBy: SortBy = SortBy.DESC
    private var displayCompletedTasks: DisplayCompletedTasks = DisplayCompletedTasks.VISIBLE
    private var isCreated: Boolean = false
    private var navigationRV: RecyclerView? = null
    private var emptyStage: ConstraintLayout? = null
    private var container: CoordinatorLayout? = null
    private var timersData: MutableList<TimerViewModel> = mutableListOf()
    private var timersAdapter: TimersAdapter? = null
    private var timerService: TimerService? = null
    private var syncService: SyncService? = null
    private var timerServiceConnection: ServiceConnection? = null
    private var syncServiceConnection: ServiceConnection? = null
    private var isTimerServiceBound: Boolean = false
    private var isSyncServiceBound: Boolean = false
    private var timersRV: ViewPager2? = null
    private var nestedScrollView: NestedScrollView? = null
    private var progressBar: ProgressBar? = null
    private var syncProgressBar: ProgressBar? = null
    private var navigationItems: MutableList<NavigationItem<*>> = mutableListOf()
    private var navigationAdapter: NavigationAdapter? = null

    // region Android hooks
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentNavigationBinding.inflate(inflater, container, false)
        initializeUI(binding)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCreated = true
    }

    override fun onResume() {
        super.onResume()
        isTimerServiceBound = false
        isSyncServiceBound = false
        refreshAsync(refreshTimers = true, collapseProjects = true)
    }

    override fun onPause() {
        super.onPause()
        Timber.tag(Constants.APP_TAG).d("HomeFragment paused: unbind timer service")
        clear()
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.tag(Constants.APP_TAG).d("HomeFragment destroyed: unbind timer service")
        clear()
    }
    //endregion

    // region Services
    private fun bindServices() {
        if (!isTimerServiceBound) {
            bindTimerService()
        }
        if (!isSyncServiceBound) {
            bindSyncService()
        }
    }

    private fun bindSyncService() {
        syncServiceConnection = object : ServiceConnection {
            override fun onServiceConnected(componentName: ComponentName?, iBinder: IBinder?) {
                // bind service:
                if (syncService == null) {
                    syncService = (iBinder as TymingService.LocalBinder).service as SyncService
                }

                launch(Dispatchers.IO) {
                    delay(1000)
                    // syncing indicator:
                    syncService?.bindOnSyncing { isLoading, isUiUpdateRequired ->
                        if (updateTimers) {
                            activity?.runOnUiThread {
                                if (isLoading) {
                                    syncProgressBar?.visibility = View.VISIBLE
                                } else {
                                    syncProgressBar?.visibility = View.GONE
                                }
                                if (isUiUpdateRequired) {
                                    showProgress()
                                    refreshAll()
                                    timerService?.refreshAfterSync()
                                }
                            }
                        }
                    }
                }
            }

            override fun onServiceDisconnected(componentName: ComponentName?) {
                syncService = null
                isSyncServiceBound = false
                Timber.tag(Constants.APP_TAG).v("Sync service disconnected")
            }
        }

        if (!isSyncServiceBound) {
            activity?.bindService(Intent(context, SyncService::class.java), syncServiceConnection!!, Context.BIND_AUTO_CREATE)
        }
        isSyncServiceBound = true
    }

    private fun bindTimerService() {
        timerServiceConnection = object : ServiceConnection {
            override fun onServiceConnected(componentName: ComponentName?, iBinder: IBinder?) {
                // bind service:
                if (timerService == null) {
                    timerService = (iBinder as TymingService.LocalBinder).service as TimerService
                }

                launch(Dispatchers.Default) {
                    delay(1000)
                    timersAdapter?.setTimerService(timerService!!)
                    navigationAdapter?.setTimerService(timerService!!)
                    timerService?.bindOnTimersCount(1000L) { timers ->
                        if (timers.isNotEmpty()) {
                            timers.forEach { timer ->
                                val task = timersAdapter?.items?.find { it.id == timer.key }
                                if (updateTimers) {
                                    activity?.runOnUiThread {
                                        task?.timeElapsed = timer.value
                                    }
                                }
                                // update items:
                                val navTask = navigationAdapter?.items?.find { it.isTask() && (it.getItemEntity() as TaskViewModel).id == timer.key }
                                if (navTask != null && updateTimers) {
                                    activity?.runOnUiThread {
                                        (navTask.getItemEntity() as TaskViewModel).timeElapsed = timer.value
                                    }
                                }
                            }
                        }
                    }
                }
            }

            override fun onServiceDisconnected(componentName: ComponentName?) {
                timerService = null
                isTimerServiceBound = false
                Timber.tag(Constants.APP_TAG).v("Timer service disconnected")
            }
        }
        if (!isTimerServiceBound) {
            activity?.bindService(Intent(context, TimerService::class.java), timerServiceConnection!!, Context.BIND_AUTO_CREATE)
        }
        isTimerServiceBound = true
    }

    private fun unbindService(serviceConnection: ServiceConnection?, serviceBound: Boolean) {
        if (serviceConnection != null && serviceBound) {
            try {
                activity?.unbindService(serviceConnection)
            } catch (ignore: Exception) {
                println(ignore)
            }
        }
    }

    private fun clear() {
        masterJob.cancel()
        timerService?.unbindTimersCallback()
        unbindService(timerServiceConnection, isTimerServiceBound)
        unbindService(syncServiceConnection, isSyncServiceBound)
    }
    // endregion

    // region Data
    private fun loadFilters() {
        val context: Context = this.context ?: return
        displayCompletedTasks = PrefUtil.getDisplayCompletedTasksVisible(context)
        sortBy = PrefUtil.getSortByProjects(context)
    }

    private fun refreshAsync(refreshTimers: Boolean = true, collapseProjects: Boolean = false) {
        // reset previous job:
        masterJob.cancel()
        masterJob = Job()

        launch(this.coroutineContext) {
            val refresh = async {
                refreshData(collapseProjects)
            }
            refresh.await()
            withContext(Dispatchers.Main) {
                updateUI(refreshTimers)
            }
        }
    }

    private fun refreshData(collapseProjects: Boolean = false) {
        loadFilters()
        if (this::viewModelFactory.isInitialized) {
            if (collapseProjects) {
                repository.collapseProjects()
            }
            navigationItems = repository.getNavigationData(isArchived = false, displayCompletedTasks = displayCompletedTasks, sortBy = sortBy)
            timersData = repository.getTimersVMs()
        }
    }

    override fun refreshFragment() {
        if (isCreated) {
            updateTimers = true
            refreshAsync()
        }
    }

    override fun refreshSelf(refreshTimers: Boolean) {
        refreshAsync(refreshTimers)
    }

    override fun refreshAll() {
        (activity as HomeActivity?)?.refresh()
    }
    // endregion

    // region UI
    private fun initializeUI(binding: FragmentNavigationBinding) {
        container = binding.mainContainer
        emptyStage = binding.emptyStage
        progressBar = binding.progressBar
        syncProgressBar = binding.progressSyncingBar
        searchContainer = binding.searchContainer
        separator = binding.separator
        clearTutorialData = binding.clearTutorialData
        dismissBtn = binding.dismissBtn
        initScroll(binding)
        initializeButtons(binding)
        initializeRecyclerViews(binding)
    }

    private fun initScroll(binding: FragmentNavigationBinding) {
        val scrollEdge = convertDpToPx(binding.root.context, 2)
        nestedScrollView = binding.nestedScrollHome
        binding.mainContainer.setPadding(0, binding.root.context.getStatusBarHeight(), 0, 0)
        nestedScrollView?.isFocusableInTouchMode = true
        nestedScrollView?.descendantFocusability = ViewGroup.FOCUS_BEFORE_DESCENDANTS
        nestedScrollView?.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (scrollY <= scrollEdge) {
                binding.topBarSeparator.visibility = View.GONE
                ViewCompat.setElevation(binding.topBar, 0f)
            } else if (scrollY > scrollEdge) {
                binding.topBarSeparator.visibility = View.VISIBLE
                ViewCompat.setElevation(binding.topBar, 15f)
            }
        })
        // pause timers UI when scrolling fragment:
        nestedScrollView?.scrollState(idle = {
            updateTimers = true
        }, scrolled = {
            updateTimers = false
        })
    }

    private fun initializeButtons(binding: FragmentNavigationBinding) {
        binding.searchContainer.setOnClickListener {
            with(it.context) {
                val intent = Intent(this, SearchActivity::class.java)
                startActivity(intent)
                activity?.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_up_out)
            }
        }

        binding.navigateDashboard.setOnClickListener {
            (activity as HomeActivity?)?.openDashboard()
        }

        binding.filters.setOnClickListener {
            val dialog = FiltersDialog.getInstance(activityType = ActivityType.PROJECTS)
            dialog.setDismissCallBack {
                refreshSelf(refreshTimers = false)
            }
            this.activity?.also {
                dialog.show(it.supportFragmentManager, "Filters Dialog")
            }
        }
    }

    private fun initializeRecyclerViews(binding: FragmentNavigationBinding) {
        navigationRV = binding.areasProjects
        timersRV = binding.timersRv

        // adapters:
        navigationAdapter = NavigationAdapter(this.activity as TymingActivity, this, repository, false, nestedScrollView)
        navigationRV?.adapter = navigationAdapter

        timersAdapter = TimersAdapter(timersData, repository, this, requireContext())
        timersRV?.run {
            orientation = ViewPager2.ORIENTATION_HORIZONTAL
            adapter = timersAdapter
            clipToPadding = false
            clipChildren = false
            offscreenPageLimit = 3
            (getChildAt(0) as RecyclerView).overScrollMode = RecyclerView.OVER_SCROLL_NEVER
            setPageTransformer(CustPageTransformer(this.context))

            registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageScrollStateChanged(state: Int) {
                    updateTimers = (state == ViewPager2.SCROLL_STATE_IDLE)
                    super.onPageScrollStateChanged(state)
                }
            })
        }
    }

    private fun updateUI(refreshTimers: Boolean) {
        updateAdapter()
        hideProgress()
        updateNavigationVisibility()
        showEmptyScene()
        showTutorial()
        if (refreshTimers) {
            timersAdapter?.refreshAdapterWithData(timersData)
            updateTimersVisibility()
            bindServices()
        }
    }

    private fun updateAdapter(){
        navigationAdapter?.refreshAdapterWithData(navigationItems)
        navigationAdapter?.setDraggingEnabled(sortBy == SortBy.CUSTOM)
    }

    private fun updateNavigationVisibility() {
        if (navigationItems.size > 0) {
            navigationRV?.visibility = View.VISIBLE
            searchContainer?.visibility = View.VISIBLE
            separator?.visibility = View.VISIBLE
        } else {
            navigationRV?.visibility = View.GONE
            searchContainer?.visibility = View.GONE
            separator?.visibility = View.GONE
        }
    }

    fun updateTimersVisibility() {
        if (timersData.size > 0) {
            timersRV?.visibility = View.VISIBLE
            timersRV?.currentItem = 0
            changeSearchMargins(12)
        } else {
            timersRV?.visibility = View.GONE
            changeSearchMargins(2)
        }
    }

    private fun changeSearchMargins(dp: Int) {
        val context: Context = this.context ?: return
        val layoutParams = searchContainer?.layoutParams as ConstraintLayout.LayoutParams
        layoutParams.setMargins(layoutParams.leftMargin, convertDpToPx(context, dp), layoutParams.rightMargin, layoutParams.bottomMargin)
        searchContainer?.layoutParams = layoutParams
    }

    private fun showEmptyScene() {
        if (navigationItems.isEmpty()) {
            emptyStage?.visibility = View.VISIBLE
        } else {
            emptyStage?.visibility = View.GONE
        }
    }

    private fun showProgress() {
        emptyStage?.visibility = View.GONE
        progressBar?.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        progressBar?.visibility = View.GONE
    }
    // endregion

    // region Tutorial
    private fun showClearTutorialData(){
        val context: Context = this.context ?: return
        if (!PrefUtil.getIsTutorialDataCleared(context)){
            clearTutorialData?.visibility = View.VISIBLE
            dismissBtn?.setOnClickListener {
                repository.clearTutorialData()
                PrefUtil.setIsTutorialDataCleared(true, context)
                clearTutorialData?.visibility = View.GONE
                refreshAll()
            }
        }
    }

    override fun showTutorial() {
        if (context?.isTutorialCompleted(TUTORIAL.PROJECTS) != true) {
            navigationRV?.viewTreeObserver?.addOnGlobalLayoutListener(
                    object : ViewTreeObserver.OnGlobalLayoutListener {
                        override fun onGlobalLayout() {
                            navigationRV?.viewTreeObserver?.removeOnGlobalLayoutListener(this)
                            // RV fully loaded:
                            startTutorial()
                        }
                    }
            )
        }
    }

    private fun startTutorial() {
        val activity = this.activity ?: return
        val targets = createTutorialTargets()
        val spotlight = createSpotlight(activity, targets) {
            context?.markTutorialCompleted(TUTORIAL.PROJECTS)
            showClearTutorialData()
        }
        spotlight.start()
        addSpotLightListeners(spotlight, targets)
    }

    private fun createTutorialTargets(): ArrayList<Target> {
        val targets = arrayListOf<Target>()

        val context: Context = this.context ?: return targets

        container?.apply {
            val location = IntArray(2)
            navigationRV!!.getLocationInWindow(location)
            val x = location[0] + navigationRV!!.width - convertDpToPx(context, 36)
            val y = location[1] + convertDpToPx(context, 28)
            val target1 = createTarget(
                    x = x.toFloat(),
                    y = y.toFloat(),
                    shapeRadius = 30,
                    title = context.getString(R.string.item_controls),
                    description = context.getString(R.string.you_can_items)
            )
            targets.add(target1)

            val target2 = createTarget(
                    anchorView = findViewById<View>(R.id.filters),
                    shapeRadius = 60,
                    title = context.getString(R.string.filters_alt),
                    description = context.getString(R.string.sort_projects)
            )
            targets.add(target2)

            val target3 = createTarget(
                    anchorView = findViewById<View>(R.id.search_icon),
                    shapeRadius = 40,
                    title = context.getString(R.string.search),
                    description = context.getString(R.string.search_is_fastest)
            )
            targets.add(target3)

            val target4 = createTarget(
                    anchorView = (activity as HomeActivity).findViewById<View>(R.id.fab),
                    shapeRadius = 60,
                    title = context.getString(R.string.add_item),
                    description = context.getString(R.string.click_to_add)
            )

            target4.overlay?.apply {
                val binding = SpotlightTargetBinding.bind(this)
                binding.nextBtn.text = context.getString(R.string.finish)
                binding.nextBtn.setMarginBottom(240)
            }

            targets.add(target4)
        }

        return targets
    }
    // endregion

    override fun getTAG(): String {
        return TAG
    }

    companion object {
        const val TAG = "HOME_FRAGMENT"
        fun newInstance(): NavigationFragment {
            val fragment = NavigationFragment()
            val args = Bundle()
            args.putString(TAG, "TAG")
            fragment.arguments = args
            return fragment
        }
    }
}
