package com.fuzzythread.tyming.global.exception;

public class TaskNotSelectedException extends RuntimeException {
    public TaskNotSelectedException() {
        super("Please select a task");
    }
}
