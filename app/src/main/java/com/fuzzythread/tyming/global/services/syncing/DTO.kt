package com.fuzzythread.tyming.global.services.syncing

//import com.squareup.moshi.Json
//import com.squareup.moshi.JsonClass
import java.util.*


//@JsonClass(generateAdapter = true)
data class DbSnapshot(
        val version: Long,
        val date: Date,
        val areas: List<Area>
)

data class Area(
        var title: String = "",
        var notes: String = "",
        var color: String = "",
        var isCollapsed: Boolean = false,
        var createdDate: Date? = null,
        var archivedDate: Date? = null,
        var singularProjectsPlaceholder:Boolean = false,
        val projects: List<Project>)

data class Project(
        var title: String = "",
        var notes: String = "",
        var color: String = "",
        var trackProgress: Boolean = false,
        var isCollapsed: Boolean = true,
        var createdDate: Date? = null,
        var deadlineDate: Date? = null,
        var archivedDate: Date? = null,
        val tasks: List<Task>)

data class Task(
        var title: String = "",
        var notes: String = "",
        var createdDate: Date? = null,
        var deadlineDate: Date? = null,
        var completedDate: Date? = null,
        var timeElapsed: Long = 0,
        var isTimerRunning: Boolean = false,
        var timeEntries: List<TimeEntry>)

data class TimeEntry(
        var notes: String = "",
        var location: String = "",
        var startDate: Date? = null,
        var endDate: Date? = null,
        var archivedDate: Date? = null)
