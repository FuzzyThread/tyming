package com.fuzzythread.tyming.global.di

import com.fuzzythread.tyming.global.utils.time.DefaultTimer
import com.fuzzythread.tyming.global.utils.time.TimerInterface
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class TimerModule {

    @Provides
    @Singleton
    fun provideTimer(): TimerInterface = DefaultTimer()
}
