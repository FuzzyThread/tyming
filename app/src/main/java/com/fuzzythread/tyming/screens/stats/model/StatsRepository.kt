package com.fuzzythread.tyming.screens.stats.model

import android.content.Context
import android.graphics.Color
import com.fuzzythread.tyming.R
import com.fuzzythread.tyming.global.db.ChartsRepository
import com.fuzzythread.tyming.global.db.dao.AreaDao
import com.fuzzythread.tyming.global.db.dao.ProjectDao
import com.fuzzythread.tyming.global.db.dao.TaskDao
import com.fuzzythread.tyming.global.db.dao.TimeEntryDao
import com.fuzzythread.tyming.global.db.entities.TimeEntryEntity
import com.fuzzythread.tyming.global.utils.other.TimeFrame
import com.fuzzythread.tyming.global.utils.time.isCurrentWeek
import com.fuzzythread.tyming.global.utils.time.sumTimeEntriesPeriods
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import org.joda.time.*
import java.util.*

class StatsRepository(
        areaDao: AreaDao,
        projectDao: ProjectDao,
        taskDao: TaskDao,
        private val timeEntryDao: TimeEntryDao,
        private val context: Context
) : ChartsRepository(areaDao, projectDao, taskDao, timeEntryDao, context) {

    fun getStatsData(date: DateTime, timeFrame: TimeFrame): StatsData {
        val firstDay: DateTime
        val lastDayId: Int
        val periodDays: String
        val dayBars = ArrayList<IBarDataSet>()
        val days = mutableListOf<DateTime>()
        val daysNames = ArrayList<String>()
        var overallTime = 0f
        var workedDays = 0
        val today = DateTime()
        val allTimeEntries = mutableListOf<TimeEntryEntity>()

        if (timeFrame == TimeFrame.WEEK) {
            firstDay = date.withDayOfWeek(1)
            lastDayId = 7
            periodDays = "${firstDay.toString("MMM dd")} - ${date.withDayOfWeek(7).toString("MMM dd")}".toUpperCase(Locale.ROOT)
        } else {
            firstDay = date.withDayOfMonth(1)
            lastDayId = date.dayOfMonth().maximumValue
            periodDays = firstDay.toString("MMMM").toUpperCase(Locale.ROOT)
        }

        // Bar Data Daily:
        for (dayId in 0 until lastDayId) {
            val day = firstDay.plusDays(dayId)
            daysNames.add(formatDay(timeFrame, day))
            days.add(day)
            // combine time entries by project:
            val projectTimes = getProjectsTimesSortedDesc(timeEntryDao.getTimeEntriesByDay(day).also { allTimeEntries.addAll(it) })
            if (projectTimes.isNotEmpty()) {
                dayBars.add(getDayBar(projectTimes, dayId))
                overallTime += projectTimes.map { it.second }.sum()
                workedDays++
            } else {
                dayBars.add(getEmptyDayBar(dayId))
            }
        }

        return StatsData(
                barData = getBarData(dayBars, getBarWidth(timeFrame)),
                pieData = getPieDataForTimeFrame(allTimeEntries),
                periodDays = periodDays,
                isCurrentTimeFrame = days.first().isCurrentWeek(today),
                tasks = getTasksRVDataForTimeFrame(allTimeEntries),
                averageHours = getAverageTime(workedDays, overallTime),
                workedDays = workedDays,
                previousTimeFramePeriod = getPreviousTimeFramePeriod(timeFrame, firstDay.minusDays(1)),
                overallTime = overallTime,
                daysNames = daysNames,
                days = days
        )
    }

    private fun formatDay(timeFrame: TimeFrame, day: DateTime): String {
        val weekDay = dateTimeFormatterDay.print(day).substring(0..2)
        val formattedDay = weekDay[0] + "\n" + day.dayOfMonth
        return if (timeFrame == TimeFrame.WEEK) {
            formattedDay
        } else {
            if (weekDay.trim().toLowerCase() == context.getString(R.string.monday)) {
                formattedDay
            } else {
                ""
            }
        }
    }

    private fun getPieDataForTimeFrame(timeEntries: List<TimeEntryEntity>): PieData {
        val pieColors = mutableListOf<Int>()
        val pieEntries = mutableListOf<PieEntry>()
        val timeEntriesByProject = timeEntries
                .filter { getTaskEntity(it.task.targetId)?.project?.targetId != null }
                .groupBy { getTaskEntity(it.task.targetId)?.project?.targetId ?: -1 }

        timeEntriesByProject.forEach { timeEntry ->
            val tempTimeEntries = timeEntry.value
            val sumTimeEntries = sumTimeEntriesPeriods(tempTimeEntries)
            // TimeEntry has project and has time:
            if (timeEntry.key >= 0 && sumTimeEntries > 0) {
                pieEntries.add(PieEntry(sumTimeEntries, timeEntry.key))
                pieColors.add(getProjectColor(timeEntry.key))
            }
        }
        val pieDataSet = PieDataSet(pieEntries, context.getString(R.string.project_time))
        pieDataSet.colors = pieColors
        return PieData(pieDataSet).also {
            it.setValueTextColor(Color.TRANSPARENT)
        }
    }

    private fun getTasksRVDataForTimeFrame(timeEntries: List<TimeEntryEntity>): MutableList<TaskStatsViewModel> {
        val tasksData = mutableListOf<TaskStatsViewModel>()

        val timeEntriesByTask = timeEntries
                .filter { getTaskEntity(it.task.targetId) != null }
                .groupBy { it.task.targetId }
        timeEntriesByTask.forEach { timeEntry ->
            val tempTask = getTaskEntity(timeEntry.key)
            tempTask?.apply {
                val tempProject = getProjectEntity(tempTask.project.targetId)
                TaskStatsViewModel().apply {
                    id = tempTask.id
                    titleTask = tempTask.title
                    titleProject = tempProject?.title ?: ""
                    color = tempProject?.color ?: ""
                    time = sumTimeEntriesPeriods(timeEntry.value)
                }.also {
                    if (it.time > 0) {
                        tasksData.add(it)
                    }
                }
            }
        }
        tasksData.sortByDescending { it.timeFormatted }
        return tasksData
    }

    private fun getBarWidth(timeFrame: TimeFrame): Float {
        return if (timeFrame == TimeFrame.WEEK) {
            0.25f
        } else {
            0.75f
        }
    }

    data class StatsData(val pieData: PieData, val barData: BarData, val tasks: MutableList<TaskStatsViewModel>, val overallTime: Float, val averageHours: Float, val periodDays: String, val daysNames: List<String>, val workedDays: Int = 0, val isCurrentTimeFrame: Boolean, val days: List<DateTime>, val previousTimeFramePeriod: Float)
}
